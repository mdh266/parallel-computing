#include "petsc.h"

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  MPI_Comm       comm;
  PetscInt       n = 20;
  PetscErrorCode ierr;
  PetscInt ntids, mytid;
  
  PetscFunctionBegin;
  ierr = PetscInitialize(&argc,&argv,0,0); CHKERRQ(ierr); 
  comm = PETSC_COMM_WORLD;
  
  ierr = MPI_Comm_size(comm,&ntids);
  ierr = MPI_Comm_rank(comm,&mytid);

  ierr = PetscOptionsGetInt(PETSC_NULL,"-n",&n,PETSC_NULL); CHKERRQ(ierr);
  printf("n=%d\n",n);
  ierr = PetscPrintf(comm,"Input parameter: %d\n",n); CHKERRQ(ierr);
  
  ierr = PetscSynchronizedPrintf(comm, "Processor %d : input param = %d \n", mytid, n);
  ierr = PetscSynchronizedFlush(comm);


  ierr = PetscFinalize();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

