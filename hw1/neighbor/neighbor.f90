program neighbor

 integer, parameter :: N=160
 integer, parameter :: M=10000

 integer, parameter :: KR8 = selected_real_kind(13)
 real(KR8)          :: time, t0, t1, gtod_timer

 real*8  ::  a(N,N), b(N,N)
 integer ::  i,j,k, nt=0

 t0 = gtod_timer()

    do j = 1,N
       do i = 1,N
          a(i,j) = 0.0
          b(i,j) = 0.0
       end do
    end do


    do i=1,N
       do j=1,N
          do k=1,M
                a(j,i) = a(j,i) + dble(i*j) / dble(i+j+k)
          end do
       end do
    end do

    do i=2,N-1
       do j=2,i
          do k=1,M

             b(j,i) =  b(j,i)                              &
                     + a(j  ,i-1)/k     + a(j  ,i+1)/k     &
                     + a(j-1,i  )/k     + a(j+1,i  )/k     &
                     + a(j-1,i-1)/(k*k) + a(j+1,i+1)/(k*k) &
                     + a(j-1,i+1)/(k*k) + a(j+1,i-1)/(k*k)
          end do
       end do
    end do

  t1 = gtod_timer()
  time = t1 - t0
  write (6,*) 'pnb ', nt,time

  !Make the compiler think it might need b, so that it doesn't
  !remove the calculation during optimization.

  if(nt==500) print*, "b(2,2),b(N-1,2):" ,b(2,2),b(2,N-1)

end program

