#define N 160
#define M 10000

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
double gtod_timer(void);

// int c_setaffinity(int);



int main() 
{

   double a[N+1][N+1], b[N+1][N+1];
   int i,j,k, nt=0;

   double t0, t1, time;

// uncomment this in parallel code-- avoids initial OMP overhead
// cost in timing below
#pragma omp parallel
{
  #ifdef _OPENMP
   nt = omp_get_num_threads();
  #endif
}
  double temp_sum = 0;

   t0 = gtod_timer();

#pragma omp parallel shared(a,b)
{
   
    // ZERO
    #pragma omp for collapse(2) schedule(static) private(j)
    for( i=1; i<=N; i++ )
    {
	for( j=1; j<=N; j++ )
	 {
           	 a[i][j] = 0.0e0;
           	 b[i][j] = 0.0e0;
       	 }
     }
    
    #pragma omp for collapse(3) schedule(static) private(j,k)
    for( i=1; i<=N; i++ )
        for( j=1; j<=N; j++ )
            for( k=1; k<=M; k++ )
       	        a[i][j] += (double)(i*j) / (double)(i+j+k);


    
   #pragma omp for schedule(runtime) private(i,j,k)
    for( i=2; i<=N-1; i++ ) 
    {
        for( j=2; j<=i; j++ )
        {
		for( k=1; k<=M; k++ )
	        {
	     	        b[i][j]  +=   a[i-1][j  ]/k     + a[i+1][j  ]/k
                          	    + a[i  ][j-1]/k     + a[i  ][j+1]/k
                         	    + a[i-1][j-1]/(k*k) + a[i+1][j+1]/(k*k)
                         	    + a[i+1][j-1]/(k*k) + a[i-1][j+1]/(k*k);
	 	}
	   
	}
    }
}  // end of parallel region

   t1 = gtod_timer();
   time  = t1 - t0;

   printf("pnb %d %lf\n",nt,time); 

   //Make the compiler think it might need b, so that it doesn't
   //remove the calculation during optimization.
   if(nt==500) printf("b(2,2),b(N-1,2): %f %f\n",b[2][2],b[N-1][2]);
}
