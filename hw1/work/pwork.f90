program triv
use omp_lib
!  integer, parameter   :: N=15000
   integer, parameter   :: N=250
   real*8,dimension(N)  :: x
   real*8,dimension(N,N):: a
   real*8                      :: factor
   real*8 gtod_timer, time, t0,t1,t2

   factor=0.5

!$omp parallel private(mytid,icore,ierr)
                       nthreads = omp_get_num_threads()
                       mytid    = omp_get_thread_num()
                       icore=mytid/2 + modulo(mytid*8,16)
                       ierr =f90_setaffinity(icore)
!$omp do
   do j = 1,N
      X(j)=0.99
   end do
!$omp do
   do j = 1,N
      do i = 1,N
         a(i,j) = 0.95
      end do
   end do

!$omp end parallel


   t0 = gtod_timer()
   t1 = gtod_timer()

!$omp parallel private(mytid,icore,ierr)
                       nthreads = omp_get_num_threads()
                       mytid    = omp_get_thread_num()
                       icore=mytid/4 + modulo(mytid*4,16)
                       ierr =f90_setaffinity(icore)

! Uncomment next line to see assignments
!print*,"nthreads,mytid:core", nthreads,mytid,":",icore
       call pwork(mytid,factor,N,x,a)
   
!$OMP END PARALLEL

   t2 = gtod_timer()
   time = dble(t2 - t1*2 + t0)
   print*," pwork",nthreads, time

end program

subroutine pwork(mytid,factor,N,x,a)
   real*8, dimension(N)   :: x
   real*8, dimension(N,N) :: a
   real*8                 :: factor
   integer :: OMP_GET_NUM_THREADS

   nthreads=OMP_GET_NUM_THREADS()
   ibeg= 1 + mytid *(N/nthreads)
   iend=(1 + mytid)*(N/nthreads)

   do j = ibeg,iend
      do i = 1,N
         a(i,j) = sqrt(a(i,j)/x(j))*sqrt(x(i)*factor/a(i,j))
      end do
   end do
end subroutine
