#define  N 15000
#include <omp.h>

double gtod_timer(void);
void pwork(int mytid,double factor,int n,double x[N],double a[N][N]);

int main()
{
   double x[N];
   double a[N][N];
   double factor;
   double time,t0,t1,t2;

   int    i,j, nthreads, mytid, icore, ierr;

   factor=0.5;

#pragma omp parallel private(mytid,icore,ierr)
{
                       nthreads = omp_get_num_threads();
                       mytid    = omp_get_thread_num();
                       icore=mytid/2 + (mytid*8)%16;
                       ierr =c_setaffinity(icore);
   #pragma omp for
      for(j=0;j<N;j++){ 
         x[j]=0.99;
      }
   #pragma omp for
      for(i=0;i<N;i++){ 
         for(j=0;j<N;j++){ 
            a[i][j] = 0.95;
         }
      }
   
}


   t0 = gtod_timer();
   t1 = gtod_timer();

   //system( "sleep 2"); 
   #pragma omp parallel private(mytid,icore,ierr)
   {
                       nthreads = omp_get_num_threads();
                       mytid    = omp_get_thread_num();
                       icore=mytid/2 + (mytid*8)%16;
                       ierr =c_setaffinity(icore);

   //Uncomment the next line to see the assignments.
   printf("nthreads,mytid:core %d %d %d\n", nthreads,mytid,icore);
   pwork(mytid,factor,N,x,a);
  
   t2 = gtod_timer();
   time = t2 - t1*2 + t0;
   printf(" pwork: %d %f \n",nthreads, time);

  }
}

void pwork(int mytid,double factor,int n,double x[N],double a[N][N]){

   int nthreads, ibeg, iend, i,j;
   nthreads=omp_get_num_threads();
   ibeg=     mytid *(N/nthreads);
   iend=(1 + mytid)*(N/nthreads);

   for(i=ibeg;i<iend;i++){
      for(j=0;j<n;j++){
         a[i][j] = sqrt( a[i][j]/x[j])*sqrt(x[i]*factor/a[i][j] );
      }
   }
}
