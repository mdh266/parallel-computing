#include <math.h>
#include <stdio.h>
#include <omp.h>

#define N 30000000

double gtod_timer(void);
//int c_setaffinity(int);

int main()
{

  int i, niter;
  double a[N], error, sum;

  int nt=0;
  double time, t0, t1;

#pragma omp parallel
nt = omp_get_num_threads();

  for(i = 0; i < N-1; i+=2)
  {
     a[i]   = 0.0; 
     a[i+1] = 1.0;
  }

  niter = 0;

  t0 = gtod_timer();


/*                 Update "a" until criteria is reached */
  do
  {
       niter++;

       error = 0.0; // error must be initialized before parallel region

        // a is shared  by default.
       #pragma omp parallel
       {
	
	  #pragma omp for schedule(runtime)
          for (i = 1; i < N;   i+=2) 
          {
	      a[i] = (a[i] + a[i-1]) / 2.0;
          } //implied barrier

	  #pragma omp for schedule(runtime)
          for (i = 0; i < N-1; i+=2)
          {
	      a[i] = (a[i] + a[i+1]) / 2.0;
	  } // implied barrier
      
	  #pragma omp for reduction(+:error) schedule(runtime)
          for (i = 0; i < N-1; i++) 
	  {
		error += fabs(a[i] - a[i+1]);
	  } //implied barrier
	
	} // end of parallel region

  } while (error >= 1.0);

  t1 = gtod_timer();
  time  = t1 - t0;

  sum = 0.0;
  
  for (i = 0; i < N; i++)
       sum += a[i];


  printf("prb %d %lf\n",nt,time);

//  printf("Average value = %15.12lf\n", sum/N);
//  printf("Number of iterations = %d\n", niter);


}
