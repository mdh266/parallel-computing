#include <math.h>
#include <stdio.h>
#include <omp.h>

#define N 10

double gtod_timer(void);
int c_setaffinity(int);
int printer(double * x, int n);

int main()
{

  int i, niter, icore, it, ierr;
  double a[N], error, sum;

  int nt=0;
  double time, t0, t1;

#pragma omp parallel
   nt = omp_get_num_threads();

  niter = 0;

  t0 = gtod_timer();


/*                 Update "a" until criteria is reached */
//  do
//   {

     
     #pragma omp parallel private(it,icore,ierr)
     {  
       do
	 {
	 	if(niter==0)
		{
			nt = omp_get_num_threads();
			it = omp_get_thread_num();
			icore=it/8 + (it*2) % 16;
			ierr = c_setaffinity(icore);

			// initialize the array
			#pragma omp for schedule(runtime)
			for(i = 0; i < N-1; i+=2)
			{
     				a[i]   = 0.0; 
				a[i+1] = 1.0;
			}
			
			#pragma omp single
			{
				for(i = 0; i < N; i++)
					printf("%f ", a[i]);
				printf("\n");
			}

		}

		// need to update niter++ just once and
		// wanted a barrier just to be safe...
		#pragma omp single 
		{
			niter++;
		}

		error = 0.0;
	
		#pragma omp for schedule(runtime)
		for (i = 1; i < N;   i+=2) 
       		{
			 a[i] = (a[i] + a[i-1]) / 2.0;
	      	}

		#pragma omp for schedule(runtime)	
       		for (i = 0; i < N-1; i+=2)
       		 {
			a[i] = (a[i] + a[i+1]) / 2.0;
		}

		#pragma omp for reduction(+:error) schedule(runtime)
	        for (i = 0; i < N-1; i++) 
		{
			error += fabs(a[i] - a[i+1]);
		}
	  } while (error >= 1.0);
}
  t1 = gtod_timer();
  time  = t1 - t0;

  sum = 0.0;
  
  for (i = 0; i < N; i++)
       sum += a[i];


  printf("prb %d %lf\n",nt,time);

//  printf("Average value = %15.12lf\n", sum/N);
//  printf("Number of iterations = %d\n", niter);
}

int printer(double * x, int n)
{
	int i;
	for(i = 0; i < N; i++)
		printf("%f ", x[i]);
	printf("\n");

	return 0;
}
