program red_black
integer,   parameter :: KR8 = selected_real_kind(13)

integer,parameter    ::  N=30000000
integer              ::  i, niter
real(KR8)            ::  a(N), error, sum

real(KR8)            ::  t0, t1, gtod_timer

  niter =   0
  error = 1.0
  niter   = 0
  nt      = 0

!!   Uncomment this to get the number of threads
!!   and have the threads "primed" to go.
!!omp parallel
 ! nt = omp_get_num_threads()
!!omp end parallel

  t0 = gtod_timer()

!                 Initialize a
  do i=1,N-1,2
     a(i)   = 0.0 
     a(i+1) = 1.0
  end do

  niter =   0


!                 Update a until criteria is reached.
  do while (error .ge. 1.0)

       do i=2, N,   2;  a(i) = (a(i) + a(i-1)) / 2.0; end do
       do i=1, N-1, 2;  a(i) = (a(i) + a(i+1)) / 2.0; end do

       error = 0.0
       do i=1,N-1
          error = error +  abs(a(i) - a(i+1))
       end do
       niter = niter+ 1
!      print*,"takemeout no error",niter,error

  end do
  t1   = gtod_timer()
  time = real(t1 - t0)

  sum = 0.0
  do i = 1,N
    sum = sum + a(i)
  end do


  print*,"Team threads, Time: ",nt, time
  print*,"Average value = ",        sum/N
  print*,"Number of iterations = ", niter, " team threads:", nt


end program red_black
