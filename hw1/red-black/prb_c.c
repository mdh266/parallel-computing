#include <math.h>
#include <stdio.h>
#include <omp.h>

#define N 30000000

double gtod_timer(void);
int c_setaffinity(int);

int main()
{

  int i, niter, icore, it, ierr;
  double a[N], error, sum;

  int nt=0;
  double time, t0, t1;

#pragma omp parallel
   nt = omp_get_num_threads();


/*                 Initialize a */
  for(i = 0; i < N-1; i+=2)
  {
     a[i]   = 0.0; 
     a[i+1] = 1.0;
  }

  niter = 0;

  t0 = gtod_timer();


/*                 Update "a" until criteria is reached */
  do
  {
     #pragma omp parallel private(it,icore,ierr)
      {
      
	if(niter==0)
	{
		nt = omp_get_num_threads();
		it = omp_get_thread_num();
		icore=it/8 + (it*2) % 16;
		ierr = c_setaffinity(icore);
	}

	// single allows for their to be a barrier 	
	#pragma omp single 
	{
		niter++;
	}

        error = 0.0;
	
	#pragma omp for schedule(runtime)
	for (i = 1; i < N;   i+=2) 
        {
		 a[i] = (a[i] + a[i-1]) / 2.0;
      	}


	#pragma omp for schedule(runtime)	
        for (i = 0; i < N-1; i+=2)
        {
		a[i] = (a[i] + a[i+1]) / 2.0;
	}

	#pragma omp for reduction(+:error) schedule(runtime)
        for (i = 0; i < N-1; i++) 
	{
		error += fabs(a[i] - a[i+1]);
	}
     }

  } while (error >= 1.0);

  t1 = gtod_timer();
  time  = t1 - t0;

  sum = 0.0;
  
  for (i = 0; i < N; i++)
       sum += a[i];


  printf("prb %d %lf\n",nt,time);

//  printf("Average value = %15.12lf\n", sum/N);
//  printf("Number of iterations = %d\n", niter);


}
