#include  <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

double gtod_timer(void);

int main (int argc, char* argv[]){

int  N, N2, i, nt=1, ierr;


double PI25DT = 3.141592653589793238462643e0;
double deltax, halfdeltax, x, mult, area, pi, error, t0,t1,time;
char line[12];

/* Once you have decided on a value of N, you can comment
  out the next two lines, assign a value to N, recompile
  and submit it as a batch job, using the "job" script. */

   //ierr=scanf("%d",&N);
    N = 650000;

#pragma omp parallel
{ nt = omp_get_num_threads(); }


   
   deltax = 2.0e0/N;
   halfdeltax = 0.5e0 * deltax;
   N2 = 2 * N;
   area = 0.0e0;
   
   t0 = gtod_timer();

   #pragma omp parallel private(x,mult)
   { 
     
      #pragma omp for reduction(+:area)
      for (i = 1; i <= N2-1; i++)
      {
         x = -1.0e0 + (double)i * halfdeltax;
         mult = ((double)(i%2)*2.0e0) + 2.0e0;
         area = area + mult * sqrt(1.0e0 - x*x) * halfdeltax;
      }
   }

   t1 = gtod_timer();

   area = area/3.0e0;
   pi   = area*2.0e0;

   error = fabs(PI25DT - pi);
   time  = t1 - t0;
   
   //printf("The value of pi and error: %17.14lf %17.14lf\n", pi, error);

   printf("%lf\n",time);
  // printf("pi %d %lf\n",nt,time); 
   
}
