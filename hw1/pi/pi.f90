program simpson

integer :: N, N2, i, nt=0

integer,   parameter :: KR8 = selected_real_kind(13)
real(KR8)            :: PI25DT = 3.141592653589793238462643_KR8
real(KR8) :: deltax, halfdeltax, x, mult, area, pi, time
real(KR8) :: t0, t1, gtod_timer

! Once you have decided on a value of N, you can comment
! out the next two lines, assign a value to N, recompile
! and submit it as a batch job, using the "job" script.

   write (6,*) 'How many intervals?'
   read (5,*) N
!  N = <something>

   nt = 1
!$omp parallel
!  nt = omp_get_num_threads()
!$omp end parallel

   
   deltax = 2.0d0/N
   halfdeltax = 0.5d0 * deltax
   N2 = 2 * N
   area = 0.0d0
   
   t0 = gtod_timer()

   do i = 1, N2-1
      x = -1.0d0 + dble(i) * halfdeltax
      mult = (dble(mod(i,2))*2.0d0) + 2.0d0
      area = area + mult * sqrt(1.0d0 - x*x) * halfdeltax
   end do

   area = area / 3.0d0
   pi = area * 2.0d0

   t1 = gtod_timer()
   time = real(t1 - t0)
   error = abs(PI25DT - pi)
   
   write (6,*) 'The value of pi and error: ', pi, error
   write (6,*) 'pi ', nt,time
   
end program
