#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

class scatterqueue : public queue 
{

public :

  // constructor extends queue!!
  scatterqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) 
  {
  
  };

  // class methods
  void addbulktasks(struct coordinate * xy_array, int numWorkers, int & stop)
  {
	   for(int i = 0; i < numWorkers; i++)
	    {
		workcircle->next_coordinate(xy_array[i]);
		if(!workcircle->is_valid_coordinate(xy_array[i]) )
			stop = 1;	
	    }

  }
  
  void wait_for_work(struct coordinate xy, int & res)
  { 
	if(workcircle->is_valid_coordinate(xy))
		res = belongs(xy, workcircle->infty);
	else
		res = 0;
 }

 void add_contributions(struct coordinate * xy_array, int * contributions, int numWorkers)
 {
	for(int i = 0; i < numWorkers; i++)
	{
 		if (workcircle->is_valid_coordinate(xy_array[i])) 
  		{
    			coordinate_to_image(xy_array[i],contributions[i]);
			total_tasks++;
		}
	}
  }

  void complete(MPI_Comm comm,circle *workcircle, int numWorkers) 
  {
 
    t_stop = MPI_Wtime();
     printf("Area computed: %e\n by %d tasks in time %f\n",
     	   area,total_tasks,t_stop-t_start);

    image->Write();
  }

};


int main(int argc,char **argv) 
{
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;
  
  struct coordinate * xy_array;
  int * contributions;
  struct coordinate xy;
  int res;
  int stop = 0;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);

  if (ierr) return MPI_Abort(comm,1);


  if (ntids==1)
  {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  circle *workcircle = new circle(2./steps,iters);
  
  scatterqueue *taskqueue = new scatterqueue(comm,workcircle);
  
 
//  boolean to kill the loop

  // intialize everything
  if (mytid==ntids-1)  
  {
   	// Create the image object
    	taskqueue->set_image( new Image(2*steps,2*steps,"mandelpicture") );
 
    	xy_array = new struct coordinate[ntids-1];
   	contributions = new int[ntids-1];
  }


  // keep on going until done
  for(;;)
  {
	if(!stop)
	{
 		 // go for it !
  		 if(mytid == ntids-1)  // fill up xy_array with coordinates
			taskqueue->addbulktasks(xy_array, ntids-1, stop);
		
  		MPI_Scatter(xy_array, 2, MPI_DOUBLE, &xy, 2, MPI_DOUBLE, ntids-1, comm);

	 	if (mytid != ntids-1)
			taskqueue->wait_for_work(xy, res);

  		MPI_Gather(&res, 1, MPI_INT, contributions, 1, MPI_INT, ntids-1, comm);
		
 	 	if (mytid==ntids-1)  
			taskqueue->add_contributions(xy_array, contributions, ntids-1);

		//update value of stop for every process
		MPI_Bcast(&stop, 1, MPI_INT, ntids-1, comm);
	}
	else 
		break;

  }

 
  if (mytid==ntids-1)
  {
	taskqueue->complete(comm, workcircle, ntids-1);
	delete xy_array;
    	delete contributions;
  } 

  MPI_Finalize();
  return 0;
}
