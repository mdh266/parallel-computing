#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

class asynchqueue : public queue 
{

public :

  // constructor extends queue!!
  asynchqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) 
  {  };

  // class methods
  void addtasks(struct coordinate *xy, int numWorkers)
  {

	MPI_Status status[numWorkers];
	MPI_Request request[numWorkers];
	int contributions[numWorkers];
	int index;

	// send all x,y values out and wait till are are back
	for(int i = 0; i < numWorkers; i++)
	{
		MPI_Isend(&xy[i], 2, MPI_DOUBLE,
			  i, 0, comm, &request[i]);

		MPI_Irecv(&contributions[i], 1, MPI_INT,
			  i, 0, comm, &request[i]);
	}

	for(;;)
	{
		// wait for any to finish
		MPI_Waitany(numWorkers, request, &index, status);
	
		// test that workers coordinate
 		if (workcircle->is_valid_coordinate(xy[index])) 
  		{
    			 coordinate_to_image(xy[index],contributions[index]);
   			 total_tasks++;
		}
	
		workcircle->next_coordinate(xy[index]);
		
		if(workcircle->is_valid_coordinate(xy[index]))
		{	
			MPI_Isend(&xy[index], 2, MPI_DOUBLE,
				  index, 0, comm, &request[index]);

			MPI_Irecv(&contributions[index], 1, MPI_INT,
				  index, 0, comm, &request[index]);
		}
		else
			break;
	}
	// go through the ones that are back and add them to image
	// if they are valid coordinates    	
	for(int i = 0; i < numWorkers; i++)
	{
 		if (workcircle->is_valid_coordinate(xy[i])) 
  		{
    		 	coordinate_to_image(xy[i],contributions[i]);
   		   	total_tasks++;
	 	 }
	}


  }

  void complete(circle *workcircle, int numWorkers) 
  { 
	struct coordinate xy[numWorkers];
	
	for(int i = 0; i < numWorkers; i++)
		workcircle->invalid_coordinate(xy[i]);
	
	MPI_Status status[numWorkers];
	MPI_Request request[numWorkers];
	int contributions[numWorkers];

	for(int i = 0; i < numWorkers; i++)
	{

		MPI_Isend(&xy[i], 2, MPI_DOUBLE,
			  i, 0, comm, &request[i]);

		MPI_Irecv(&contributions[i], 1, MPI_INT,
			  i, 0, comm, &request[i]);
  	}
	
	MPI_Waitall(numWorkers, request, status);
	
	t_stop = MPI_Wtime();
	printf("Area computed: %e\n by %d tasks in time %f\n",
     		   area,total_tasks,t_stop-t_start);

	image->Write();
  }
};


int main(int argc,char **argv) 
{
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);

  if (ierr) return MPI_Abort(comm,1);


  if (ntids==1)
  {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  // every process has a workcircle
  circle *workcircle = new circle(2./steps,iters);
  
  // every process has a taskqueue.  On ntids-1 it will pass 
  // doll out the tasks using addtask.  On all the other process it just
  // waits for work and then pass its instance of a circle to its function
  // wait for work
  asynchqueue *taskqueue = new asynchqueue(comm,workcircle);
  
  if (mytid==ntids-1)  
  {
    // Create the image object
    taskqueue->set_image( new Image(2*steps,2*steps,"mandelpicture") );
   
    struct coordinate xy_array[ntids-1];

      // fill up xy_array with coordinates
      for(int i = 0; i < ntids-1; i++)
      {
	     workcircle->next_coordinate(xy_array[i]);
      }
	
      taskqueue->addtasks(xy_array,ntids-1);
	// once done sampling the points
    	// MASTER THREAD PRINTS THE OBJECT TO FILE
      taskqueue->complete(workcircle, ntids-1);
 }
  else
	taskqueue->wait_for_work(comm,workcircle);
    // other processors wait for work, i.e. they
    // wait to be sent something
    //
  MPI_Finalize();
  return 0;
}
