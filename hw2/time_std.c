#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <mpi.h>

int main(int argc,char **argv) 
{
  int mytid;
  int comm_sz;
  MPI_Comm comm;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
  MPI_Comm_size(comm,&comm_sz);


  {
    double tstart,tstop,jitter; 
    double jitter_sum, jitter_avg, diffSquared, jitter_std;
    void *sendbuf;
    int wait;

    // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;

//	printf("Jitter before reduce of process %d = %e\n", mytid, jitter);
	//printf("comm_sz = %d\n", comm_sz);
	
    // Find the average of the jitters
    sendbuf = (void*)&jitter; 
    	
// sendbuffer, recv buffer, size, double, opp, rank of root, communicator
    MPI_Reduce(sendbuf,(void*)&jitter_sum,1,MPI_DOUBLE,MPI_SUM,0,comm);
    
//    printf("Jitter after reduce of process %d = %e\n", mytid, jitter);
    
    if (mytid==0)
    {
      jitter_avg = jitter_sum/comm_sz;
      printf("Average OS jitter: %e\n",jitter_avg);
    }
    
    // Broadcast jitter_avg to all process
    // NOTE: This needs to be called by all process!  
    //       Fine as is, does not belong in if statement.
    MPI_Bcast((void*)&jitter_avg, 1, MPI_DOUBLE, 0, comm);
	

//	printf("Jitter after bcast of process %d = %e\n", mytid, jitter);
//	printf("jitter_avg after bcast of process %d = %e\n", mytid, jitter_avg);
	
	 //Find each process difference from the jitter average
	 // squared
	diffSquared = (jitter-jitter_avg)*(jitter-jitter_avg);
	
//	printf("diffSquared for process %d = %e\n", mytid, diffSquared);

	// Now reduce to get the std
	sendbuf = (void*)&diffSquared;

	MPI_Reduce(sendbuf,(void*)&jitter_std,1,MPI_DOUBLE,MPI_SUM,0,comm);
	
	// Print out the standard deviation
	if (mytid == 0)
	{
		jitter_std = sqrt(jitter_std/comm_sz);
//		printf("Standard deviation of jittters = %e \n",jitter_std);
		printf("%e \n",jitter_std);
	}
	
  }
  MPI_Finalize();
  return 0;
}
