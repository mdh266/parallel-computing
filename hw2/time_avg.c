#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>

int main(int argc,char **argv) 
{
  int mytid;
  int comm_sz;
  MPI_Comm comm;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
  MPI_Comm_size(comm,&comm_sz);


  {
    double tstart,tstop,jitter; 
    void *sendbuf;
    int wait;

    // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;
	//printf("Jitter of process %d = %e\n", mytid, jitter);
	//printf("cmm_sz = %d\n", comm_sz);
	
    // Find the average of the jitters
    if (mytid==0)
		sendbuf = MPI_IN_PLACE; //use to specify input out buffers are same
    else 
    	sendbuf = (void*)&jitter; //otherswise sending form process address
    							  //of jitter
    	
// sendbuffer, recv buffer, size, double, opp, rank of root, communicator
    MPI_Reduce(sendbuf,(void*)&jitter,1,MPI_DOUBLE,MPI_SUM,0,comm);
    
    if (mytid==0)
      printf("Average OS jitter: %e\n",jitter/comm_sz);
    }
	
  MPI_Finalize();
  return 0;
}
