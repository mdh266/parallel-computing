#include<stdio.h>
#include<mpi.h>

int main()
{
	int mytid, ntids;
	MPI_Comm comm = MPI_COMM_WORLD;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(comm, &ntids);
	MPI_Comm_rank(comm, &mytid);
	
	printf("Hello from processor %i\n", mytid);

	MPI_Finalize();

	return 0;
}
