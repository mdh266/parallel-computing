#ifndef _CARRIER_H_
#define _CARRIER_H_

#include "includes.hpp"
#include "main.hpp"
#include "solver.hpp"


typedef struct ddpCarrier_type
{

  //////////////////////////////////////////////////////////////////////////////
  // Data Members
  ///////////////////////////////////////////////////////////////////////////////


  // Dofs
  ddpCarrierState_type carrierState;

  // Carrier Matrices
  ddpCarrierProperties_type carrierProps;

  // Weights and Points Vectors
  ddpSparseVector_type
    weightsSparse,
    PTSSparse;

  ddpDenseVector_type
    PTSDense,
    weightsDense;

  ddpDenseVector_MKL_type
  PTSDense_MKL,
    weightsDense_MKL;

  // Bijections
  ddpBijection_type Bijections;
 
 // VandeMondeMatrices, dont need if memory beocomes issue
  ddpVandeMondeMatrices_type VandeMondeMatrices;
  ddpDGFluxMatrices_type DGFluxMatrices;
 
 // Workspace vectors
  ddpDenseVector_type uNext; //used in forwardEuler to do time swtiching
  ddpDenseVector_MKL_type uNext_MKL; //used in forwardEuler to do time swtiching

 // MKL Solvers
 ddpSolver_MKL_type solverQ_MKL;
 ddpSolver_MKL_type solverU_MKL;
 


  //TODO: REMOVE THIS AFTER
  ddpDenseVector_MKL_type ElecFieldDof_MKL;

///////////////////////////////////////////////////////////////////////////////
// Data Functions
///////////////////////////////////////////////////////////////////////////////

  // Boundary Conditions Function Pointers
  double (* BCDirLeft)(double const & x);
  double (* BCDirRight)(double const & x);
  
  // Initial Condition Function Pointers
  double (* InitialConditionsU)(double const & x);

  // Recombination Model  (Assumes symmetric recomboination model)
  double RadiativeRecombination(double const & me_at_x,
				double const & other_at_x)
  {
	return carrierProps.RadRecomboConst*
	 	(me_at_x * other_at_x 
		 - (carrierProps.IntrinsicDensity *
		   carrierProps.IntrinsicDensity) );
  }

///////////////////////////////////////////////////////////////////////////////
// Structure Methods
///////////////////////////////////////////////////////////////////////////////

  int initialize(ddpGrid_type & grid,
		  ddpProblemInfo_type problem,
		  ddpCarrierConstants_type & carrierConstants,
		  char const * nameOfCarrier
		 )
  {
	// Store all the physical constants
	if(nameOfCarrier == "electrons")
	{
           carrierProps.Mobility = carrierConstants.electron_Mobility;	
  	   carrierProps.Diffusivity = 
		carrierConstants.electron_Mobility * problem.thermalVoltage;
   
 	    carrierProps.ChargeSign = carrierConstants.electron_ChargeSign;
  	    carrierProps.RecombinationTime = 
				carrierConstants.electron_RecombinationTime;
        
	    carrierProps.RadRecomboConst = problem.RadRecomboConst;
	    carrierProps.IntrinsicDensity = problem.IntrinsicDensity;
	}
	else if(nameOfCarrier == "holes")
	{
	  carrierProps.Mobility = carrierConstants.hole_Mobility;	
	  carrierProps.Diffusivity = 
	         carrierConstants.hole_Mobility * problem.thermalVoltage;

	  carrierProps.ChargeSign = carrierConstants.hole_ChargeSign;
	  carrierProps.RecombinationTime = 
			     carrierConstants.hole_RecombinationTime;
	  carrierProps.RadRecomboConst = problem.RadRecomboConst;
	  carrierProps.IntrinsicDensity = problem.IntrinsicDensity;
        }
	else
	{
		cout << "No carrier of that name." << endl;
		assert(false);
	}
	
	// set the constant for charge sign
	if(carrierProps.ChargeSign == Negative)
	{
		carrierProps.Sign4Force = - 1.0;
		carrierProps.Sign4Poisson = 1.0;
	}
	else
	{
		carrierProps.Sign4Force = 1.0; 
		carrierProps.Sign4Poisson = -1.0;
	}

        // make the weights and points
	ddpMakeWeightsAndPoints(grid, 
				problem, 
				weightsSparse,
				PTSSparse,
				PTSDense,
				weightsDense);

	PTSDense_MKL = new double[PTSDense.size()];
	weightsDense_MKL = new double[PTSDense.size()];

	// Copy into the MKL versions of weights and points.
	for(int i = 0; i < PTSDense.size(); ++i)
	  {
	    PTSDense_MKL[i] = PTSDense(i);
	    weightsDense_MKL[i] = weightsDense(i);
	  }

	

	
	// Make the bijections
	ddpMakeAllBijections(grid, Bijections);
	
	// Make the global VandeMonde Matrice and Flux Matrices 
  	ddpMakeVandeMondeMatrices(grid, 
				  problem, 
				  Bijections,
				  VandeMondeMatrices,
				  DGFluxMatrices);
	

	///////////////////////////////////////////////////////////////////////
	// Make the MKL type for the DGFluxMatrices
	///////////////////////////////////////////////////////////////////////
	int nnz = 0;
	int m = 0;
	int numRows = 0;
	
	// plusplus
	//Assign
	ddpEigenSparseToIntelCsr
	  (DGFluxMatrices.plusplus,
	   DGFluxMatrices.plusplus_MKL);
	
		

	// plusminus
	ddpEigenSparseToIntelCsr
	  (DGFluxMatrices.plusminus,
	   DGFluxMatrices.plusminus_MKL);


	//minusplus
	//Assign
	ddpEigenSparseToIntelCsr
	  (DGFluxMatrices.minusplus,
	   DGFluxMatrices.minusplus_MKL);

	// minusminus
	ddpEigenSparseToIntelCsr
	  (DGFluxMatrices.minusminus,
	   DGFluxMatrices.minusminus_MKL);
	
	
	///////////////////////////////////////////////////////////////////////
	// Make the MKL types for the VandeMonde Matrices
	///////////////////////////////////////////////////////////////////////
	
	
	// globalVandeMondDG
	//Assign
	ddpEigenSparseToIntelCsr
	  (VandeMondeMatrices.globalVandeMondeDG,
	   VandeMondeMatrices.globalVandeMondeDG_MKL);


	// globalVandeMondDGPrime
	//Assign
	ddpEigenSparseToIntelCsr
	  (VandeMondeMatrices.globalVandeMondeDGPrime,
	   VandeMondeMatrices.globalVandeMondeDGPrime_MKL);
	
	// globalVandeMondMX
	//Assign
	ddpEigenSparseToIntelCsr
	  (VandeMondeMatrices.globalVandeMondeMX,
	   VandeMondeMatrices.globalVandeMondeMX_MKL);

	// globalVandeMondMXPrime
	//Assign
	ddpEigenSparseToIntelCsr
	  (VandeMondeMatrices.globalVandeMondeMXPrime,
	   VandeMondeMatrices.globalVandeMondeMXPrime_MKL);


	// globalVandeMondeFluxMX
	//Assign
	ddpEigenSparseToIntelCsr
	  (VandeMondeMatrices.globalVandeMondeFluxMX,
	   VandeMondeMatrices.globalVandeMondeFluxMX_MKL);

	///////////////////////////////////////////////////////////////////////
	//
	////////////////////////////////////////////////////////////////////////

       
	// Make all the carrier Matrices
  	ddpMakeCarrierProperies(problem,
				grid,
				weightsSparse,
				Bijections,
				VandeMondeMatrices,
				DGFluxMatrices,
				carrierProps);

	///////////////////////////////////////////////////////////////////////
	//Make the MKL types for the carrier properties
	///////////////////////////////////////////////////////////////////////
	
	
	//MassU
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.MassU, carrierProps.MassU_MKL);


	//TotalUFromQRHS_MKL
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalUFromQRHS,
				 carrierProps.TotalUFromQRHS_MKL);

	//MassQ
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.MassQ, carrierProps.MassQ_MKL);
	
	
	//TotalQFromBCRHS
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalQFromBCRHS,
				 carrierProps.TotalQFromBCRHS_MKL);
	
	
	//TotalQFromURHS
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalQFromURHS,
				 carrierProps.TotalQFromURHS_MKL);
	
	
	
	//  cout << "before  URHS" << this->carrierProps.TotalQFromURHS << endl;
	//  cout << "before BCRHS" << this->carrierProps.TotalQFromBCRHS << endl;

	int DGDof = Bijections.DGForward.size();
	int numEndPoints = grid.NumElementsNoGhost + 1;

	// Allocate memory for carrierState Dofs and uNext
	//	carrierState.uDof = ddpDenseVector_type(DGDof);
	//	carrierState.qDof = ddpDenseVector_type(DGDof);
	//	carrierState.uDotDof = ddpDenseVector_type(DGDof);
	uNext = ddpDenseVector_type(DGDof);

	// Alocate memory for the 
	carrierState.uDof_MKL = new double[DGDof];
	carrierState.qDof_MKL = new double[DGDof];
	carrierState.uDotDof_MKL = new double[DGDof];
	uNext_MKL = new double[DGDof];
	

	// allocate memory for FOR RHSes
	carrierState.BCInput = ddpDenseVector_type(2);
	carrierState.QRHS = ddpDenseVector_type(DGDof);
	carrierState.RHSFromQ = ddpDenseVector_type(DGDof);
	carrierState.RHSFromStiff = ddpDenseVector_type(DGDof);
	carrierState.RHSFromFluxRightPOS =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromFluxRightNEG =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromFluxLeftPOS =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromFluxLeftNEG =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromBCRight =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromBCLeft =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSPartial =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSTotal =	
				ddpDenseVector_type(DGDof);
	carrierState.RHSFromRecombination =
				ddpDenseVector_type::Zero(DGDof);

	// allocate memory for FOR RHSes MKL
	carrierState.BCInput_MKL              = new double[2];
	carrierState.QRHS_MKL                 = new double[DGDof];
	carrierState.RHSFromQ_MKL             = new double[DGDof];
	carrierState.RHSFromStiff_MKL         = new double[DGDof];
	carrierState.RHSFromFluxRightPOS_MKL  = new double[DGDof];
	carrierState.RHSFromFluxRightNEG_MKL  = new double[DGDof];
	carrierState.RHSFromFluxLeftPOS_MKL   = new double[DGDof];
	carrierState.RHSFromFluxLeftNEG_MKL   = new double[DGDof];
	carrierState.RHSFromBCRight_MKL       = new double[DGDof];
	carrierState.RHSFromBCLeft_MKL        = new double[DGDof];
	carrierState.RHSPartial_MKL           = new double[DGDof];
	carrierState.RHSTotal_MKL             = new double[DGDof];
	carrierState.RHSFromRecombination_MKL = new double[DGDof];
 	carrierProps.leftBCVector_MKL 	      = new double[DGDof];	
 	carrierProps.rightBCVector_MKL 	      = new double[DGDof];	
 
	// initialize RHSFromRecombination_MKL as zeros
	for(int i = 0; i < DGDof; i++)
	{
		carrierState.RHSFromRecombination_MKL[i] = 0.0;
        }
	

	int numElements = numEndPoints-1;

	ddpDenseVector_type leftBCVector = carrierProps.DGFluxMatrices.minusplusTransposed.col(0);   
	ddpDenseVector_type rightBCVector = carrierProps.DGFluxMatrices.minusplusTransposed.col(numElements-1);   
	for(int i = 0; i < DGDof; i++)
	{
		carrierProps.leftBCVector_MKL[i] = leftBCVector(i);
		carrierProps.rightBCVector_MKL[i] = rightBCVector(i);
	}

	// allocate memory for work space vectors
	carrierState.ElecPTS = ddpDenseVector_type(PTSDense.size());
	carrierState.ElecEndPoints = ddpDenseVector_type(numEndPoints);
	carrierState.PosElecEndPoints = ddpDenseVector_type(numEndPoints);
	carrierState.NegElecEndPoints = ddpDenseVector_type(numEndPoints);
	carrierState.PosElecEndPTSNoEnd = ddpDenseVector_type(numEndPoints-1);
	carrierState.NegElecEndPTSNoEnd = ddpDenseVector_type(numEndPoints-1);
	carrierState.ElecPTSTimesWeights = ddpDenseVector_type(PTSDense.size());
	
	// allocate memory for the work space vectors MKL
	carrierState.ElecPTS_MKL            = new double[PTSDense.size()];
	carrierState.ElecEndPoints_MKL      = new double[numEndPoints];
	carrierState.PosElecEndPoints_MKL   = new double[numEndPoints];
	carrierState.NegElecEndPoints_MKL   = new double[numEndPoints]; 
	carrierState.PosElecEndPTSNoEnd_MKL = new double[numEndPoints - 1];
	carrierState.NegElecEndPTSNoEnd_MKL = new double[numEndPoints - 1];
	
	carrierState.ElecPTSTimesWeights_MKL = new double[PTSDense.size()];


	// allocate memory for the work space matrices
	carrierState.ElecTimesWeightsMatrix = 
				ddpDiagonalMatrix_type(weightsDense.size() );
   	carrierState.PosElecEndPTSNoEndMatrix =
				ddpDiagonalMatrix_type(numEndPoints-1);
   	carrierState.NegElecEndPTSNoEndMatrix =
				ddpDiagonalMatrix_type(numEndPoints-1);

	// allocate memory for work space vectors
	carrierState.workSpaceVector0_MKL = new double[PTSDense.size()];
	carrierState.workSpaceVector1_MKL = new double[PTSDense.size()];

	// Set the carrier solvers
	carrierState.solverU.compute(carrierProps.MassU);
	carrierState.solverQ.compute(carrierProps.MassQ);  
	
	// Set the MKL solvers
	solverU_MKL.initialize(carrierProps.MassU_MKL);
	solverQ_MKL.initialize(carrierProps.MassQ_MKL);


	carrierProps.numDGDof_MKL = DGDof;
	carrierProps.increment_MKL = 1;
	carrierProps.numPoints_MKL = PTSDense.size();
	carrierProps.numEndPoints_MKL = numEndPoints;
	carrierProps.numEndPointsNoEnd_MKL = numEndPoints-1;


	//TODO: REMOVE THIS
	 ElecFieldDof_MKL = new double[DGDof];
	
	return 0;
  }

int remakeProperties(ddpGrid_type const & grid, 
		     ddpProblemInfo_type const & problem)
{
	// Make all the carrier Matrices
	// This is used for testing purposes;
  	ddpMakeCarrierProperies(problem,
			  grid,
			  weightsSparse,
			  Bijections,
			  VandeMondeMatrices,
			  DGFluxMatrices,
			  carrierProps);

//  cout << "after  URHS" << this->carrierProps.TotalQFromURHS << endl;
//  cout << "after BCRHS" << this->carrierProps.TotalQFromBCRHS << endl;

	// Set the carrier solvers
	carrierState.solverU.compute(carrierProps.MassU);
	carrierState.solverQ.compute(carrierProps.MassQ);  
 
	///////////////////////////////////////////////////////////////////////
	// ReMake the MKL types for the carrier properties
	///////////////////////////////////////////////////////////////////////
	
	
	//MassU
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.MassU, carrierProps.MassU_MKL);


	//TotalUFromQRHS_MKL
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalUFromQRHS,
				 carrierProps.TotalUFromQRHS_MKL);

	//MassQ
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.MassQ, carrierProps.MassQ_MKL);
	
	
	//TotalQFromBCRHS
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalQFromBCRHS,
				 carrierProps.TotalQFromBCRHS_MKL);
	
	
	//TotalQFromURHS
	//Assign
	ddpEigenSparseToIntelCsr(carrierProps.TotalQFromURHS,
				 carrierProps.TotalQFromURHS_MKL);
	
	// Set the MKL solvers
	solverU_MKL.initialize(carrierProps.MassU_MKL);
	solverQ_MKL.initialize(carrierProps.MassQ_MKL);

  return 0;
}

  
  int setDirichletBoundaryConditions( double (* BCLeft) (const double & x), 
		 	    	    double (* BCRight) (const double & x)
			   	  )
  { 
	BCDirLeft = BCLeft;
	BCDirRight = BCRight;
	return 0;
  }	

  int setInitialConditions( ddpGrid_type & grid,
			    double (* InitialFunction) (const double & t) )
  {
	// set this carriers initial condition function to InitialFunction
	InitialConditionsU = InitialFunction;
        
        ddpBijFlag_type BijFlag = DG;

	// Project this carrier's initial condition function onto the basis
	// and store result in this carriers.UDof
        ddpProjectFunction(InitialConditionsU,
			   grid,
			   Bijections.DGForward,
			   carrierProps.VandeMondeMatrices.globalVandeMondeDG,
			   Bijections.PTForward,
			   BijFlag,
			   weightsSparse,
			   carrierProps.MassU,
			   carrierState.uDof
			   );
	
 	/// Copy over to the MKL Versiojn	
	for(int i = 0; i < carrierState.uDof.size(); ++i)
	  {
	    carrierState.uDof_MKL[i]  = carrierState.uDof(i);
	  }
  }

 int updateRecombination(ddpCarrier_type const & other)
  {
    // get carriers own point values
    ddpDenseVector_type 
      ThisCarriersPTValues 
      = VandeMondeMatrices.globalVandeMondeDG * carrierState.uDof,
      OtherCarriersPTValues
      = other.VandeMondeMatrices.globalVandeMondeDG *other.carrierState.uDof,
      RecomboPTSTimesWeights 
      = ddpDenseVector_type(weightsDense.size());

    
    grvy_timer_begin("Recombination assemble");
    // Get recombination point values times weights
    for(int i = 0; i < weightsDense.size(); i++)
    {
	RecomboPTSTimesWeights(i) = weightsDense(i) *
	  RadiativeRecombination(ThisCarriersPTValues(i),
						          OtherCarriersPTValues(i)); 
    }
    grvy_timer_end("Recombination assemble");

    grvy_timer_begin("Recombination product");
    carrierState.RHSFromRecombination = 
			VandeMondeMatrices.globalVandeMondeDGTransposed * 
			RecomboPTSTimesWeights;
    grvy_timer_end("Recombination product");
	
  //  cout << "RecomboPTSTimesWeights = " << RecomboPTSTimesWeights << endl;
    return 0;
 }



int makeDot_MKL(ddpDenseVector_MKL_type const & ElecFieldDof_MKL,
	    double const & timeCurrent)
{

  // update Boundary condtions
  carrierState.BCInput_MKL[0] = BCDirLeft(timeCurrent);
  carrierState.BCInput_MKL[1] = BCDirRight(timeCurrent);


  grvy_timer_begin("MKL Solve For Q");
  
  char transa = 'N';

  char matsecra[6];
  matsecra[0] = 'G';
  matsecra[1] = 'L';
  matsecra[2] = 'N';
  matsecra[3] = 'C';

  
  int numEndPoints = carrierState.PosElecEndPoints.size();
  int numElements = numEndPoints-1;
  
  double alpha = 1.0;
  double beta = 1.0;


  // compute carrierProp.TotalQFromBCRHS_MKL * carrierState.BCInput_MKL and put the result
  // in carrierState.QRHS_MKL
  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.TotalQFromBCRHS_MKL.numRows,
		       carrierProps.TotalQFromBCRHS_MKL.values,
		       carrierProps.TotalQFromBCRHS_MKL.I,
		       carrierProps.TotalQFromBCRHS_MKL.J,
		       carrierState.BCInput_MKL,
		       carrierState.QRHS_MKL
		       );

  
  // compute carrierProps.TotalQFromURHS_MKL * carrierState.uDof_MKL  + carrierState.QRHS_MKL
  // and put the result in carrierState.QRHS_MKL 

  mkl_dcsrmv(  
	     &transa,
	     &carrierProps.TotalQFromURHS_MKL.numRows,
	     &carrierProps.TotalQFromURHS_MKL.numCols,
	     &alpha,
	     matsecra,
	     carrierProps.TotalQFromURHS_MKL.values,
	     carrierProps.TotalQFromURHS_MKL.J,
	     carrierProps.TotalQFromURHS_MKL.I,
	     carrierProps.TotalQFromURHS_MKL.I + 1,
	     carrierState.uDof_MKL,
	     &beta,
	     carrierState.QRHS_MKL
	     );
 

  // solve for qDof_MKL in MassQ_MKL * qDof_MKL = QRHS_MKL
  solverQ_MKL.solve(carrierProps.MassQ_MKL, carrierState.QRHS_MKL, carrierState.qDof_MKL);

  grvy_timer_end("MKL Solve For Q");


   // Compute contribution for qCurrent;
  grvy_timer_begin("MKL RHSFromQ");


  // RHSFromQ = TotalUFromQRHS * qDOf 
  //MKL Version
  transa = 'N';
  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.TotalUFromQRHS_MKL.numRows,
		       carrierProps.TotalUFromQRHS_MKL.values,
		       carrierProps.TotalUFromQRHS_MKL.I,
		       carrierProps.TotalUFromQRHS_MKL.J,
		       carrierState.qDof_MKL,
		       carrierState.RHSFromQ_MKL
		       );


  grvy_timer_end("MKL RHSFromQ");


///////////////////////////////////////////////////////////////////////////////
// Get the "Force" of electric field point values
/////////////////////////////////////////////////////////////////////////////// 

  
 // Electric field point values
  grvy_timer_begin("MKL ElecPTS");
 
   
  // compute carrierProps.Sign4Force * VandeMondeMatrices.globalVandeMondeMX *
  // 			ElecFieldDof and put it into ElecPTS_MKL
 mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.numRows,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.values,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.I,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.J,
		       ElecFieldDof_MKL,
		       carrierState.ElecPTS_MKL
		       );
 
 // Now compute ElecPTS_MKL *= sign4Force
 cblas_dscal(carrierProps.numPoints_MKL,
	     carrierProps.Sign4Force,
	     carrierState.ElecPTS_MKL,
	     carrierProps.increment_MKL);



  grvy_timer_end("MKL ElecPTS");
 
  // Electric field point values at the end point of cells
  grvy_timer_begin("MKL ElecEndPoints");

  
 /// create ElecEndPts = globalVandeMondeMX * ElecFieldDof

  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.numRows,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.values,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.I,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.J,
		       ElecFieldDof_MKL,
		       carrierState.ElecEndPoints_MKL
		      );

  // Now compute ElecEndPoints_MKL *= sign4Force
  cblas_dscal(carrierProps.numEndPoints_MKL,
	     carrierProps.Sign4Force,
	     carrierState.ElecEndPoints_MKL,
	     carrierProps.increment_MKL);

  grvy_timer_end("MKL ElecEndPoints");

  // Get the Positive and negative parts
  grvy_timer_begin("MKL Positive and Negative Parts");
  ddpPositivePart(carrierState.ElecEndPoints_MKL, carrierState.PosElecEndPoints_MKL, numEndPoints);
  ddpNegativePart(carrierState.ElecEndPoints_MKL, carrierState.NegElecEndPoints_MKL, numEndPoints);
  grvy_timer_end("MKL Positive and Negative Parts");

  /////////////////////////////////////////////////////////////////////////////
  // Compute contribution from "stiffness term"
  /////////////////////////////////////////////////////////////////////////////

  grvy_timer_begin("MKL RHSStiff"); 
  //Compenent-wise multiplication of weightsDense_MKL and carrierState.ElecPTS_MKL 
  //to produce ElectPTSTimesWeights_MKL
  
  vdMul(
	 weightsDense.size(),
	 weightsDense_MKL,
	 carrierState.ElecPTS_MKL, 
	 carrierState.ElecPTSTimesWeights_MKL
       ); 

   //compute carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL times
   // carrierState.uDof_MKL, put the result in  carrierState.workSpaceVector0_MKL
   
   mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.numRows,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.values,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.I,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
   

   // Component-wise multiplication of workSpaceVector0_MKL and ElecPTSTTimesWeights_MKL
   // put the result in workSpaceVector1_MKL
    
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.ElecPTSTimesWeights_MKL,
	  carrierState.workSpaceVector1_MKL
	  );
    
   // compute Transpose(carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL) * worksSpaceVector1_MKL
   // put the result in carrierState.RHSFromStiff_MKL

    transa = 'T';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.numRows,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.values,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.I,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.J,
			carrierState.workSpaceVector1_MKL,
			carrierState.RHSFromStiff_MKL
			);


  grvy_timer_end("MKL RHSStiff"); 

 /////////////////////////////////////////////////////////////////////////////
  // Compute contribution from "flux Right term"
  /////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("MKL RHSFluxRight");
  
  // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.PosElecEndPoints_MKL+1,
	       carrierProps.increment_MKL,
	       carrierState.PosElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
  

   // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.NegElecEndPoints_MKL+1,
	       carrierProps.increment_MKL,
	       carrierState.NegElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
	


    // workSpaceVector0_MKL = plusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
			carrierProps.DGFluxMatrices.plusminus_MKL.values,
			carrierProps.DGFluxMatrices.plusminus_MKL.I,
			carrierProps.DGFluxMatrices.plusminus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = PosElecEndPtsNoEndMatrix * plusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.PosElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );

   // RHSFromFluxRightPOS_MKL = plusminus^T * PosElecEndPtsNoEndMatrix * plusminus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;

    mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.plusminus_MKL.values,
             carrierProps.DGFluxMatrices.plusminus_MKL.J,
             carrierProps.DGFluxMatrices.plusminus_MKL.I,
             carrierProps.DGFluxMatrices.plusminus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxRightPOS_MKL
             );

    // workSpaceVector0_MKL = plusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.plusplus_MKL.numRows,
			carrierProps.DGFluxMatrices.plusplus_MKL.values,
			carrierProps.DGFluxMatrices.plusplus_MKL.I,
			carrierProps.DGFluxMatrices.plusplus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
 
 
   // workSpaceVector1_MKL  = NegElecEndPtsNoEndMatrix * plusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.NegElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );


  // RHSFromFluxRightNEG_MKL = plusminus^T * NegElecEndPtsNoEndMatrix * plusplus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.plusminus_MKL.values,
             carrierProps.DGFluxMatrices.plusminus_MKL.J,
             carrierProps.DGFluxMatrices.plusminus_MKL.I,
             carrierProps.DGFluxMatrices.plusminus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxRightNEG_MKL
             );

   grvy_timer_end("MKL RHSFluxRight");
   
  
   /////////////////////////////////////////////////////////////////////////////
   // Compute contribution from "flux Left term"
   /////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("MKL RHSFluxLeft");   
 
 
  // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.PosElecEndPoints_MKL,
	       carrierProps.increment_MKL,
	       carrierState.PosElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL); 
 
   // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.NegElecEndPoints_MKL,
	       carrierProps.increment_MKL,
	       carrierState.NegElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
  
    // workSpaceVector0_MKL = minusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.minusminus_MKL.numRows,
			carrierProps.DGFluxMatrices.minusminus_MKL.values,
			carrierProps.DGFluxMatrices.minusminus_MKL.I,
			carrierProps.DGFluxMatrices.minusminus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = PosElecEndPtsNoEndMatrix * minusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.PosElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );


   // RHSFromFluxRightPOS_MKL = minusplus^T * POSElecEndPtsNoEndMatrix * minusminus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.minusplus_MKL.values,
             carrierProps.DGFluxMatrices.minusplus_MKL.J,
             carrierProps.DGFluxMatrices.minusplus_MKL.I,
             carrierProps.DGFluxMatrices.minusplus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxLeftPOS_MKL
             );

   
    // workSpaceVector0_MKL = minusplus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
			carrierProps.DGFluxMatrices.minusplus_MKL.values,
			carrierProps.DGFluxMatrices.minusplus_MKL.I,
			carrierProps.DGFluxMatrices.minusplus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = NEGElecEndPtsNoEndMatrix * minusplus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.NegElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );


   // RHSFromFluxRightPOS_MKL = minusplus^T * NEGElecEndPtsNoEndMatrix * minusplus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.minusplus_MKL.values,
             carrierProps.DGFluxMatrices.minusplus_MKL.J,
             carrierProps.DGFluxMatrices.minusplus_MKL.I,
             carrierProps.DGFluxMatrices.minusplus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxLeftNEG_MKL
             );
  
 
   grvy_timer_end("MKL RHSFluxLeft");
   
   
   /////////////////////////////////////////////////////////////////////////////
   // Compute contribution from "Boundary conditions"
   /////////////////////////////////////////////////////////////////////////////
   

   grvy_timer_begin("MKL Boundary Conditions");

 
    //MKL VERSION 
    alpha = BCDirLeft(timeCurrent) * carrierState.PosElecEndPoints_MKL[0];
    
    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierProps.leftBCVector_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSFromBCLeft_MKL,
		carrierProps.increment_MKL
	       );

    cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSFromBCLeft_MKL,
	       carrierProps.increment_MKL
	      );

    

    alpha = BCDirRight(timeCurrent) * carrierState.PosElecEndPoints_MKL[numElements-1];
    
    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierProps.rightBCVector_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSFromBCRight_MKL,
		carrierProps.increment_MKL
	       );

    cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSFromBCRight_MKL,
	       carrierProps.increment_MKL
	      );

 
   grvy_timer_end("MKL Boundary Conditions");


   grvy_timer_begin("MKL Summing RHSTotal");
 
    // MKL Version
    // RHS_Partial_MKL = RHSFromStiff
    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierState.RHSFromStiff_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSPartial_MKL,
		carrierProps.increment_MKL
	       );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxRightPOS_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );
  
   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxRightNEG_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxLeftPOS_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );


   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxLeftNEG_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );


   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   //		      - RHSFromBCRight 
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromBCRight_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   //		      - RHSFromBCRight + RHSFromBCLeft
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromBCLeft_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );
   
   // Update Recombination Dof Values  
   // MKL Version
   
   // RHSTotal_MKL = mobility * RHSPartial_MKL		
   cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierState.RHSPartial_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSTotal_MKL,
		carrierProps.increment_MKL
	       );

   alpha = carrierProps.Mobility;
   
   cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSTotal_MKL,
	       carrierProps.increment_MKL
	       );
	     
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromQ_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSTotal_MKL,
	      carrierProps.increment_MKL
	     );

   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromRecombination_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSTotal_MKL,
	      carrierProps.increment_MKL
	     );

  grvy_timer_end("MKL Summing RHSTotal");

   ////////////////////////////////////////////////////////////////////////////
   // Solve
   ////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("MKL Solve for U");

   solverU_MKL.solve(carrierProps.MassU_MKL, carrierState.RHSTotal_MKL, carrierState.uDotDof_MKL); 
   
   grvy_timer_end("MKL Solve for U");

 return 0;
}

	
int
makeDot(ddpDenseVector_type const & ElecFieldDof,
	double const & timeCurrent)
{
 
 ///////////////////////////////////////////////////////////////////////////////
 // Compute QRHS
 ////////////////////////////////////////////////////////////////////////////// 
  
  int numEndPoints = carrierState.PosElecEndPoints.size();
  int numElements = numEndPoints-1;

/*  // copy the ElecFieldDof
  for(int i = 0; i < ElecFieldDof.size(); i++)
  {
	ElecFieldDof_MKL[i] = ElecFieldDof(i);
  }
*/

  // update Boundary condtions
  
  carrierState.BCInput(0) = BCDirLeft(timeCurrent);
  carrierState.BCInput(1) = BCDirRight(timeCurrent);

/*
  carrierState.BCInput_MKL[0] = BCDirLeft(timeCurrent);
  carrierState.BCInput_MKL[1] = BCDirRight(timeCurrent);
*/

  grvy_timer_begin("Solve For Q");
  
  // create carrierState.QRHS_MKL
  carrierState.QRHS =  carrierProps.TotalQFromURHS * carrierState.uDof 
		     + carrierProps.TotalQFromBCRHS * carrierState.BCInput;

// char transa = 'N';

  //carrierProps.TotalQFromBCRHS_MKL.
  
//  MKL_INT numRows = carrierProps.TotalQFromBCRHS_MKL.numRows;
//  MKL_INT numCols = carrierProps.TotalQFromBCRHS_MKL.numCols;
  
 /*  
  char matsecra[6];
  matsecra[0] = 'G';
  matsecra[1] = 'L';
  matsecra[2] = 'N';
  matsecra[3] = 'C';

  
  // compute carrierProp.TotalQFromBCRHS_MKL * carrierState.BCInput_MKL and put the result
  // in carrierState.QRHS_MKL
  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.TotalQFromBCRHS_MKL.numRows,
		       carrierProps.TotalQFromBCRHS_MKL.values,
		       carrierProps.TotalQFromBCRHS_MKL.I,
		       carrierProps.TotalQFromBCRHS_MKL.J,
		       carrierState.BCInput_MKL,
		       carrierState.QRHS_MKL
		       );


  double alpha = 1.0;
  double beta = 1.0;
 
  int numDof = carrierState.uDof.size();
  
  // compute carrierProps.TotalQFromURHS_MKL * carrierState.uDof_MKL  + carrierState.QRHS_MKL
  // and put the result in carrierState.QRHS_MKL 

  mkl_dcsrmv(  
	     &transa,
	     &carrierProps.TotalQFromURHS_MKL.numRows,
	     &carrierProps.TotalQFromURHS_MKL.numCols,
	     &alpha,
	     matsecra,
	     carrierProps.TotalQFromURHS_MKL.values,
	     carrierProps.TotalQFromURHS_MKL.J,
	     carrierProps.TotalQFromURHS_MKL.I,
	     carrierProps.TotalQFromURHS_MKL.I + 1,
	     carrierState.uDof_MKL,
	     &beta,
	     carrierState.QRHS_MKL
	     );

// cout << "TotalQFromURHS" <<  carrierProps.TotalQFromURHS << endl;
 
*/
  // TODO: REMOVE WHEN DONE
//  isSame(carrierState.QRHS, carrierState.QRHS_MKL);


  // Eigen call
  carrierState.qDof = carrierState.solverQ.solve(carrierState.QRHS);
 
  // MKL call
//  solverQ_MKL.solve(carrierProps.MassQ_MKL, carrierState.QRHS_MKL, carrierState.qDof_MKL);

//  isSame(carrierState.qDof,carrierState.qDof_MKL);


  
  // TODO: Check the solve
  grvy_timer_end("Solve For Q");
  
   // Compute contribution for qCurrent;
  grvy_timer_begin("RHSFromQ");
  carrierState.RHSFromQ = carrierProps.TotalUFromQRHS * carrierState.qDof;
/* 
  //MKL Version
  transa = 'N';
  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.TotalUFromQRHS_MKL.numRows,
		       carrierProps.TotalUFromQRHS_MKL.values,
		       carrierProps.TotalUFromQRHS_MKL.I,
		       carrierProps.TotalUFromQRHS_MKL.J,
		       carrierState.qDof_MKL,
		       carrierState.RHSFromQ_MKL
		       );
*/

  grvy_timer_end("RHSFromQ");
 

///////////////////////////////////////////////////////////////////////////////
// Get the "Force" of electric field point values
/////////////////////////////////////////////////////////////////////////////// 

  
 // Electric field point values
  grvy_timer_begin("ElecPTS");
 
  carrierState.ElecPTS = 
    carrierProps.Sign4Force * VandeMondeMatrices.globalVandeMondeMX
    * ElecFieldDof;
   
  // compute carrierProps.Sign4Force * VandeMondeMatrices.globalVandeMondeMX *
  // 			ElecFieldDof and put it into ElecPTS_MKL
/*
  
 mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.numRows,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.values,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.I,
		       carrierProps.VandeMondeMatrices.globalVandeMondeMX_MKL.J,
		       ElecFieldDof_MKL,
		       carrierState.ElecPTS_MKL
		       );
 
 // Now compute ElecPTS_MKL *= sign4Force
 cblas_dscal(carrierProps.numPoints_MKL,
	     carrierProps.Sign4Force,
	     carrierState.ElecPTS_MKL,
	     carrierProps.increment_MKL);

*/
  // TODO: REMOVE WHEN DONE
 // isSame(carrierState.ElecPTS, carrierState.ElecPTS_MKL);

  grvy_timer_end("ElecPTS");
 
  // Electric field point values at the end point of cells
  grvy_timer_begin("ElecEndPoints");
  
 carrierState.ElecEndPoints 
    = carrierProps.Sign4Force * VandeMondeMatrices.globalVandeMondeFluxMX
    * ElecFieldDof;
/*
 /// create ElecEndPts = globalVandeMondeMX * ElecFieldDof

  mkl_cspblas_dcsrgemv(
		       &transa,
		       &carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.numRows,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.values,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.I,
		       carrierProps.VandeMondeMatrices.globalVandeMondeFluxMX_MKL.J,
		       ElecFieldDof_MKL,
		       carrierState.ElecEndPoints_MKL
		      );

  // Now compute ElecEndPoints_MKL *= sign4Force
  cblas_dscal(carrierProps.numEndPoints_MKL,
	     carrierProps.Sign4Force,
	     carrierState.ElecEndPoints_MKL,
	     carrierProps.increment_MKL);

*/  // TODO: REMOVE WHEN DONE 
//  isSame(carrierState.ElecEndPoints, carrierState.ElecEndPoints_MKL);

  grvy_timer_end("ElecEndPoints");
  
//  int numEndPoints = carrierState.PosElecEndPoints.size();
//  int numElements = numEndPoints-1;
  
  // Get the Positive and negative parts
  grvy_timer_begin("Positive and Negative Parts");
  ddpPositivePart(carrierState.ElecEndPoints, carrierState.PosElecEndPoints);
  ddpNegativePart(carrierState.ElecEndPoints, carrierState.NegElecEndPoints);

/*  // MKL VERSIONS
  ddpPositivePart(carrierState.ElecEndPoints_MKL, carrierState.PosElecEndPoints_MKL, numEndPoints);
  ddpNegativePart(carrierState.ElecEndPoints_MKL, carrierState.NegElecEndPoints_MKL, numEndPoints);
*/
  grvy_timer_end("Positive and Negative Parts");

//  isSame(carrierState.PosElecEndPoints,carrierState.PosElecEndPoints_MKL);
 // isSame(carrierState.NegElecEndPoints,carrierState.NegElecEndPoints_MKL);

  
  
  /////////////////////////////////////////////////////////////////////////////
  // Compute contribution from "stiffness term"
  /////////////////////////////////////////////////////////////////////////////
  
   // Point wise multiplication of quadrature weights times electric field
   // quadrature pointvalues
  carrierState.ElecPTSTimesWeights =
			 carrierState.ElecPTS.cwiseProduct(weightsDense);
  
  
  //Compenent-wise multiplication of weightsDense_MKL and carrierState.ElecPTS_MKL 
  //to produce ElectPTSTimesWeights_MKL
/*  
  vdMul(
	 weightsDense.size(),
	 weightsDense_MKL,
	 carrierState.ElecPTS_MKL, 
	 carrierState.ElecPTSTimesWeights_MKL
       ); 
 
   // TODO:  REMOVE WHEN DONE
*/
 // isSame(carrierState.ElecPTSTimesWeights, carrierState.ElecPTSTimesWeights_MKL);

  // Makes diagonal matrices of the previous DenseVector
  carrierState.ElecTimesWeightsMatrix = 
			carrierState.ElecPTSTimesWeights.asDiagonal();
  
  
  grvy_timer_begin("RHSStiff"); 
   carrierState.RHSFromStiff =     
     (carrierProps.VandeMondeMatrices.globalVandeMondeDGPrimeTransposed
      * (carrierState.ElecTimesWeightsMatrix 
	 * (carrierProps.VandeMondeMatrices.globalVandeMondeDG
	    * carrierState.uDof
	    )
	 )
      );
   grvy_timer_end("RHSStiff");   

/*   //compute carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL times
   // carrierState.uDof_MKL, put the result in  carrierState.workSpaceVector0_MKL
   
   mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.numRows,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.values,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.I,
			carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
   

   // Component-wise multiplication of workSpaceVector0_MKL and ElecPTSTTimesWeights_MKL
   // put the result in workSpaceVector1_MKL
   
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.ElecPTSTimesWeights_MKL,
	  carrierState.workSpaceVector1_MKL
	  );
    
   // compute Transpose(carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL) * worksSpaceVector1_MKL
   // put the result in carrierState.RHSFromStiff_MKL

    transa = 'T';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.numRows,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.values,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.I,
			carrierProps.VandeMondeMatrices.globalVandeMondeDGPrime_MKL.J,
			carrierState.workSpaceVector1_MKL,
			carrierState.RHSFromStiff_MKL
			);
*/    
  // TODO: // REMOVE WHEN DONE
//  isSame(carrierState.RHSFromStiff, carrierState.RHSFromStiff_MKL); 


 /////////////////////////////////////////////////////////////////////////////
  // Compute contribution from "flux Right term"
  /////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("RHSFluxRight");
   carrierState.PosElecEndPTSNoEnd = 
		carrierState.PosElecEndPoints.segment(1, numEndPoints - 1);
   carrierState.NegElecEndPTSNoEnd = 
		carrierState.NegElecEndPoints.segment(1, numEndPoints - 1);
/*  
  // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.PosElecEndPoints_MKL+1,
	       carrierProps.increment_MKL,
	       carrierState.PosElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
  

   // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.NegElecEndPoints_MKL+1,
	       carrierProps.increment_MKL,
	       carrierState.NegElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
	

   // TODO REMOVE WHEN DONE
  isSame(carrierState.PosElecEndPTSNoEnd, carrierState.PosElecEndPTSNoEnd_MKL);
  isSame(carrierState.NegElecEndPTSNoEnd, carrierState.NegElecEndPTSNoEnd_MKL);
*/

   carrierState.PosElecEndPTSNoEndMatrix 
		= carrierState.PosElecEndPTSNoEnd.asDiagonal(),
   carrierState. NegElecEndPTSNoEndMatrix 
		= carrierState.NegElecEndPTSNoEnd.asDiagonal();
 
   carrierState.RHSFromFluxRightPOS =
     carrierProps.DGFluxMatrices.plusminusTransposed * //test function
     (carrierState.PosElecEndPTSNoEndMatrix  
      * (carrierProps.DGFluxMatrices.plusminus // trial funciton
	 * carrierState.uDof
	 )
      );

/*
    // workSpaceVector0_MKL = plusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
			carrierProps.DGFluxMatrices.plusminus_MKL.values,
			carrierProps.DGFluxMatrices.plusminus_MKL.I,
			carrierProps.DGFluxMatrices.plusminus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = PosElecEndPtsNoEndMatrix * plusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.PosElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );

   // RHSFromFluxRightPOS_MKL = plusminus^T * PosElecEndPtsNoEndMatrix * plusminus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.plusminus_MKL.values,
             carrierProps.DGFluxMatrices.plusminus_MKL.J,
             carrierProps.DGFluxMatrices.plusminus_MKL.I,
             carrierProps.DGFluxMatrices.plusminus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxRightPOS_MKL
             );

  
   isSame(carrierState.RHSFromFluxRightPOS, carrierState.RHSFromFluxRightPOS_MKL);
*/
   
  carrierState.RHSFromFluxRightNEG =
     carrierProps.DGFluxMatrices.plusminusTransposed * // test function
     (carrierState.NegElecEndPTSNoEndMatrix 
      * (carrierProps.DGFluxMatrices.plusplus // trial function
	 * carrierState.uDof
	 )
      );

/*					 

    // workSpaceVector0_MKL = plusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.plusplus_MKL.numRows,
			carrierProps.DGFluxMatrices.plusplus_MKL.values,
			carrierProps.DGFluxMatrices.plusplus_MKL.I,
			carrierProps.DGFluxMatrices.plusplus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
 
 
   // workSpaceVector1_MKL  = NegElecEndPtsNoEndMatrix * plusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.NegElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );



  // RHSFromFluxRightNEG_MKL = plusminus^T * NegElecEndPtsNoEndMatrix * plusplus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numRows,
             &carrierProps.DGFluxMatrices.plusminus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.plusminus_MKL.values,
             carrierProps.DGFluxMatrices.plusminus_MKL.J,
             carrierProps.DGFluxMatrices.plusminus_MKL.I,
             carrierProps.DGFluxMatrices.plusminus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxRightNEG_MKL
             );


    //TODO: REMOVE WHEN DONE
   isSame(carrierState.RHSFromFluxRightNEG, carrierState.RHSFromFluxRightNEG_MKL);
*/   
   grvy_timer_end("RHSFluxRight");
   
  
   /////////////////////////////////////////////////////////////////////////////
   // Compute contribution from "flux Left term"
   /////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("RHSFluxLeft");   
   carrierState.PosElecEndPTSNoEnd = 
		carrierState.PosElecEndPoints.segment(0, numEndPoints - 1);
   carrierState.NegElecEndPTSNoEnd = 
		carrierState.NegElecEndPoints.segment(0, numEndPoints - 1);
   
   carrierState.PosElecEndPTSNoEndMatrix 
		= carrierState.PosElecEndPTSNoEnd.asDiagonal(),
   carrierState. NegElecEndPTSNoEndMatrix 
		= carrierState.NegElecEndPTSNoEnd.asDiagonal();
 
/* 
  // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.PosElecEndPoints_MKL,
	       carrierProps.increment_MKL,
	       carrierState.PosElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL); 
 
   // MKL Version
  cblas_dcopy(carrierProps.numEndPointsNoEnd_MKL,
	      carrierState.NegElecEndPoints_MKL,
	       carrierProps.increment_MKL,
	       carrierState.NegElecEndPTSNoEnd_MKL,
	       carrierProps.increment_MKL);
  
   // TODO REMOVE WHEN DONE
  isSame(carrierState.PosElecEndPTSNoEnd, carrierState.PosElecEndPTSNoEnd_MKL);
  isSame(carrierState.NegElecEndPTSNoEnd, carrierState.NegElecEndPTSNoEnd_MKL);

*/
 
   carrierState.RHSFromFluxLeftPOS =
     (carrierProps.DGFluxMatrices.minusplusTransposed) * // test function
       (carrierState.PosElecEndPTSNoEndMatrix 
	* (carrierProps.DGFluxMatrices.minusminus // trial function
	   * carrierState.uDof
	   )
	);

/*
    // workSpaceVector0_MKL = minusminus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.minusminus_MKL.numRows,
			carrierProps.DGFluxMatrices.minusminus_MKL.values,
			carrierProps.DGFluxMatrices.minusminus_MKL.I,
			carrierProps.DGFluxMatrices.minusminus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = PosElecEndPtsNoEndMatrix * minusminus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.PosElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );


   // RHSFromFluxRightPOS_MKL = minusplus^T * POSElecEndPtsNoEndMatrix * minusminus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.minusplus_MKL.values,
             carrierProps.DGFluxMatrices.minusplus_MKL.J,
             carrierProps.DGFluxMatrices.minusplus_MKL.I,
             carrierProps.DGFluxMatrices.minusplus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxLeftPOS_MKL
             );

  //TODO: remove when done 
  isSame(carrierState.RHSFromFluxLeftPOS, carrierState.RHSFromFluxLeftPOS_MKL);

*/
   carrierState.RHSFromFluxLeftNEG =
     carrierProps.DGFluxMatrices.minusplusTransposed * // test function
     (carrierState.NegElecEndPTSNoEndMatrix 
      * (carrierProps.DGFluxMatrices.minusplus // trial function
	 * carrierState.uDof
	 )
      );
/*   
    // workSpaceVector0_MKL = minusplus * uDof
    transa = 'N';
    mkl_cspblas_dcsrgemv(
			&transa,
			&carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
			carrierProps.DGFluxMatrices.minusplus_MKL.values,
			carrierProps.DGFluxMatrices.minusplus_MKL.I,
			carrierProps.DGFluxMatrices.minusplus_MKL.J,
			carrierState.uDof_MKL,
			carrierState.workSpaceVector0_MKL
			);
  
   // workSpaceVector1_MKL  = NEGElecEndPtsNoEndMatrix * minusplus * uDof
    vdMul(
	  weightsDense.size(),
	  carrierState.workSpaceVector0_MKL,
	  carrierState.NegElecEndPTSNoEnd_MKL,
	  carrierState.workSpaceVector1_MKL
	  );


   // RHSFromFluxRightPOS_MKL = minusplus^T * NEGElecEndPtsNoEndMatrix * minusplus * uDof
  transa = 'T';
  alpha = 1.0;
  beta = 0.0;
  mkl_dcsrmv(
             &transa,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numRows,
             &carrierProps.DGFluxMatrices.minusplus_MKL.numCols,
             &alpha,
             matsecra,
             carrierProps.DGFluxMatrices.minusplus_MKL.values,
             carrierProps.DGFluxMatrices.minusplus_MKL.J,
             carrierProps.DGFluxMatrices.minusplus_MKL.I,
             carrierProps.DGFluxMatrices.minusplus_MKL.I + 1,
	     carrierState.workSpaceVector1_MKL,
             &beta,
	     carrierState.RHSFromFluxLeftNEG_MKL
             );
  
  //TODO: REMOVE WHEN DONE  
  isSame(carrierState.RHSFromFluxLeftNEG, carrierState.RHSFromFluxLeftNEG_MKL);
 */
   grvy_timer_end("RHSFluxLeft");
   
   
   /////////////////////////////////////////////////////////////////////////////
   // Compute contribution from "Boundary conditions"
   /////////////////////////////////////////////////////////////////////////////
   

   grvy_timer_begin("Boundary Conditions");


   carrierState.RHSFromBCLeft 
     = (BCDirLeft(timeCurrent) * carrierState.PosElecEndPoints(0))
     * (carrierProps.DGFluxMatrices.minusplusTransposed).col(0);   
   
   carrierState.RHSFromBCRight
     = (BCDirRight(timeCurrent) * 
	carrierState.NegElecEndPoints(numEndPoints - 1))
       *(carrierProps.DGFluxMatrices.plusminusTransposed).col(numElements-1);
 
 /*//MKL VERSION 
    alpha = BCDirLeft(timeCurrent) * carrierState.PosElecEndPoints_MKL[0];

   
    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierProps.leftBCVector_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSFromBCLeft_MKL,
		carrierProps.increment_MKL
	       );

    cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSFromBCLeft_MKL,
	       carrierProps.increment_MKL
	      );

    //TODO: REMOVE WHEN DONE
    isSame(carrierState.RHSFromBCLeft, carrierState.RHSFromBCLeft_MKL);
    
    alpha = BCDirRight(timeCurrent) * carrierState.PosElecEndPoints_MKL[numElements-1];

    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierProps.rightBCVector_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSFromBCRight_MKL,
		carrierProps.increment_MKL
	       );

    cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSFromBCRight_MKL,
	       carrierProps.increment_MKL
	      );

    isSame(carrierState.RHSFromBCRight, carrierState.RHSFromBCRight_MKL);
*/

   grvy_timer_end("Boundary Conditions");

   //  cout << "RHSFromBCRight = \n" << RHSFromBCRight << endl;

   grvy_timer_begin("Summing RHSTotal");
   
     // Eigen Version
    carrierState.RHSPartial =
       (1.0) * carrierState.RHSFromStiff 
     - (1.0) * carrierState.RHSFromFluxRightPOS 
     - (1.0) * carrierState.RHSFromFluxRightNEG
     + (1.0) * carrierState.RHSFromFluxLeftPOS 
     + (1.0) * carrierState.RHSFromFluxLeftNEG 
     - (1.0) * carrierState.RHSFromBCRight
     + (1.0) * carrierState.RHSFromBCLeft;
/* 
    // MKL Version
    // RHS_Partial_MKL = RHSFromStiff
    cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierState.RHSFromStiff_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSPartial_MKL,
		carrierProps.increment_MKL
	       );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxRightPOS_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );
  
   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxRightNEG_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxLeftPOS_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );


   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromFluxLeftNEG_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );


   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   //		      - RHSFromBCRight 
   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromBCRight_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );

   //RHS_Partial_MKL = RHSFromStiff - RHSFromFluxRightPos - RHSFromFluxRightNEG
   //		      + RHSFromFluxLeftPOS + RHSFromFluxLeftNEG
   //		      - RHSFromBCRight + RHSFromBCLeft
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromBCLeft_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSPartial_MKL,
	      carrierProps.increment_MKL
	     );


   isSame(carrierState.RHSPartial, carrierState.RHSPartial_MKL);
*/

   // Update Recombination Dof Values  
   carrierState.RHSTotal = 
	carrierProps.Mobility * carrierState.RHSPartial
	 + (1.0) * carrierState.RHSFromQ ;
	 - (1.0) * carrierState.RHSFromRecombination;

   // MKL Version
/* 
   // RHSTotal_MKL = mobility * RHSPartial_MKL		
   cblas_dcopy(
                carrierProps.numDGDof_MKL,
		carrierState.RHSPartial_MKL,
		carrierProps.increment_MKL,
		carrierState.RHSTotal_MKL,
		carrierProps.increment_MKL
	       );

   alpha = carrierProps.Mobility;
   
  cblas_dscal(
	       carrierProps.numDGDof_MKL,
	       alpha,
 	       carrierState.RHSTotal_MKL,
	       carrierProps.increment_MKL
	       );
	     
   alpha = 1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromQ_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSTotal_MKL,
	      carrierProps.increment_MKL
	     );

   alpha = -1.0;
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      alpha,
	      carrierState.RHSFromRecombination_MKL,
	      carrierProps.increment_MKL,
	      carrierState.RHSTotal_MKL,
	      carrierProps.increment_MKL
	     );

  isSame(carrierState.RHSTotal, carrierState.RHSTotal_MKL);
*/
  grvy_timer_end("Summing RHSTotal");

   ////////////////////////////////////////////////////////////////////////////
   // Solve
   ////////////////////////////////////////////////////////////////////////////
   grvy_timer_begin("Solve for U");
   carrierState.uDotDof = carrierState.solverU.solve(carrierState.RHSTotal);
 /* 
  solverU_MKL.solve(carrierProps.MassU_MKL, carrierState.RHSTotal_MKL, carrierState.uDotDof_MKL); 

  isSame(carrierState.uDotDof, carrierState.uDotDof_MKL);
*/
   if( carrierState.solverU.info() != Eigen::Success)
   {
       cout << "Failed to solver for U" << endl;
       assert(false);
   }  
   
   grvy_timer_end("Solve for U");
   
   return 0;
}

int isSame(ddpDenseVector_type eigen, ddpDenseVector_MKL_type MKL)
{
  for(int i = 0; i < eigen.size(); ++i)
    {
      if( !(
	    fabs(eigen(i) - MKL[i]) < 0.0001 * fabs(eigen(i)) + 0.00001 ))
	    {
	cout << "i = " << i << endl;
	cout << "eigen(i) - MKL[i] = " <<  eigen(i) - MKL[i] << endl;
	cout << "eigen(i) = " << eigen(i) << endl;
	cout << "MKL[i] = " << MKL[i] << endl;
						
		  
	assert(false);
	
      }
    }
  return 0;
}

 int forwardEuler(double const & deltaT)
 {
	// u(i+1) = u'(i)*dt + u(i)

	uNext = deltaT * carrierState.uDotDof + carrierState.uDof;
	
	carrierState.uDof = uNext;

	return 0;
 } 

 int forwardEuler_MKL(double const & deltaT)
 {
   cblas_daxpy(
	      carrierProps.numDGDof_MKL,
	      deltaT,
	      carrierState.uDotDof_MKL,
	      carrierProps.increment_MKL,
	      carrierState.uDof_MKL,
	      carrierProps.increment_MKL
	     );
		   
  
	return 0;
 }


 int free( )
 {
	delete [] carrierState.uDof_MKL;
	delete [] carrierState.qDof_MKL;
	delete [] carrierState.uDotDof_MKL;
	delete [] uNext_MKL;

	delete [] carrierState.BCInput_MKL;  
	delete [] carrierState.QRHS_MKL;    
//	delete [] carrierState.RHSFromQ_MKL;
/*	delete [] carrierState.RHSFromStiff_MKL;      
	delete [] carrierState.RHSFromFluxRightPOS_MKL;
	delete [] carrierState.RHSFromFluxRightNEG_MKL;
	delete [] carrierState.RHSFromFluxLeftPOS_MKL;
	delete [] carrierState.RHSFromFluxLeftNEG_MKL;
	delete [] carrierState.RHSFromBCRight_MKL;   
	delete [] carrierState.RHSFromBCLeft_MKL;   
	delete [] carrierState.RHSPartial_MKL;     
	delete [] carrierState.RHSTotal_MKL; 
	delete [] carrierState.RHSFromRecombination_MKL; 
*/

	delete [] carrierState.ElecPTS_MKL;
	delete [] carrierState.ElecEndPoints_MKL;
	delete [] carrierState.PosElecEndPoints_MKL;
	delete [] carrierState.NegElecEndPoints_MKL;
	delete [] carrierState.PosElecEndPTSNoEnd_MKL;
	delete [] carrierState.NegElecEndPTSNoEnd_MKL;
	
	delete [] carrierState.ElecPTSTimesWeights_MKL;


	delete [] carrierState.workSpaceVector0_MKL;
	delete [] carrierState.workSpaceVector1_MKL;

	delete [] PTSDense_MKL;
	delete [] weightsDense_MKL;


	/// TODO: REMOVE WHEN DONE
	delete [] ElecFieldDof_MKL;


	return 0;
  }
	
  
} ddpCarrier_type;

#endif
