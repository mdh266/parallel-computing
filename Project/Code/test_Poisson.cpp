#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"

int ddpTestingMakeInputFile(void);
int print2File(ddpPoisson_type const & testPoisson);
int print2File(ddpCarrier_type const & testCarrier);

double ZeroFunction(double const & x)
{
	return 0.0;
}

double Const_BC_Zero(double const & t)
{
	return 0;
}

double Const_BC_One(double const & t)
{
	return 1.0;
}

double SineFunction(double const & x)
{
	return sin(x);
}


bool Dofs_Are_Same(ddpPoisson_type const & P1, ddpPoisson_type const & P2)
{
  double tol = 1.0e-8;
  
  if(P2.PoissonState.elecDof.size() != P1.PoissonState.elecDof.size())
    {
      cout << "elecDofs size not same" << endl;
      return false;
    }
  
  if(P2.PoissonState.potDof.size() != P1.PoissonState.potDof.size())
    {
      cout << "potDofs size not same" << endl;
      return false;
    }
  
  for(int i = 0; i < P1.PoissonState.potDof.size(); i++)
    {
      if( fabs(P1.PoissonState.potDof(i)-P2.PoissonState.potDof(i)) > tol )
	{
	  cout << "potDofs not same at entry " << i << endl;
	  return false;
	}
      if( fabs(P1.PoissonState.elecDof(i) - P2.PoissonState.elecDof(i)) > tol )
	{
	  cout << "elecDofs not same at entry " << i << endl;
	  return false;
	}
      
    }
  return true;
}

bool Dofs_Are_Same_MKL(ddpPoisson_type const & P1, ddpPoisson_type const & P2)
{
  double tol = 1.0e-8;
  
  for(int i = 0; i < P1.PoissonState.potDof.size(); i++)
    {
      if(fabs(P1.PoissonState.potDof_MKL[i]-P2.PoissonState.potDof(i)) > tol)
	{
	  cout << "potDofs not same at entry " << i << endl;
	  return false;
	}
      
      if(fabs(P1.PoissonState.elecDof_MKL[i] - P2.PoissonState.elecDof(i)) > tol)
	{
	  cout << "elecDofs not same at entry " << i << endl;
	  return false;
	}
    }
  return true;
}


// Solution to 1.)
double Solution1Pot(double const & x)
{
   return (1.0/M_PI) * x;
}

double Solution1Elec(double const & x)
{
   return -1.0/M_PI;
}

double Solution2Pot(double const & x)
{
	return 	sin(x);
}

double Solution2Elec(double const & x)
{
	return 	-1.0*cos(x);
}

double Solution3Pot(double const & x)
{
	return -1.0*sin(x);
}

double Solution3Elec(double const & x)
{
	return cos(x);
}

int main()
{
  grvy_timer_init("GRVY TIMING");
  grvy_timer_reset();
  grvy_timer_begin("Initialization");

  // Initial set up stuff
  ddpTestingMakeInputFile();  
  
  ddpDomain_type testdomain;
  ddpGrid_type testgrid;
  ddpProblemInfo_type testproblem;
  ddpCarrierConstants_type testConstants;

  readInput(testdomain, testgrid, testproblem, testConstants, "ddp-test");

  testdomain.LeftEndPoint = 0.0;
  testdomain.RightEndPoint = M_PI;

  ddpMakeUniformGrid(testproblem, testdomain, testgrid); 
  
  ddpPoisson_type testPoisson, solutionPoisson;
  ddpCarrier_type testElectrons, testHoles;
  testElectrons.initialize(testgrid, testproblem, testConstants, "electrons");
  testHoles.initialize(testgrid, testproblem, testConstants, "holes");
  ddpDopingProfile_type dopingProfile;
  
  testPoisson.initialize(testgrid, testproblem, testConstants);
  solutionPoisson.initialize(testgrid, testproblem, testConstants);


  // Things to use to project funtin onto a basis
  ddpBijForw_type DGForward = testPoisson.Bijections.DGForward;
  ddpBijForw_type MXForward = testPoisson.Bijections.MXForward;

  ddpSparseMatrix_type globalVandeMondeDG = 
			testPoisson.VandeMondeMatrices.globalVandeMondeDG;
  
  ddpSparseMatrix_type globalVandeMondeMX = 
			testPoisson.VandeMondeMatrices.globalVandeMondeMX;

  ddpBijForw_type PTForward = testPoisson.Bijections.PTForward;

  ddpSparseVector_type sparseWeights = testPoisson.weightsSparse;

  ddpSparseMatrix_type MassU = testElectrons.carrierProps.MassU;
  ddpSparseMatrix_type A00 = testPoisson.PoissonProps.A00;

  ddpBijFlag_type BIJFlagDG = DG; 
  ddpBijFlag_type BIJFlagMX = Mixed; 

  grvy_timer_end("Initialization");
 
  cout << "\n\n \t\tTesting ddpCarrier_type" << endl;

  cout << "\n \nTests If solving -lambda d^2/dx^2 = C(x) correctly. Lambda = 1"
       << endl << endl;
	
  // sets lambdda = 1
  testPoisson.PoissonProps.Lambda = 1.0;  

  cout << "1.) Testing Phi(0) = 0, Phi(pi) = 1, C(x) = 0 " << endl; 

  // set Phi(0) = 0, Phi(pi) =1
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_One);
  
  // set n(x,t) = 0
  testElectrons.setInitialConditions(testgrid, &ZeroFunction);
  
  // Now set C(x) = 0
  dopingProfile.setDopingProfile(testgrid, testElectrons, &ZeroFunction);
 
 
  
  //   Construct TRUE solution
  ddpProjectFunction(&Solution1Pot,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    solutionPoisson.PoissonState.potDof);
  
  ddpProjectFunction(&Solution1Elec,
		    testgrid, 
		    MXForward,
		    globalVandeMondeMX,
		    PTForward,
		    BIJFlagMX,
		    sparseWeights,
		    A00,
		    solutionPoisson.PoissonState.elecDof);

 // Solve for Phi, time = 0.0.
  testPoisson.solveSystem_MKL(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same_MKL(testPoisson, solutionPoisson))
    cout << " MKL Version PASSED!" << endl;
  else
    cout << " MKL Version FAILED!" << endl;
  
  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same(testPoisson, solutionPoisson))
    cout << " Eigen Version PASSED!" << endl;
  else
    cout << " Eigen Version FAILED!" << endl;
  



  


  
  cout << "\n2.) Testing Phi(0) = 0, Phi(pi) = 0, C(x) = sin(X) " 
	<< endl; 
 
  // Set Phi(0) = 0, Phi(pi) = 0
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_Zero);
 
  
  // Now set C(x) = SinFunction(x)
  dopingProfile.setDopingProfile(testgrid, testElectrons, &SineFunction);
 
      

  
  //   Construct TRUE solution
  ddpProjectFunction(&Solution2Pot,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    solutionPoisson.PoissonState.potDof);
  
  ddpProjectFunction(&Solution2Elec,
		    testgrid, 
		    MXForward,
		    globalVandeMondeMX,
		    PTForward,
		    BIJFlagMX,
		    sparseWeights,
		    A00,
		    solutionPoisson.PoissonState.elecDof);
  
  // Solve for Phi, time = 0.0.
  testPoisson.solveSystem_MKL(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same_MKL(testPoisson, solutionPoisson))
    cout << " MKL Version PASSED!" << endl;
  else
    cout << " MKL Version FAILED!" << endl;

  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same(testPoisson, solutionPoisson))
    cout << " Eigen Version PASSED!" << endl;
  else
   cout << " Eigen Version FAILED!" << endl;



 




  cout << "\n \n \nTests If solving -lambda d^2/dx^2 = C(x) - (n-p)"
	<< " correctly. Lambda = 1" << endl << endl;

  cout << "3.) Testing Phi(0) = 0, Phi(pi) = 0, C(x) = 0, "
	   << "n = sin(x), p = 0 "  << endl; 


  // Now set C(x) = 
  dopingProfile.setDopingProfile(testgrid, testElectrons, &ZeroFunction);
 


  // set n(x,t) = SineFunction(x)
  testElectrons.setInitialConditions(testgrid, &SineFunction);

  
  //  print2File(testPoisson);
//  print2File(testElectrons);
  
//   Construct TRUE solution
  ddpProjectFunction(&Solution3Pot,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    solutionPoisson.PoissonState.potDof);
  
  ddpProjectFunction(&Solution3Elec,
		    testgrid, 
		    MXForward,
		    globalVandeMondeMX,
		    PTForward,
		    BIJFlagMX,
		    sparseWeights,
		    A00,
		    solutionPoisson.PoissonState.elecDof);
  
  
  // Solve for Phi, time = 0.0.  

  testPoisson.solveSystem_MKL(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same_MKL(testPoisson, solutionPoisson))
    cout << " MKL Version PASSED!" << endl;
  else
    cout << " MKL Version FAILED!" << endl;
  

  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same(testPoisson, solutionPoisson))
    cout << " Eigen Version PASSED!" << endl;
  else
    cout << " Eigen Version FAILED!" << endl;
  





  
  

  cout << "\n4.) Testing Phi(0) = 0, Phi(pi) = 0, C(x) = 0, "
	<< "n = 0, p = sin(x) " << endl; 


  // set p(x,t) = SineFunction(x)
  testHoles.setInitialConditions(testgrid, &SineFunction);

//  print2File(testHoles);
 

  print2File(testPoisson);
  print2File(testElectrons);
 
//   Construct TRUE solution
  ddpProjectFunction(&Solution2Pot,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    solutionPoisson.PoissonState.potDof);
  
  ddpProjectFunction(&Solution2Elec,
		    testgrid, 
		    MXForward,
		    globalVandeMondeMX,
		    PTForward,
		    BIJFlagMX,
		    sparseWeights,
		    A00,
		    solutionPoisson.PoissonState.elecDof);
 
  // Solve for Phi, time = 0.0.
 
  testPoisson.solveSystem_MKL(testHoles, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same_MKL(testPoisson, solutionPoisson))
    cout << " MKL Version PASSED!" << endl;
  else
    cout << " MKL Version FAILED!" << endl;

  testPoisson.solveSystem(testHoles, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same(testPoisson, solutionPoisson))
    cout << " Eigen Version PASSED!" << endl;
  else
    cout << " Eigen Version FAILED!" << endl;



  cout << "\n5.) Testing Phi(0) = 0, Phi(pi) = 1, C(x) = 0, "
	<< "n = p  = sin(x) " << endl; 


  // set Phi(0) = 0, Phi(pi) =1
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_One);
  

  print2File(testPoisson);
//  print2File(testElectrons);
 
//   Construct TRUE solution
  ddpProjectFunction(&Solution1Pot,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    solutionPoisson.PoissonState.potDof);
  
  ddpProjectFunction(&Solution1Elec,
		    testgrid, 
		    MXForward,
		    globalVandeMondeMX,
		    PTForward,
		    BIJFlagMX,
		    sparseWeights,
		    A00,
		    solutionPoisson.PoissonState.elecDof);
  
  // Solve for Phi, time = 0.0.
  
  testPoisson.solveSystem_MKL(testElectrons, testHoles, dopingProfile, 0.0, testproblem);
  if(Dofs_Are_Same_MKL(testPoisson, solutionPoisson))
    cout << " MKL Version PASSED!" << endl;
  else
    cout << " MKL Version FAILED!" << endl;
  
  testPoisson.solveSystem(testElectrons, testHoles, dopingProfile, 0.0, testproblem);
 if(Dofs_Are_Same(testPoisson, solutionPoisson))
    cout << " Eigen Version PASSED!" << endl;
  else
    cout << " Eigen Version FAILED!" << endl;
  



  
  
  
  
  cout << "\n\n \t\tTesting of poisson.hpp complete." << endl;
 
  dopingProfile.free(); 
  testElectrons.free();
  testHoles.free();
  testPoisson.free();
  solutionPoisson.free();

 
  grvy_timer_finalize();
  grvy_timer_summarize();

  return 0;
}

int print2File(ddpCarrier_type const & testCarrier)
{
  ofstream prt;
  prt.open("testCarrier.dat");
 
  ddpDenseVector_type
    UPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.uDof;
  
   for(int i = 0; i < UPTS.size(); i++)
   {
	prt << testCarrier.PTSDense(i) << " " 
	    << UPTS(i) << endl;
   }
 
 prt.close();
 return 0;

}

int print2File(ddpPoisson_type const & testPoisson)
{
  ofstream prt;
  prt.open("testPoisson.dat");
 
  ddpDenseVector_type
    ElecPTS = testPoisson.VandeMondeMatrices.globalVandeMondeMX 
	 	 * testPoisson.PoissonState.elecDof,
    POTPTS = testPoisson.VandeMondeMatrices.globalVandeMondeDG 
		 * testPoisson.PoissonState.potDof;
  
   for(int i = 0; i < ElecPTS.size(); i++)
   {
	prt << testPoisson.PTSDense(i) << " " 
	    << ElecPTS(i) << " " 
	    << POTPTS(i) << endl;
   }
 
 prt.close();
 return 0;

}




int ddpTestingMakeInputFile(void)
{
  ofstream inputfile;
  inputfile.open("ddp-test");
  inputfile << "[physical] \n \
		xLeftEndPoint = 0.0\n \
		xRightEndPoint = 1.0\n \
		timeInitial = 0.0\n \
	        timeFinal = 1.00\n \
		temperature = 300 \n \
		electronCharge = 1.6e-19\n \
		vacuumPermittivity = 8.85e-14\n \
		semiCondRelativePerm     = 11.9\n \
		BoltzmanConst = 1.3806488e-23\n\
		characteristicLength	= 1.0e-4\n \
		characteristicTime 	= 1.0e-12\n \
		\n \n \
		[electrons] \n \
		Mobility = 1500 \n \
		ChargeSign = Negative \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[computational]\n \
		maxOrderMX = 2 \n \
		maxOrderDG = 2 \n \
		integrationOrder = 10 \n \
		numElements = 1000 \n \
		PointsPerElement = 5 \n \
		numTimeStamps          = 47 \n \
		GaussLobbatoNumPoints  = 15  \n \
		GaussLegendreNumPoints = 5 \n \
		\n \n \
		[couplingStatus]\n \
		couplingToPoisson      = On \n";
  inputfile.close();
   return 0;
}
		
			
	
