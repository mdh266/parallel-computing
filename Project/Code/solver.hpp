#ifndef _SOLVER_H_
#define _SOLVER_H_

#include "includes.hpp"

typedef struct ddpSolver_MKL_type
{

  /////////////////////////////////////////////////////////////////////////////
  // Data Members
  /////////////////////////////////////////////////////////////////////////////

   MKL_INT nRhs;
   MKL_INT nRows;
   MKL_INT nCols;
   MKL_INT nNonZeros;
   MKL_INT * rowIndex;
   MKL_INT * columns;
   double * values;
  
   MKL_INT opt;
   MKL_INT sym;
   MKL_INT type;

   _INTEGER_t error;
   _INTEGER_t zero;

   _MKL_DSS_HANDLE_t solverHandle;

  /////////////////////////////////////////////////////////////////////////////
  // Member Functions
  /////////////////////////////////////////////////////////////////////////////
  
  int initialize(ddpCsr_MKL_type const & A_MKL)
  {
	nRhs = 1;
	nRows = A_MKL.numRows;
        nCols = A_MKL.numRows;
	nNonZeros = A_MKL.nnz;
	rowIndex = A_MKL.I;
	columns = A_MKL.J;
	values = A_MKL.values;
	
	opt = MKL_DSS_DEFAULTS;
	sym = MKL_DSS_NON_SYMMETRIC;
	type = MKL_DSS_INDEFINITE;
	zero = 0;
	
 	// Initialize the solver

	opt =   MKL_DSS_MSG_LVL_WARNING
	        + MKL_DSS_TERM_LVL_ERROR
       		+ MKL_DSS_ZERO_BASED_INDEXING;

	error = dss_create(solverHandle, opt);

	if(error != MKL_DSS_SUCCESS)
        {
          goto printError;
        }

	// Define the non-zero structure of the matrix

	opt = MKL_DSS_DEFAULTS;

	error = dss_define_structure(
                                     solverHandle,
                                     sym,
                                     rowIndex,
                                     nRows,
                                     nCols,
                                     columns,
                                     nNonZeros
				    );

        if(error != MKL_DSS_SUCCESS)
        {
          goto printError;
        }
	
	// Preorder the matrix
	
    	opt = MKL_DSS_AUTO_ORDER;
        
	error = dss_reorder(solverHandle, opt, &zero);

	if(error != MKL_DSS_SUCCESS)
        {
          goto printError;
        }

	// Refactor the matrix
	//
	opt = MKL_DSS_INDEFINITE;

	error = dss_factor_real(solverHandle, opt, values);
	
	if(error != MKL_DSS_SUCCESS)
        {
          goto printError;
        }
     
	return 0;
	
	printError:
		cout << "Initializing the Solver did not work" << endl;
		assert(false);
		return 1;
  }

  int solve(ddpCsr_MKL_type const & A_MKL,
	    ddpDenseVector_MKL_type const & RHS,
	    ddpDenseVector_MKL_type & solution)
  {	
	opt = MKL_DSS_DEFAULTS;
	sym = MKL_DSS_NON_SYMMETRIC;
	type = MKL_DSS_INDEFINITE;
	
//	grvy_timer_begin("MKL SOLVER LINE");
	error = dss_solve_real(solverHandle, opt, RHS, nRhs, solution);
//	grvy_timer_end("MKL SOLVER LINE");
	
	if(error != MKL_DSS_SUCCESS)
        {
          goto printError;
        }
     
	return 0;
	
	printError:
		cout << "DSS solve did not work" << endl;
		assert(false);
		return 1;
  }

} ddpSolver_MKL_type;

#endif 

