#ifndef _MAIN_H_
#define _MAIN_H_

#include "includes.hpp"


///////////////////////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS IN FILES
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// In Main:
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// In ReadAndPrint.cpp
//////////////////////////////////////////////////////////////////////////////

int
readInput(ddpDomain_type & domain, ddpGrid_type & grid,
	  ddpProblemInfo_type & problem,
	  ddpCarrierConstants_type & carrierConstants,
	  char const * nameOfFile);

int 
ddpPrintGrid(ddpGrid_type const & grid);

int ddpPrintTimeStamps(
		       std::vector< double > const & timeStamps);

int
ddpPrintBij_Backward(ddpBijBack_type const & bij);

int 
ddpExportCarrierMatrices2Market(ddpCarrierProperties_type & carrier,
  			        std::string str);

int 
ddpExportPotentialMatrices2Market(ddpPoissonProperties_type & PotentialState,
				  std::string str);

////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// In SimulationConditions.cpp
///////////////////////////////////////////////////////////////////////////////
double
ddpElectronDirichletLeft(double const & t);

double
ddpElectronDirichletRight(double const & t);

double 
ddpElectronInitialConditions(double const & x);

double
ddpHoleDirichletLeft(double const & t);

double
ddpHoleDirichletRight(double const & t);

double 
ddpHoleInitialConditions(double const & x);

double
ddpPotentialDirichletLeft(double const & t);

double
ddpPotentialDirichletRight(double const & t);

double
NPlus_N_NPlus(double const & x);

double
ddpDonorDopingProfile(double const & x);

double
ddpAcceptorDopingProfile(double const & x);


///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// In Utilities.cpp
///////////////////////////////////////////////////////////////////////////////
int
ddpWeightedL2Norm(
		  ddpDenseVector_type const & input,
		  ddpDenseVector_type const & weight,
		  double & output
		  );

int
ddpConcatenateMatrices(ddpSparseMatrix_type const & A, 
		       ddpSparseMatrix_type const & B,
		       ddpSparseMatrix_type & C);

int
ddpEigenSparseToIntelCoo(ddpSparseMatrix_type const & input, 
			 ddpCoo_MKL_type & output);

int
ddpIntelCooToIntelCsr(ddpCoo_MKL_type const & input,
		      ddpCsr_MKL_type & output
		      );

int
ddpEigenSparseToIntelCsr(ddpSparseMatrix_type const & input,
			 ddpCsr_MKL_type & output
			 );
int
ddpPositivePart(ddpDenseVector_type const & input, 
		ddpDenseVector_type & output);

int
ddpNegativePart(ddpDenseVector_type const & input, 
		ddpDenseVector_type & output);

int
ddpPositivePart(ddpDenseVector_MKL_type const & input, 
		ddpDenseVector_MKL_type & output, int const & len);

int
ddpNegativePart(ddpDenseVector_MKL_type const & input, 
		ddpDenseVector_MKL_type & output, int const & len);

int
ddpProjectFunction( double (*function)(double const &), 
		    ddpGrid_type const & grid,
		    ddpBijForw_type const & FuncForward,
		    ddpSparseMatrix_type const & globalVandeMondeJ,
		    ddpBijForw_type const & PTForward,
		    ddpBijFlag_type const & BijFlag,
		    ddpSparseVector_type const & sparseWeights,
		    ddpSparseMatrix_type const & MassMatrix,
		    ddpDenseVector_type & output);


///////////////////////////////////////////////////////////////////////////////
// In problemSetUp.cpp
///////////////////////////////////////////////////////////////////////////////

double
ddpPsi( ddpGrid_type const & grid, 
	int const & elem, 
	int const & order,
	int const & numDerivs,
	double const & x
	);

double
ddpLimPsi(
	  ddpGrid_type const & grid,
	  int const & elem, 
	  int const & order,
	  ddpDirection_type const & Flag,
	  int const & endPointNumber
	  );

double
ddpUpsilon( ddpGrid_type const & grid, 
	    int const & elem, 
	    int const & order,
	    int const & numDerivs,
	    double const & x
	    );


int
ddpMakeUniformGrid
(ddpProblemInfo_type const & problem, 
 ddpDomain_type const & domain, ddpGrid_type & grid);



int
ddpMakeTimeStamps(ddpProblemInfo_type const & problem, 
		  std::vector< double > & timeStamps);




int
ddpMakeBijs(ddpGrid_type const & grid,
	    ddpBijFlag_type const & flag,
	    ddpBijForw_type& Forward,
	    ddpBijBack_type& Backward);

int
ddpMakeGlobalVandeMonde(ddpGrid_type const & grid, 
			ddpProblemInfo_type const & problem,
			ddpBijFlag_type const & flag,
			ddpBijForw_type const & FuncForward,
			ddpBijForw_type const & PTForward,
			ddpNumDeriv_type const & numDeriv,
			ddpSparseMatrix_type & vandeMonde);

int
ddpMakeGlobalVandeMondeFluxMX(ddpGrid_type const & grid, 
			      ddpProblemInfo_type const & problem,
			      ddpBijForw_type const & FuncForward,
			      ddpSparseMatrix_type & vandeMonde);

int
ddpMakeGlobalVandeMondeFluxDG(ddpGrid_type const & grid, 
			      ddpProblemInfo_type const & problem,
			      ddpBijForw_type const & FuncForward,
			      ddpDirection_type const & Flag1,
			      ddpDirection_type const & Flag2,		      
			      ddpSparseMatrix_type & vandeMonde);

int
ddpPrintBij_Backward(ddpBijBack_type const & bij);

 
int
ddpMakeWeightVectorDense(ddpGrid_type const & grid, 
			 ddpDenseVector_type & weights);

int
ddpMakeWeightVectorSparse(ddpGrid_type const & grid,
			  ddpSparseVector_type & weights);
int
ddpMakePTVectorSparse(ddpGrid_type const & grid,
		      ddpSparseVector_type & points);
int
ddpMakePTVectorDense(ddpGrid_type const & grid,
		     ddpDenseVector_type & points);

int
ddpMakeGeneralMatrix(ddpProblemInfo_type const & problem,
		     ddpGrid_type const & grid,
		     ddpSparseVector_type const & sparseWeights,
		     ddpBijFlag_type const & flagI,
		     ddpBijForw_type const & BijI,
		     ddpSparseMatrix_type const & globalVandeMondeI,
		     ddpBijFlag_type const & flagJ,
		     ddpBijForw_type const & BijJ,
		     ddpSparseMatrix_type const & globalVandeMondeJ,
		     ddpSparseMatrix_type & OutputMatrix
		     );

int ddpMakeGeneralFluxMatrix(ddpGrid_type const & grid,
				ddpDenseVector_type const & Vpos,
				ddpDenseVector_type const & Vneg,
				ddpBijForw_type const & FuncForward,
				ddpDGFluxMatrices_type const & DGFluxMatrices,
				ddpDirection_type const & Flag1,
				ddpSparseMatrix_type & output);

int ddpMakeGeneralFluxVector(ddpGrid_type const & grid,
			     ddpDenseVector_type const & VposOrVneg,
			     int const & ElementNumber,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type  const & DGFluxMatrices,
			     ddpDirection_type const & Flag1, 
			     ddpSparseMatrix_type & output);

int ddpMakeSpecialFluxMatrix(ddpGrid_type const & grid,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type const & DGFluxMatrices,
			     ddpDirection_type const & Flag1,
			     std::vector<ddpDirection_type> const& Flag2Vector,
			     ddpSparseMatrix_type & output);

int ddpMakeSpecialFluxVector(ddpGrid_type const & grid,
			     int const & ElementNumber,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type const & DGFluxMatrices,
			     ddpDirection_type const & Flag1,
			     ddpSparseMatrix_type & output);


int 
ddpMakeWeightsAndPoints(ddpGrid_type const & grid, 
  			ddpProblemInfo_type const & problem,
			ddpSparseVector_type & weightsSparse,
			ddpSparseVector_type & PTSSparse, 
			ddpDenseVector_type & PTSDense, 
			ddpDenseVector_type & weightsDense);

int 
ddpMakeAllBijections(ddpGrid_type const & grid,
	      	     ddpBijection_type & Bijections);

int 
ddpMakeVandeMondeMatrices(ddpGrid_type const & grid, 
	ddpProblemInfo_type const & problem, 
	ddpBijection_type const & Bijections,
	ddpVandeMondeMatrices_type & VandeMondeMatrices,
	ddpDGFluxMatrices_type & DGFluxMatrices);


int 
ddpMakeCarrierProperies(
			ddpProblemInfo_type const & problem,
			ddpGrid_type const & grid,
			ddpSparseVector_type const & weightsSparse,
			ddpBijection_type const & Bijections,
			ddpVandeMondeMatrices_type const & VandeMondeMatrices,
			ddpDGFluxMatrices_type const & DGFluxMatrices,
			ddpCarrierProperties_type & CarrierProps);


int 
ddpMakePoissonProperties( ddpProblemInfo_type const & problem,
			  ddpGrid_type const & grid,
			  ddpSparseVector_type const & weightsSparse,
			  ddpBijection_type const & Bijections,
			  ddpVandeMondeMatrices_type const & VandeMondeMatrices,
			  ddpDGFluxMatrices_type const & DGFluxMatrices,
			  ddpPoissonProperties_type & PoissonProperties);
  

#endif
