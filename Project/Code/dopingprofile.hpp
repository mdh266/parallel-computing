#ifndef _DOPINGPROFILE_H_
#define _DOPINGPROFILE_H_

#include "main.hpp" // for stuff in SimulationConditions.cpp
#include "includes.hpp"
#include "carrier.hpp"
#include "poisson.hpp"

typedef struct ddpDopingProfile_type 
{

///////////////////////////////////////////////////////////////////////////////
//   Data Members
///////////////////////////////////////////////////////////////////////////////

	// Dofs
	ddpDenseVector_type Dof;
	ddpDenseVector_type DonorDof;
	ddpDenseVector_type AcceptorDof;

	// MKL Dofs
	ddpDenseVector_MKL_type Dof_MKL;
	MKL_INT numDof_MKL;
	MKL_INT increments_MKL;

	// Doping Functions
	double (* DonorDopingProfileFunction)(double const & x); 
	double (* AcceptorDopingProfileFunction)(double const & x);


///////////////////////////////////////////////////////////////////////////////
//   Class Methods
///////////////////////////////////////////////////////////////////////////////
	
	// Projects the doping function on to the basis and
	// stores it in this structures Dof
	int setDopingProfile(ddpGrid_type const & grid,
			     ddpCarrier_type const & carrier,
			     double (* DopingProfileFunction) (double const & x))

	{
		ddpBijFlag_type BijFlag = DG;
		
		// set C(x) = N_D(x) or C(x) = -N_A(x)
		if(carrier.carrierProps.ChargeSign == Negative)
		{

	            // set donor function
		    this->setDonorFunction(DopingProfileFunction);	
	
		    // Project doping function onto basis 
	 	    ddpProjectFunction( DonorDopingProfileFunction,
			 	      grid,
				      carrier.Bijections.DGForward,
				      carrier.carrierProps.VandeMondeMatrices.globalVandeMondeDG,
				      carrier.Bijections.PTForward,
				      BijFlag,
				      carrier.weightsSparse,
				      carrier.carrierProps.MassU,
				      Dof);
		// C(x) = N_D
		
		}
		
		else
		{
		  // set acceptor function
		  this->setAcceptorFunction(DopingProfileFunction);	
		
		  // Project doping function onto basis 
	 	  ddpProjectFunction( AcceptorDopingProfileFunction,
			 	      grid,
				      carrier.Bijections.DGForward,
				      carrier.carrierProps.VandeMondeMatrices.globalVandeMondeDG,
				      carrier.Bijections.PTForward,
				      BijFlag,
				      carrier.weightsSparse,
				      carrier.carrierProps.MassU,
				      Dof);
		  
		// switch signs so C(x) = -N_A
		  Dof = -1.0*Dof;
		}
		
		Dof_MKL = new double[Dof.size()];
	 	// SET THE MKL DOF
		for(int i = 0; i < Dof.size();	i++)
			Dof_MKL[i] = Dof(i); 
	
		return 0;
	}

	// Projects the doping function on to the basis and
	// stores it in this structures Dof
	int setDopingProfile(ddpGrid_type const & grid,
			     ddpCarrier_type const & carrier1,
			     ddpCarrier_type const & carrier2,
			     double (* DonorProfileFunction) (double const & x),
			     double (* AcceptorProfileFunction) (double const & x) 
			     )
	{
	
		ddpBijFlag_type BijFlag = DG;

		// set donor function
		this->setDonorFunction(DonorProfileFunction);	
		
		// set acceptor function
		this->setAcceptorFunction(AcceptorProfileFunction);	
		
	 	ddpProjectFunction(DonorDopingProfileFunction,
			 	   grid,
				   carrier1.Bijections.DGForward,
				   carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG,
				   carrier1.Bijections.PTForward,
				   BijFlag,
				   carrier1.weightsSparse,
				   carrier1.carrierProps.MassU,
				   DonorDof);

		  // carrier2 = type for N_A
	 	ddpProjectFunction(AcceptorDopingProfileFunction,
			 	   grid,
				   carrier2.Bijections.DGForward,
				   carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG,
				   carrier2.Bijections.PTForward,
				   BijFlag,
				   carrier2.weightsSparse,
				   carrier2.carrierProps.MassU,
				   AcceptorDof);
		
		// Set C(x) = N_D - N_A
		Dof = DonorDof - AcceptorDof;
		
	 	// SET THE MKL DOF
		Dof_MKL = new double[Dof.size()];
	 	// SET THE MKL DOF
		for(int i = 0; i < Dof.size();	i++)
			Dof_MKL[i] = Dof(i); 
			   
		return 0;
	}
	
	// set the donor doping function
	int setDonorFunction(double (* DopingProfileFunction) (double const & x))
	{
		DonorDopingProfileFunction = DopingProfileFunction;
	 	return 0;
	}
	
	// set the acceptor doping function
	int setAcceptorFunction(double (* DopingProfileFunction) (double const & x))
	{
		AcceptorDopingProfileFunction = DopingProfileFunction;
	 	return 0;
	}

	int free( )
	{
		delete []  Dof_MKL;
	}
	
} ddpDopingProfile_type;

#endif
