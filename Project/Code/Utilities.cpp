#include "includes.hpp"
#include "main.hpp"


int
ddpWeightedL2Norm(
		  ddpDenseVector_type const & input,
		  ddpDenseVector_type const & weight,
		  double & output
		  )
{
  output = sqrt(input.cwiseProduct(input).cwiseProduct(weight).sum());
  return 0;
}


int
ddpConcatenateMatrices(ddpSparseMatrix_type const & A, 
		       ddpSparseMatrix_type const & B,
		       ddpSparseMatrix_type & C)
{
  assert(A.rows() == B.rows());
  C.resize(A.rows(), A.cols() + B.cols());
  C.middleCols(0,A.cols()) = A;
  C.middleCols(A.cols(),B.cols()) = B;
  return 0;
}

int
ddpEigenSparseToIntelCoo(ddpSparseMatrix_type const & input,
			 ddpCoo_MKL_type & output)
{
  int counter = 0;
  
  for (int k = 0; k < input.outerSize();  ++k)
    {
      for (ddpSparseMatrix_type::InnerIterator it(input, k);
	   it; ++it)
	{
	  output.values[counter] = it.value();
	  output.rows[counter] = it.row();
	  output.columns[counter] = it.col();
	  ++counter;
	}
    }
  output.nnz = counter;
  return 0;
}

int
ddpIntelCooToIntelCsr(ddpCoo_MKL_type const & input,
		      ddpCsr_MKL_type & output
		      )
{
  //unpack the structures
  //int numDof = input.
  
  double * inputVals = input.values;
  int * inputRows = input.rows;
  int * inputCols = input.columns;
  int inputNumRows = input.numRows;
  int inputNumCols = input.numCols;
  int inputNnz = input.nnz;
  
  double * outputVals = output.values;
  int * outputI = output.I;
  int * outputJ = output.J;
  output.numRows = input.numRows;
  output.numCols = input.numCols;
  output.nnz = inputNnz;
  

  // Fill in the job information, these entries are describled in the intel
  // MKL API documentation.
  int job[6];
  job[0] = 1; //The matrix in the coordinate format is converted to the 
              //CSR format
  job[1] = 0; //Zero based indexing for the matrix in the Csr format
  job[2] = 0; //Zero based indexing for the matrix in the Coo format
  job[3] = 0; //Doesn't mean anything
  job[4] = inputNnz;
  job[5] = 0;

  int dim = input.numCols;
  cout << "dim = " << dim << endl;
  cout << "inputNnz = " << inputNnz << endl;
  
  int info;
  cout << "before MKL Call" << endl;
  mkl_dcsrcoo(job,
	      &dim,
	      outputVals,
	      outputJ,
	      outputI,
	      &inputNnz,
	      inputVals,
	      inputRows,
	      inputCols,
	      &info);
  cout << "after MKL Call" << endl;
  return 0;
}

			 

int
ddpEigenSparseToIntelCsr(ddpSparseMatrix_type const & input,
			 ddpCsr_MKL_type & output
			 )
{
  
  //First convert to input to intelCoo
   ddpCoo_MKL_type Temp_Coo_MKL;
   int numRows = input.rows();
   Temp_Coo_MKL.numRows = input.rows();
   Temp_Coo_MKL.numCols = input.cols();
   
   Temp_Coo_MKL.nnz = input.nonZeros();
   int nnz  = Temp_Coo_MKL.nnz;

   Temp_Coo_MKL.values = new double[nnz];
   Temp_Coo_MKL.rows = new int[nnz];
   Temp_Coo_MKL.columns = new int[nnz];
   
   int counter = 0;
    
   for (int k = 0; k < input.outerSize();  ++k)
     {
       for (ddpSparseMatrix_type::InnerIterator it(input, k);
	    it; ++it)
	 {
	   Temp_Coo_MKL.values[counter] = it.value();
	   Temp_Coo_MKL.rows[counter] = it.row();
	   Temp_Coo_MKL.columns[counter] = it.col();
	   ++counter;
	 }
    }
  Temp_Coo_MKL.nnz = counter;
  
  /*
  cout << "input = \n" << input << endl;
  
  for(int i = 0; i < Temp_Coo_MKL.nnz; ++i)
    {
      cout << " Temp_Coo_MKL.values[" << i << "] = " << Temp_Coo_MKL.values[i] << endl;
      cout << "   Temp_Coo_MKL.rows[" << i << "] = " << Temp_Coo_MKL.rows[i] << endl;
      cout << "Temp_Coo_MKL.columns[" << i << "] = " << Temp_Coo_MKL.columns[i] << endl;
    }
  */
  
  //Then convert to intelCoo to intelCsr
  double * outputVals = new double[nnz];
  MKL_INT * outputI = new MKL_INT[numRows + 1];
  MKL_INT * outputJ = new MKL_INT[nnz];
  double temp;

  //  cout << "input.rows() = " << input.rows() << endl;

  output.numRows = input.rows();
  output.numCols = input.cols();
  output.nnz = input.nonZeros();
  
  //  cout << "output.nnz = " << output.nnz << endl;
  output.values = Temp_Coo_MKL.values;
  output.J = Temp_Coo_MKL.columns;
  nnz = output.nnz;
  // Fill in the job information, these entries are describled in the intel
  // MKL API documentation.
  
  int job[6];
  job[0] = 1; //The matrix in the coordinate format is converted to the CSR format
  job[1] = 0; //Zero based indexing for the matrix in the Csr format
  job[2] = 0; //Zero based indexing for the matrix in the Coo format
  job[3] = 0; //Doesn't mean anything
  job[4] = nnz;
  job[5] = 0;


  nnz = output.nnz;
  
  int info;
  
  mkl_dcsrcoo(job,
	      &numRows,
	      outputVals,
	      outputJ,
	      outputI,	      
	      &nnz,
	      Temp_Coo_MKL.values,
	      Temp_Coo_MKL.rows,
	      Temp_Coo_MKL.columns,
	      &info);
  assert(info == 0);
  
  //cout << "nnz = " << nnz << endl;
  //cout << "numRows = " << numRows << endl;
  /*
  for(int i = 0; i < nnz; ++i)
    {
      cout << "outputVals[" << i << "] = " << outputVals[i] << endl;
      cout << "outputJ[" << i << "] = " << outputJ[i] << endl;
    }
  
  for(int i = 0; i < numRows + 1; ++i)
    {
      cout << "outputI[" << i << "] = " << outputI[i] << endl;
    }
  */

  //free memory we don't need any more
  delete [] Temp_Coo_MKL.values;
  delete [] Temp_Coo_MKL.rows;
  delete [] Temp_Coo_MKL.columns; 
  
  output.J = outputJ;
  output.I = outputI;
  output.values = outputVals;
  
  return 0;

}
			 
			 

int
ddpPositivePart(ddpDenseVector_type const & input, ddpDenseVector_type & output)
{
  ddpDenseVector_type tempVector(input.size());
  for(int i = 0; i < input.size(); ++i)
    {
      tempVector(i) = std::max(0.0, input.coeffRef(i) );
    }
  output = tempVector;
  return 0;
}

int
ddpNegativePart(ddpDenseVector_type const & input, ddpDenseVector_type & output)
{
  ddpDenseVector_type tempVector(input.size());
  for(int i = 0; i < input.size(); ++i)
    {
      tempVector(i) = std::min(0.0, input.coeffRef(i) );
    }
  output = tempVector;
  return 0;
}

// MKL VERSIONS

int
ddpPositivePart(ddpDenseVector_MKL_type const & input, 
		ddpDenseVector_MKL_type & output, int const & len)
{
  for(int i = 0; i < len; ++i)
    {
      output[i] = std::max(0.0, input[i] );
    }
  return 0;
}

int
ddpNegativePart(ddpDenseVector_MKL_type const & input, 
		ddpDenseVector_MKL_type & output, int const & len)
{
  for(int i = 0; i < len; ++i)
    {
      output[i] = std::min(0.0, input[i] );
    }
  return 0;
}



int
ddpProjectFunction( double (*function)(double const &),		    
		    ddpGrid_type const & grid,
		    ddpBijForw_type const & FuncForward,
		    ddpSparseMatrix_type const & globalVandeMondeJ,
		    ddpBijForw_type const & PTForward,
		    ddpBijFlag_type const & BijFlag,
		    ddpSparseVector_type const & sparseWeights,
		    ddpSparseMatrix_type const & MassMatrix,
		    ddpDenseVector_type & output)
{
 // Projects a function name function on to a given basis
 // using quadrature.  The basis id determined to be either DG or Mixed
 // using the BijFlag.  Notice we need the MassMatrix for this, 
 // because we have not assumed that the basis functions are orthogonal

  int lastElement;

  if(DG == BijFlag)
    {
      lastElement = grid.NumElementsNoGhost;
    }
  else if(Mixed == BijFlag)
    {
      lastElement = grid.NumElementsWithGhost;
    }

  else
    { 
      assert(false);
      return 1;
    }
  
  ddpSparseVector_type localWeights = sparseWeights;
  int numTestFunctions = FuncForward.size();
  ddpDenseVector_type RHSVector(numTestFunctions);
  ddpDenseVector_type tempOutput(numTestFunctions);

  
  // TODO:  The name ElectricFieldPointVals doensn't make sense in the 
  // general context.
   ddpDenseVector_type electricFieldPointVals( PTForward.size() );
  
   // Build a vector that contains that the function to be projected,
   // with its value at each sample point.
  for(int elem = 0; elem < lastElement; ++elem)
    {
      int localNumGaussLegendrePoints 
	= grid.ElementList[elem].GaussLegendreNumPoints;
  
      for(int pointNum = 0; 
	  pointNum < localNumGaussLegendrePoints; ++pointNum)
	{
	  double xi = grid.ElementList[elem].GaussLegendrePoints[pointNum];
	  
	  std::pair<int, int> elemAndPointJ;
	  elemAndPointJ.first = elem;
	  elemAndPointJ.second = pointNum;
	  int globalJ = PTForward.find(elemAndPointJ)->second;

	  
	  electricFieldPointVals(globalJ) = function(xi);

	}
    }
  

  //Build the row vector from sampled function. 
  int maxOrder;
  for(int elem = 0; elem < lastElement; ++elem)
    {
      //cout << "elem = " << elem << endl;

      if(DG == BijFlag)
	{
	  maxOrder = (grid.ElementList[elem]).OrderDG;
	}
      else if(Mixed == BijFlag)
	{
	  maxOrder = (grid.ElementList[elem]).OrderMX;
	}
      else
	{ 
	  assert(false);
	  return 1;
	}
      
      for(int order = 0; order <= maxOrder; ++order)
	{
	  //cout << "\torder = " << order << endl;
	  std::pair<int, int> elemAndOrderJ;
	  elemAndOrderJ.first = elem;
	  elemAndOrderJ.second = order;
	  int globalJ =  FuncForward.find(elemAndOrderJ)->second;

	  ddpSparseVector_type temp = globalVandeMondeJ.col(globalJ);
	  //We loop over the nonzero elements of temp1
	  double partial_sum = 0.0;
	  for(ddpSparseVector_type::InnerIterator it(temp); it; ++it)
	    {
	      int ind = it.index();
	      double tempval = temp.coeffRef(ind);
	      double weightval = localWeights.coeffRef(ind);
	      double electricFieldVal = electricFieldPointVals.coeffRef(ind);
	      partial_sum += tempval * electricFieldVal * weightval;
	    }
	  double valueJ = partial_sum;
	  
	  double Threeeps = 12.0*std::numeric_limits<double>::epsilon();
	  
	  if(fabs(valueJ) > Threeeps)
	    {
	      RHSVector(globalJ) = valueJ;
	    }
	  else
	    {
	      RHSVector(globalJ) = 0.0;
	    }
	}
    }
   
  //cout << "RHSVector = \n" << RHSVector << endl;
  
  //perform a linear solve to find the function's degrees of freedom
  // TODO: make a rational choice for this solver.
  
  ddpSolver_type solver0;
  //  solver0.setTolerance( TOLERANCE );
  //Eigen::UmfPackLU<Eigen::SparseMatrix<double> > solver0;
  Eigen::BiCGSTAB<Eigen::SparseMatrix<double> > solver1;
  //  solver1.setTolerance( TOLERANCE );

 
  solver0.compute(MassMatrix);
  if(solver0.info() != Eigen::Success)
    {
      cout << "solver0 compute step didn't work" << endl;
    }
    
  tempOutput = solver0.solve(RHSVector);
  
  //  cout << "solver0.error = " << solver0.error() << endl;

  //cout << "MassMatrix = \n" << MassMatrix << endl;

  if(solver0.info() == Eigen::Success) 
    {
      //cout << "Solver0 did work" << endl;
      output = tempOutput;
      return 0;
    }
  else
    {
      
      tempOutput = solver1.compute(MassMatrix).solve(RHSVector);
      if(solver1.info() == Eigen::Success)
	{
	  //cout << "Solver1 did work" << endl;
	  output = tempOutput;
	  return 0;
	}	 
      else
	{ 
	  cout << "MassMatrix = \n" << MassMatrix  << endl;
	  cout << "RHSVector = \n" << RHSVector << endl;
	  cout << "Neither solver worked" <<endl;
	  assert(false);
	  return 1;
	}
    }
 
  return 0;
}

