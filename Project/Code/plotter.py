#!/usr/bin/env python

###############################################################################
#  Plotter.py 
#  Plots and makes movies for main.cpp
#
#  Must load python
#
###############################################################################

# IMPORTS
import sys, os, commands
import matplotlib.pyplot as plt
import ConfigParser
from numpy import *

import bipolar
import unipolar

###############################################################################
# User choices
###############################################################################

# Image type
imageType = 'eps'

# Movie type
movieType = 'mp4'


###############################################################################
# Simulation information
##############################################################################
# Read the simulation information from the input file "ddp-input"
parser = ConfigParser.ConfigParser()
parser.read('ddp-input')

input_dictionary = {}

# Physical
for name in parser.options('physical'):
	string_value = parser.get('physical',name)
	value = string_value.split()[0]
	input_dictionary[name] = float(value)


# computational
for name in parser.options('computational'):
	string_value = parser.get('computational',name)
	value = string_value.split()[0]
	input_dictionary[name] = int(value)

# Note:  Python will uncapitalize everything when scanning in
#xLeftEndPoint = input_dictionary['xleftendpoint']
#xRightEndPoint = input_dictionary['xrightendpoint']
#characteristicLength = input_dictionary['characteristiclength']
#xRightEndPoint  = xRightEndPoint * characteristicLength;
#xRightEndPoint  = xRightEndPoint * characteristicLength;
#timeInitial = input_dictionary['timeinitial']
#timeFinal = input_dictionary['timefinal']
#maxOrderMX = input_dictionary['maxordermx']
#maxOrderDG = input_dictionary['maxorderdg']
#numElements = input_dictionary['numelements']

numTimeStamps = input_dictionary['numtimestamps']-1

##############################################################################
# see if user wants to keep the images and movie in a file
##############################################################################

ans = raw_input("Do you want to keep a record of this run? (y or n)\n\n")

# if yes create the directory
if(ans == 'y'):
	DirName = raw_input("\nWhat do you want to name the record\n\n")
	cmd2MakeDir = 'mkdir ' + DirName
	failure, ouput = commands.getstatusoutput(cmd2MakeDir)
	if failure:
		raise NameError('Failed to make the directory.')

##############################################################################
# get variables to be plotted from the user
##############################################################################

model = raw_input("Unipolar or Bipolar model? (u or b)\n\n")

# str to get the input to get variable to plot
str4input = "\nWhat do you want to plot? Enter: \n u for density \n q for"\
            " diffusive current \n d for drift current\n"\
 	    " E for electric field \n p for potential \n" \
	    " J for total current\n"


# Get variable to plot
VAR = raw_input(str4input)


if(model == 'b'):
	bipolar.makeImages(numTimeStamps, VAR, ans, imageType, movieType)

if(model == 'u'):
	unipolar.makeImages(numTimeStamps,VAR, ans, imageType, movieType)


