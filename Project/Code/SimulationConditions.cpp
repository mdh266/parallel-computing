#include "includes.hpp"
#include "main.hpp"


///////////////////////////////////////////////////////////////////////////////
// Electron Boundary/Initial Conditions
///////////////////////////////////////////////////////////////////////////////

double
ddpElectronDirichletLeft(double const & t)
{
   // n(left end point, t ) = 
  return 1.0e17;  // per cm^-3
}

double
ddpElectronDirichletRight(double const & t)
{
   // n(right end pointmt, t) = 
  return 0.0;//1.0e17; // per cm^-3
}

double 
ddpElectronInitialConditions(double const & x)
{
   return ddpDonorDopingProfile(x);  // per cm^-3
}

///////////////////////////////////////////////////////////////////////////////
// Hole Boundary/Initial Conditions
///////////////////////////////////////////////////////////////////////////////

double
ddpHoleDirichletLeft(double const & t)
{
   // p(left end point, t) = 
  return 0.0;  // per cm^-3
}

double
ddpHoleDirichletRight(double const & t)
{
   // p(right end point), t = 
  return 1.0e17;  // per cm^-3
}

double 
ddpHoleInitialConditions(double const & x)
{
   // p(x, 0) 
   return ddpAcceptorDopingProfile(x);  // per cm^-3
}


///////////////////////////////////////////////////////////////////////////////
// Potential Boundary Conditions
///////////////////////////////////////////////////////////////////////////////


double
ddpPotentialDirichletLeft(double const & t)
{
  // Phi(left pt, t)
  return 0.0;  // Volts
}

double
ddpPotentialDirichletRight(double const & t)
{
 // Phi(right pt, t)
 return  0.5; //5.0*sin(t*M_PI*2.0e12);  // Volts
} 



///////////////////////////////////////////////////////////////////////////////
// Dopping Profiles
///////////////////////////////////////////////////////////////////////////////


double
NPlus_N_NPlus(double const & x)
{

  if(x <= 0.3e-4)
    return 1.0e17;
  else if ( (x > 0.3e-4) && (x < 0.7e-4) )
    return 1.0e15;
  else
    return 1.0e17;
}

double
ddpDonorDopingProfile(double const & x)
{

   if ( x <= 0.5e-4 )
  	return 1.0e17;
   else
   	return 0.0;


  /*  if      ( (0.0    <= x) && (x < 0.2e-4) )
    return 1.0e17;
  else if ( (0.2e-4 <= x) && (x < 0.4e-4) )
    return (1.0e12-1.0e17)/(0.2e-4)* (x - 0.2e-4) + 1.0e17;
  else if ( (0.4e-4 <= x) && (x < 0.6e-4) )
    return 1.0e12;
  else if ( (0.6e-4 <= x) && (x < 0.8e-4) )
    return (1.0e17-1.0e12)/(0.2e-4)* (x - 0.6e-4) + 1.0e12;
  else if ( (0.8e-4 <= x) && (x < 1.0e-4) )
    return 1.0e17;
  else 
    assert(
  */

  /*
  double sigma = 0.1e-4;
  double mu = 0.5e-4;

  double temp;
  temp = 1.0e17 * ( 1 - .99 * exp( - (x - mu) * (x - mu) / (2 * sigma * sigma)));
  //temp = 1.0e17 * ( 1 - (1.0 / (sigma * sqrt(2 * M_PI) ) ) * exp( - (x - mu) * (x - mu) / (2 * sigma * sigma)));
  
  return temp;
  */
}

double
ddpAcceptorDopingProfile(double const & x)
{


   if(x >= 0.5e-4)
	return 1.0e17;
   else 
	return 0.0;
}



