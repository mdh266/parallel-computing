#!/usr/bin/env python

import sys, os, commands
import matplotlib.pyplot as plt
from numpy import *


def makeImages(numTimeStamps, VAR, ans, imageType, movieType):
	# Check to make sure varible is acceptable, if not throw exception
	if(VAR == 'u'):
		varLabel = '[1/cm^3]'
		plotTitle = 'Density distribution'
		index = 1
		name = 'density'
	elif(VAR == 'q'):
		varLabel = 'Amperes/cm^2'
		plotTitle = 'Diffusive current distribution'
		index = 3
		name = 'qValues'
	elif(VAR == 'd'):
		varLabel = 'Amperes/cm^2'
		plotTitle = 'Drift current distribution'
		index = 5
		name = 'dValues'
	elif(VAR == 'E'):
		varLabel = 'Volts/cm'
		plotTitle = 'Electric field distribution'
		index = 7
		name = 'ElectricField'
	elif(VAR == 'p'):
		varLabel = 'Volts'
		plotTitle = 'Potential distribution'
		index = 8
		name = 'Potential'
	else:
		raise NameError('Input not valid')


	# index corresponds to which column to use in the data files.  
	# This in turn  is the column which corresponds to the 
	# variable the user choose.

	cmd2convert = 'convert ' 

###############################################################################
# Open each data file according to the time stamp and find the max/min for 
# variable being plotted
###############################################################################
	yMinValue = 0.0	
	yMaxValue = 0.0
	for timeStamp in range(0,numTimeStamps):

		# Open the appropriate file according to the time stamp
		stamp = str.zfill( str(timeStamp), 4) # convert to 4 characters
		nameOfFile = 'State' + stamp + '.dat'
		f = open(nameOfFile)
	
		# Get all the lines from the file and then chose the file
		linesInFile = f.readlines()
		f.close()

		# Create numpy array
		varVals = zeros( len(linesInFile) )
		i = 0
		

		# Get the information line by line
		for lines in linesInFile:
			varVals[i] = float((lines.split())[index]) 
			i = i + 1
                 
	                # Find min and max of this file, and set to 
			# global max/min if necessary
			tempMax = varVals.max()
			tempMin = varVals.min()
			if(tempMin < yMinValue):
				yMinValue = tempMin
			if(tempMax > yMaxValue):
				yMaxValue = tempMax



###############################################################################
# Open each data file according to the time stamp and createa a plot based off
# of the variable the user chose
###############################################################################

	for timeStamp in range(0,numTimeStamps):

		# Open the appropriate file according to the time stamp
		stamp = str.zfill( str(timeStamp), 4) # convert to 4 characters
		nameOfFile = 'State' + stamp + '.dat'
		f = open(nameOfFile)
	
		# Get all the lines from the file and then chose the file
		linesInFile = f.readlines()
		f.close()

		# Create numpy array
		xVals = zeros( len(linesInFile) )	
		VarVals = zeros( len(linesInFile) )
		Var2Vals = zeros( len(linesInFile) )
		i = 0
	
		# Get the information line by line
		for lines in linesInFile:
			xVals[i] = float((lines.split())[0]) 
			VarVals[i] = float((lines.split())[index]) 
			Var2Vals[i] = float((lines.split())[index+1])
			i = i + 1
		
                 
		# Plot the figure
		plt.clf() # clear window		if(VAR == 'u'):
		if((VAR != 'E') & (VAR != 'p') & (VAR != 'J')):
			plt.plot(xVals, VarVals,'b-',label='electrons')		
			plt.plot(xVals,Var2Vals,'r-',label='holes')
			plt.legend()
		else:
			plt.plot(xVals, VarVals,'b-')		

		plt.ylim([yMinValue, (1.5)*yMaxValue])
		plt.xlabel('[cm]')
		plt.ylabel(varLabel)
		plt.title(plotTitle)

		# Save the figure
		if(ans == 'y'):
			nameOfImage =  DirName + '/'+ 'State' + stamp + '.' + imageType
		else:	
			nameOfImage = 'State' + stamp + '.' + imageType

		plt.savefig(nameOfImage)  # Save image

		# Add the images to be converted into 
		cmd2convert += nameOfImage + ' ' 


###############################################################################
# Convert all the images into a movie
###############################################################################

	# Add the name of the movie onto the string which will execute convert
	nameOfMovie = name + '.' + movieType
	cmd2convert += nameOfMovie

# Run the command to execute convert
	failure, ouput = commands.getstatusoutput(cmd2convert)
	if failure:
		raise NameError('Error in converting png files into mp4 movie')

# Print the name of the movie
	print 'Your movie is complete.  It is titled: ' + nameOfMovie


# Erase images
#if(ans == 'n'):
#	failure, ouput = commands.getstatusoutput('rm *.' + imageType)
#if failure:
#	raise NameError('Error removing images.')

# Open the movie
#failure, ouput = commands.getstatusoutput('totem ' + nameOfMovie)
#if failure:
#	raise NameError('Error in launching movie')

###############################################################################
