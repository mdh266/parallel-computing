#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"

int ddpTestingMakeInputFile(void);

int testUniformGrid(ddpProblemInfo_type const & testproblem, 
		    ddpDomain_type const & testdomain,
		    ddpGrid_type const & grid);


int testReadInput(ddpDomain_type const & testdomain, 
		  ddpGrid_type const & testgrid, 
		  ddpProblemInfo_type const & testproblem, 
		  ddpCarrierConstants_type const & testConstants);



int main()
{
  
  ddpDomain_type testdomain;
  ddpGrid_type testgrid;
  ddpProblemInfo_type testproblem;
  ddpCarrierConstants_type testConstants;

  ddpTestingMakeInputFile();
  readInput(testdomain, testgrid, testproblem, testConstants, "ddp-test");
  
  cout << "\n \n \t\tTesting for readInput.cpp and makeUniformGrid.cpp."
	" Only testing non-constants" << endl;
  cout << "\n \n1.) Testing readInput.cpp" << endl;
  testReadInput(testdomain, testgrid, testproblem, testConstants);
  cout << "PASSED!" << endl; 

  cout << " \n \n2.) Testing makeUniformGrid.cpp" << endl;
  ddpMakeUniformGrid(testproblem, testdomain, testgrid); 
  testUniformGrid(testproblem, testdomain, testgrid);
  cout << "PASSED!" << endl;

  cout << "\n\n \t \tTesting ReadAndGrid.cpp complete." << endl;
  return 0;
}

int testUniformGrid(ddpProblemInfo_type const & testproblem, 
		    ddpDomain_type const & testdomain,
		    ddpGrid_type const & testgrid)

{
	// test to make sure the grid is correct
	// test the first and last end point
	int NumElem = testgrid.NumElementsNoGhost;
	assert(testgrid.ElementList[0].Left == testdomain.LeftEndPoint);
        assert(testgrid.ElementList[NumElem-1].Right == testdomain.RightEndPoint);

	// test the ghost elements
	assert(testgrid.ElementList[NumElem].Left == testdomain.RightEndPoint);
        assert(testgrid.ElementList[NumElem].Right == testdomain.RightEndPoint);

	// left end point of  element[i] = i * 1.0e-4/NumELements
	// Right end point of  element[i] = (i+1) * 1.0e-4/NumELements
	for(int i = 1; i < NumElem; i++)
	{
		assert(testgrid.ElementList[i].Left == i*testdomain.RightEndPoint/NumElem);
		assert(testgrid.ElementList[i].Right == (i+1)*testdomain.RightEndPoint/NumElem);
	}



	return 0;
}

int testReadInput(ddpDomain_type const & testdomain, 
		  ddpGrid_type const & testgrid, 
		  ddpProblemInfo_type const & testproblem, 
		  ddpCarrierConstants_type const & testConstants)
{

	// Test Grid
	assert(testgrid.OrderDGMax == 2);
	assert(testgrid.NumElementsNoGhost == 4);
	assert(testgrid.NumElementsWithGhost == 5);
	
	// Test Problem
	assert(testproblem.MaxOrderDG == 2);
	assert(testproblem.MaxOrderMX == 2);
	assert(testproblem.characteristicTime == 1.0e-12);
	assert(testproblem.characteristicLength == 1.0e-4);
	assert(testproblem.TimeInitial == 0.0e-12);

	// TODO: Find a way to compare floating points better	
	assert((testproblem.TimeFinal - 100.0e-12)/100.0e-12 < 0.001);
	assert(testproblem.GaussLobattoNumPoints == 15);
	assert(testproblem.GaussLegendreNumPoints == 5);
	assert(testproblem.NumFrames == 47);
	assert(testproblem.ElecFieldCouplingStatus == Off);
	assert(testproblem.characteristicTime == 1.0e-12);
	//assert(testproblem.characteristicDensity == 1.0e17);
	
	// Test domain
	assert(testdomain.LeftEndPoint == 0.0);
	assert(testdomain.RightEndPoint == 1.0e-4);

	return 0;
}


int ddpTestingMakeInputFile(void)
{
  ofstream inputfile;
  inputfile.open("ddp-test");
  inputfile << "[physical] \n \
		xLeftEndPoint = 0.0\n \
		xRightEndPoint = 1.0\n \
		timeInitial = 0.0\n \
	        timeFinal = 100.0\n \
		temperature = 300 \n \
		electronCharge = 1.6e-19\n \
		vacuumPermittivity = 8.85e-14\n \
		semiCondRelativePerm     = 11.9\n \
		BoltzmanConst = 1.3806488e-23\n\
		characteristicLength	= 1.0e-4\n \
		characteristicTime 	= 1.0e-12\n \
		\n \n \
		[electrons] \n \
		Mobility = 1500 \n \
		ChargeSign = Negative \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[computational]\n \
		maxOrderMX = 2 \n \
		maxOrderDG = 2 \n \
		integrationOrder = 10 \n \
		numElements = 4 \n \
		PointsPerElement= 5 \n \
		numTimeStamps          = 47 \n \
		GaussLobbatoNumPoints  = 15  \n \
		GaussLegendreNumPoints = 5 \n \
		\n \n \
		[couplingStatus]\n \
		couplingToPoisson      = Off \n";
  inputfile.close();
   return 0;
}
		
			
	
