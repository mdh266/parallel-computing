#include "ddpComputeDeltaT.hpp"


int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_MKL_type const & electricFieldPTS_MKL,
		 ddpCarrier_type const & carrier,
		 double & DeltaT
		 )
{    

  int index =  cblas_idamax(carrier.carrierProps.numPoints_MKL, 
	       electricFieldPTS_MKL,
	       carrier.carrierProps.increment_MKL);
  double maxV = electricFieldPTS_MKL[index];
  double 
    theta = (1.0 + grid.OrderDGMax) * (1.0 + grid.OrderDGMax) / 
	    (0.0 + grid.DeltaxMin);  

  double numerator = 2.0 * carrier.carrierProps.Diffusivity;
  double sqrtOfDenominator =  carrier.carrierProps.Mobility *maxV + 2 * 
			      carrier.carrierProps.Diffusivity * theta;
  double denominator = sqrtOfDenominator * sqrtOfDenominator;
  DeltaT = numerator / denominator;


  return 0;
}

int
ddpComputeDeltaT_MKL(ddpGrid_type const & grid,
		 ddpDenseVector_MKL_type const & electricFieldPTS_MKL,
		 ddpCarrier_type const & carrier1,
		 ddpCarrier_type const & carrier2,
		 double & DeltaT
		 )
{    
  int index =  cblas_idamax(carrier1.carrierProps.numPoints_MKL, 
	       electricFieldPTS_MKL,
	       carrier1.carrierProps.increment_MKL);

  double maxV = electricFieldPTS_MKL[index];
  
  double 
    theta = (1.0 + grid.OrderDGMax) * (1.0 + grid.OrderDGMax) / 
    (0.0 + grid.DeltaxMin);  

  double numerator1 = 2.0 * carrier1.carrierProps.Diffusivity;
  double numerator2 = 2.0 * carrier2.carrierProps.Diffusivity;

  double sqrtOfDenominator1 =  carrier1.carrierProps.Mobility *maxV + 2 * 
    carrier1.carrierProps.Diffusivity * theta;
  double sqrtOfDenominator2 =  carrier2.carrierProps.Mobility *maxV + 2 * 
    carrier2.carrierProps.Diffusivity * theta;
  
  double denominator1 = sqrtOfDenominator1 * sqrtOfDenominator1;
  double denominator2 = sqrtOfDenominator2 * sqrtOfDenominator2;
  
  double DeltaT1 = numerator1 / denominator1;
  double DeltaT2 = numerator2 / denominator2;

  
  DeltaT = std::min(DeltaT1, DeltaT2);
  return 0;
}

 
int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_type const & electricFieldPTS,
		 ddpCarrier_type const & carrier,
		 double & DeltaT
		 )
{    
  double maxV = electricFieldPTS.lpNorm<Eigen::Infinity>();

  double 
    theta = (1.0 + grid.OrderDGMax) * (1.0 + grid.OrderDGMax) / 
	    (0.0 + grid.DeltaxMin);  

  double numerator = 2.0 * carrier.carrierProps.Diffusivity;
  double sqrtOfDenominator =  carrier.carrierProps.Mobility *maxV + 2 * 
			      carrier.carrierProps.Diffusivity * theta;
  double denominator = sqrtOfDenominator * sqrtOfDenominator;
  DeltaT = numerator / denominator;


  return 0;
}

int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_type const & electricFieldPTS,
		 ddpCarrier_type const & carrier1,
		 ddpCarrier_type const & carrier2,
		 double & DeltaT
		 )
{    
  double maxV = electricFieldPTS.lpNorm<Eigen::Infinity>();
  
  double 
    theta = (1.0 + grid.OrderDGMax) * (1.0 + grid.OrderDGMax) / 
    (0.0 + grid.DeltaxMin);  

  double numerator1 = 2.0 * carrier1.carrierProps.Diffusivity;
  double numerator2 = 2.0 * carrier2.carrierProps.Diffusivity;

  double sqrtOfDenominator1 =  carrier1.carrierProps.Mobility *maxV + 2 * 
    carrier1.carrierProps.Diffusivity * theta;
  double sqrtOfDenominator2 =  carrier2.carrierProps.Mobility *maxV + 2 * 
    carrier2.carrierProps.Diffusivity * theta;
  
  double denominator1 = sqrtOfDenominator1 * sqrtOfDenominator1;
  double denominator2 = sqrtOfDenominator2 * sqrtOfDenominator2;
  
  double DeltaT1 = numerator1 / denominator1;
  double DeltaT2 = numerator2 / denominator2;

  
  DeltaT = std::min(DeltaT1, DeltaT2);
 
  return 0;
}

