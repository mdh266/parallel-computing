#include "includes.hpp"
#include "main.hpp"
#include "ddpPrintState.hpp"

int
readInput(ddpDomain_type & domain, ddpGrid_type & grid,
	  ddpProblemInfo_type & problem,
	  ddpCarrierConstants_type & carrierConstants,
	  char const * nameOfFile)
{

  // COULD DO THIS A SHORTER WAY PASSING DOMAIN AND GRIDS
  // DATA TYPE ADDRESS BUT I THINK THIS IS A LITTLE SAFER/EASIER
  // TO READ

  // Physical inputs
  double 
    xLeftEndPoint,
    xRightEndPoint,
    timeInitial,
    timeFinal,
    temperature,
    electronCharge,
    vacuumPermittivity,
    semiCondRelativePerm,
    BoltzmannConst,
    characteristicLength,
    characteristicTime,
    IntrinsicDensity,
    RadRecomboConst;
   
  // hole properties inputs
  double
    electronMobility,
    electronRecomboTime,
    holeMobility,
    holeRecomboTime;

  std::string
     ElecFieldCouplingStatus,
     electronChargeSign,
     holeChargeSign;

  // Problem dependent inputs
  int
    maxOrderMX,
    maxOrderDG,
    //integrationOrder = 10,
    numElements,
    //PointsPerElement = 5,./
    //rungeKuttaOrder = 5,
    numTimeStamps,
    GaussLobbatoNumPoints,
    GaussLegendreNumPoints;

///////////////////////////////////////////////////////////////////////////////
// READ IN INPUTS FROM GRVY INPUT PARSER
///////////////////////////////////////////////////////////////////////////////

  GRVY::GRVY_Input_Class iparse;  // Input parsing object

  // Initialize/read the file
  if(! iparse.Open(nameOfFile))
  {
	exit(1);
  }
	
  
  // NOTE: If the values of the variables read in from file are the same as 
  // the default values, grvy will print to screen that it is using the 
  // pre-registered values
  // The title before the / and the variable is the section of the input file
  
  // Read from the Physical constants section with default values given
  iparse.Read_Var("physical/xLeftEndPoint", &xLeftEndPoint,-1.0);
  iparse.Read_Var("physical/xRightEndPoint", &xRightEndPoint,1.0);
  iparse.Read_Var("physical/timeInitial", &timeInitial,0.0);
  iparse.Read_Var("physical/timeFinal", &timeFinal,1.0);
  iparse.Read_Var("physical/temperature", &temperature,300.0);
  iparse.Read_Var("physical/electronCharge", &electronCharge,1.6e-19);
  iparse.Read_Var("physical/vacuumPermittivity", &vacuumPermittivity,1.0);
  iparse.Read_Var("physical/semiCondRelativePerm", &semiCondRelativePerm,1.0);
  iparse.Read_Var("physical/BoltzmannConst", &BoltzmannConst,1.3806488e-23);
  iparse.Read_Var("physical/characteristicLength", &characteristicLength,1.0e-6);
  iparse.Read_Var("physical/characteristicTime", &characteristicTime,1.0e-12);
  iparse.Read_Var("physical/IntrinsicDensity", &IntrinsicDensity,1.45e10);
  iparse.Read_Var("physical/RadRecomboConst", &RadRecomboConst,1.1e-14);

   // Read in the electron properties section with default values given
  iparse.Read_Var("electrons/Mobility", &electronMobility,480.0);
  iparse.Read_Var("electrons/recombinationTime", &electronRecomboTime,1.0e-6);
  iparse.Read_Var("electrons/ChargeSign", &electronChargeSign,"Positive");
  
   // Read in the electron properties section with default values given
  iparse.Read_Var("holes/Mobility", &holeMobility,480.0);
  iparse.Read_Var("holes/recombinationTime", &holeRecomboTime,1.0e-6);
  iparse.Read_Var("holes/ChargeSign", &holeChargeSign,"Positive");

  // Read from the computational constants section with default values given
  iparse.Read_Var("computational/maxOrderMX", &maxOrderMX,1);
  iparse.Read_Var("computational/maxOrderDG", &maxOrderDG,1);
  iparse.Read_Var("computational/numElements", &numElements,4);
  iparse.Read_Var("computational/numTimeStamps", &numTimeStamps,10);
  iparse.Read_Var("computational/GaussLobbatoNumPoints", 
		  &GaussLobbatoNumPoints,20);
  iparse.Read_Var("computational/GaussLegendreNumPoints",
		  &GaussLegendreNumPoints,16);
  
  // Read in the coupling status
  iparse.Read_Var("couplingStatus/couplingToPoisson", &ElecFieldCouplingStatus,"On");
  
  // Print read-in-input to log file
  cout << "\n ------ Full Dump to LOG.txt ---------- \n" << endl;
  iparse.Fdump("%","LOG.txt");
  cout << "\n ------ End Full Dump  ---------- \n" <<  endl;
 
  // Close the input file

  // Some GSl functions aren't happy if GuassLegendreNumPoints is higher than 16
  assert(GaussLegendreNumPoints <= 16);
  //  The integration won't work properly if we don't ensure the following
  assert(2 * maxOrderDG <= GaussLegendreNumPoints);
  assert(2 * maxOrderMX <= GaussLegendreNumPoints);
 
///////////////////////////////////////////////////////////////////////////////
// Push everything on to grid and files.
///////////////////////////////////////////////////////////////////////////////

  // Prepare Grid
  // These are structures defined in main.hpp
  domain.LeftEndPoint = xLeftEndPoint * characteristicLength;
  domain.RightEndPoint = xRightEndPoint * characteristicLength;

  grid.Domain = domain;
  grid.OrderDGMax = maxOrderDG;
  grid.NumElementsNoGhost = numElements;
  grid.NumElementsWithGhost = numElements + 1;
  
  // The + 1 accounts for the ghost element in the mixed FEM part for poisson. 
  
  problem.MaxOrderDG = maxOrderDG;
  problem.MaxOrderMX = maxOrderMX;
  problem.NumFrames = numTimeStamps;
  problem.GaussLobattoNumPoints = GaussLobbatoNumPoints;
  problem.GaussLegendreNumPoints = GaussLegendreNumPoints;
  problem.TimeFinal = timeFinal * characteristicTime;
  problem.TimeInitial = timeInitial * characteristicTime;
  problem.temperature = temperature;
  problem.electronCharge = electronCharge;
  problem.vacuumPermittivity = vacuumPermittivity;
  problem.semiCondRelativePerm = semiCondRelativePerm;
  problem.BoltzmannConst = BoltzmannConst; 
  problem.thermalVoltage = BoltzmannConst*temperature/electronCharge;
  problem.characteristicLength = characteristicLength;
  problem.characteristicTime = characteristicTime;
  problem.IntrinsicDensity = IntrinsicDensity;
  problem.RadRecomboConst = RadRecomboConst;

  // Apply map to convert ElecFieldCoupingStatus from string
  // to ddpElecFielCoupling_type
  ddpElecFieldCouplingMap CouplingMap;
  CouplingMap.insert(std::pair<std::string,
			ddpElecFieldCoupling_type>("On",On));
  CouplingMap.insert(std::pair<std::string,
			ddpElecFieldCoupling_type>("Off",Off));
  problem.ElecFieldCouplingStatus = CouplingMap[ElecFieldCouplingStatus];


  // Apply map to convert charge sign from string to ddpChargeSign_type
  ddpChargeSignMap_type ChargeSignMap;
  ChargeSignMap.insert(std::pair<std::string, 
			ddpChargeSign_type>("Positive",Positive));
  ChargeSignMap.insert(std::pair<std::string, 
			ddpChargeSign_type>("Negative",Negative));

  // Assign read in values to carrierConsts
  carrierConstants.electron_Mobility = electronMobility;
  carrierConstants.electron_RecombinationTime = electronRecomboTime;
  carrierConstants.electron_ChargeSign = ChargeSignMap[electronChargeSign];

  // Assign read in values to carrierConsts
  carrierConstants.hole_Mobility = holeMobility;
  carrierConstants.hole_RecombinationTime = holeRecomboTime;
  carrierConstants.hole_ChargeSign = ChargeSignMap[holeChargeSign];

  return 0;
}


int ddpPrintGrid(ddpGrid_type const & grid)
{
  // This function will print out all the information about the grid;

  std::cout << "The grid has " << grid.NumElementsWithGhost 
	    << " elements." << std::endl;
  std::cout << "We list the information each element has." << std::endl;
  for(int i = 0; i < grid.NumElementsWithGhost; ++i)
    {
      std::cout << "element[" << i << "]" << std::endl;
      std::cout << "The left endpoint of element[" << i << "] is " 
		<< grid.ElementList[i].Left << std::endl;
      std::cout << "The right endpoint of element[" << i << "] is " 
		<< grid.ElementList[i].Right << std::endl;
    }

    std::cout << "Remember last element is a ghost element" << std::endl;

  return 0;
}

int ddpPrintTimeStamps(std::vector< double > const & timeStamps)
{
  // prints the number of times stamps that were put into the
  // code using the input file and also will print out to the screen
  // an array of the actual times it will print 
  std::cout << "We request a total of "  << timeStamps.size()
	    << " snap shots to be taken " << std::endl;
  std::cout << "We request a \"time stamp\" be taken at the following times " 
	    << std::endl;
  for(int i = 0; i < timeStamps.size(); ++i)
    {
      std::cout << "time = " << timeStamps[i] << std::endl;
    }
  return 0;
}


int
ddpPrintBij_Backward(ddpBijBack_type const & bij)
{
  // This will print the backward bijection to the screen.
  // It will print the [element number, polynomial order]
  // and the global degree of freedom it gets mapped to
  // 
  //
  // [elem, order] - > [index]
  

  int bijLength = bij.size();
  int globalCount;
  std::pair<int, int > orderedPair;
  std::cout << "bijLength = " << bijLength << std::endl;
  int a;
  int b;
  for(int i = 0; i < bijLength; ++i)
    {
      globalCount = i;
      orderedPair = bij.find(i)->second;
      a = orderedPair.first;
      b = orderedPair.second;
      std::cout << "The ordered pair ["<< a <<","<< b<< "] gets mapped to " << i
		<< ". Line number " << __LINE__  << std::endl;
    }
  return 0;
}

	      
	      
int
ddpPrintState(ddpCarrier_type const & carrier1,
	      ddpPoisson_type const & poisson,
	      int const & timeStamp
	      )
{
  // THIS WILL PRINT THE STATE OF THE SIMULATION AT TIME STAMPS
  // THE FILE WILL BE NAMED "State(TimeStamp#).dat" and will contain
  // Space delimited values:
  // pt uPTval qPTval ElectricFieldPTVal PotentialPTVal JPTval
  
  // this is to deal with the name of the file
  ofstream ptr;
  std::string prefix = "State";
  std::string extension = ".dat";
  std::string filename;
  std::stringstream ss;

  char snapShotNumberString[3];
  
  sprintf (snapShotNumberString, "%4.4d", timeStamp);
  
  // Use stringstream toconcatenate the strings using C++ including '\0' 
  // character to signify end of string.
  ss << prefix << snapShotNumberString << extension << '\0';

  // convert the string stream into a C++ string and then int a C string.
  filename = ss.str ();
  char
    str[filename.size ()];

  // convert the C++ string to a C string by copying by value.
  for (unsigned int i = 0; i < filename.size (); ++i)
    {
      str[i] = filename[i];
    }

  // Now can finally use the string str = "uUvals(snapShotNumber).txt\0"
  ptr.open (str);


    // QPTS already have diffusivity in them		
		
  ddpDenseVector_type
    UPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG 
		 * carrier1.carrierState.uDof,
    QPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG
		 * carrier1.carrierState.qDof,
    ElecPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeMX 
	 	 * poisson.PoissonState.elecDof,
    POTPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG 
		 * poisson.PoissonState.potDof;

 /// MAKE SURE EVERYTHING IS THE SAME LENGTH
  assert(carrier1.PTSDense.size() == UPTS.size());
  assert(UPTS.size() == QPTS.size());
  assert(QPTS.size() == ElecPTS.size());
  assert(ElecPTS.size() == POTPTS.size());
    

  
  // PRINTS THE LINE (SPACE DELIMTED)
  // pt uPTval qPTval ElectricFieldPTVal PotentialPTVal JPTval
  double SignOfCharge; 


   for(int i = 0; i < UPTS.size(); ++i)
     {
       ptr << carrier1.PTSDense(i) << " " 
           << UPTS(i) << " " 
           << carrier1.carrierProps.Sign4Force * QPTS(i) << " "
           << carrier1.carrierProps.Sign4Force * 
  	    carrier1.carrierProps.Mobility*ElecPTS(i)*UPTS(i)  << " "
           << ElecPTS(i) << " " 
           << POTPTS(i) << " " 
           << carrier1.carrierProps.Sign4Force *( QPTS(i) 
		+ carrier1.carrierProps.Mobility *
		 ElecPTS(i)*UPTS(i) ) << endl;
     }
   ptr.close();

  return 0;
}


int
ddpPrintState(ddpCarrier_type  & carrier1,
	      ddpCarrier_type  & carrier2,
	      ddpPoisson_type const & poisson,
	      int const & timeStamp
	      )
{
  // THIS WILL PRINT THE STATE OF THE SIMULATION AT TIME STAMPS
  // THE FILE WILL BE NAMED "State(TimeStamp#).dat" and will contain
  // Space delimited values:
  // pt uPTval qPTval ElectricFieldPTVal PotentialPTVal JPTval
  
  // this is to deal with the name of the file
  ofstream ptr;
  std::string prefix = "State";
  std::string extension = ".dat";
  std::string filename;
  std::stringstream ss;

  char snapShotNumberString[3];
  
  sprintf (snapShotNumberString, "%4.4d", timeStamp);
  
  // Use stringstream toconcatenate the strings using C++ including '\0' 
  // character to signify end of string.
  ss << prefix << snapShotNumberString << extension << '\0';

  // convert the string stream into a C++ string and then int a C string.
  filename = ss.str ();
  char
    str[filename.size ()];

  // convert the C++ string to a C string by copying by value.
  for (unsigned int i = 0; i < filename.size (); ++i)
    {
      str[i] = filename[i];
    }

  // Now can finally use the string str = "uUvals(snapShotNumber).txt\0"
  ptr.open (str);

		
  ddpDenseVector_type
    U1PTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG 
		 * carrier1.carrierState.uDof,
    Q1PTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG
		 * carrier1.carrierState.qDof,
    U2PTS = carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG 
		 * carrier2.carrierState.uDof,
    Q2PTS = carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG
		 * carrier2.carrierState.qDof,
    ElecPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeMX 
	 	 * poisson.PoissonState.elecDof,
    POTPTS = carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG 
		 * poisson.PoissonState.potDof;

 ddpDenseVector_MKL_type UPTS1_MKL = new double[U1PTS.size()];

 ddpDenseVector_MKL_type UPTS2_MKL = new double[U1PTS.size()];
	

 char transa = 'N';
   mkl_cspblas_dcsrgemv(&transa,
	      &carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.numRows,
	      carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.values,
	      carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.I,
	      carrier1.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.J,
	      carrier1.carrierState.uDof_MKL,
	      UPTS1_MKL);
   
   mkl_cspblas_dcsrgemv(&transa,
	      &carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.numRows,
	      carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.values,
	      carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.I,
	      carrier2.carrierProps.VandeMondeMatrices.globalVandeMondeDG_MKL.J,
	      carrier2.carrierState.uDof_MKL,
	      UPTS2_MKL);
	     
  


 /// MAKE SURE EVERYTHING IS THE SAME LENGTH
/*  assert(carrier1.PTSDense.size() == U1PTS.size());
  assert(UPTS.size() == QP1TS.size());
  assert(QPTS.size() == ElecPTS.size());
  assert(ElecPTS.size() == POTPTS.size());
  */  
  
  // PRINTS THE LINE (SPACE DELIMTED)
  // pt uPTval qPTval ElectricFieldPTVal PotentialPTVal JPTval
   for(int i = 0; i < U1PTS.size(); ++i)
     {
       ptr << carrier1.PTSDense(i) << " " 
           << UPTS1_MKL[i] << " " 
           << UPTS2_MKL[i] << " "
           << carrier1.carrierProps.Sign4Force*Q1PTS(i) << " "
	   << carrier2.carrierProps.Sign4Force*Q2PTS(i) << " "
           << carrier1.carrierProps.Sign4Force * 
  	    carrier1.carrierProps.Mobility*ElecPTS(i)*U1PTS(i)  << " "
           << carrier2.carrierProps.Sign4Force * 
  	    carrier2.carrierProps.Mobility*ElecPTS(i)*U2PTS(i)  << " "
           << ElecPTS(i) << " " 
           << POTPTS(i) << " " 
	   << endl;
   }
   ptr.close();
   

  return 0;
}
	      

int 
ddpExportCarrierMatrices2Market(ddpCarrierProperties_type & carrier,
			              std::string str)
{ 
 // This function exports all the carrier's matrices to be exported to 
 // market matrix form so Kent can compare them to the Mathematica
 // version
  ddpSparseMatrix_type 
    DGMassU,
    DGStiffUFromQ,
    DGFluxRightUFromQ,
    DGFluxLeftUFromQ,
    DGTotalUFromQRHS,
    DGMassQ,
    DGStiffQFromU,
    DGFluxRightQFromU,
    DGFluxLeftQFromU,
    DGFluxRightEndPointQFromBC,
    DGFluxLeftEndPointQFromBC,
    DGTotalQFromBCRHS,
    DGTotalQFromURHS;
 
  // concatenate carrier_type name str and matrix name 
  std::string nameOfMatrix = str;

  nameOfMatrix = str + "_DGMassU.mtx";
  DGMassU = carrier.MassU;
  Eigen::saveMarket(DGMassU,nameOfMatrix);  
  
  nameOfMatrix = str + "_DGMassQ.mtx"; 
  DGMassQ = carrier.MassQ;
  Eigen::saveMarket(DGMassQ, nameOfMatrix);  

  nameOfMatrix = str + "_DGStiffUFromQ.mtx"; 
  DGStiffUFromQ = carrier.StiffUFromQ;
  Eigen::saveMarket(DGStiffUFromQ, nameOfMatrix);
 
  nameOfMatrix = str + "_DGStiffQFromU.mtx";
  DGStiffQFromU = carrier.StiffQFromU;
  Eigen::saveMarket(DGStiffQFromU, nameOfMatrix);
  
  nameOfMatrix = str + "_DGFluxRightUFromQ.mtx";
  DGFluxRightUFromQ = carrier.FluxRightUFromQ;
  Eigen::saveMarket(DGFluxRightUFromQ, nameOfMatrix);  
  
  nameOfMatrix = str + "_DGFluxLeftUFromQ.mtx";
  DGFluxLeftUFromQ = carrier.FluxLeftUFromQ;
  Eigen::saveMarket(DGFluxLeftUFromQ, nameOfMatrix);  
  
  nameOfMatrix = str + "_DGFluxRightQFromU.mtx";
  DGFluxRightQFromU = carrier.FluxRightQFromU;
  Eigen::saveMarket(DGFluxRightQFromU, nameOfMatrix);
 
  nameOfMatrix = str + "_DGTotalQFromBCRHS.mtx";
  DGTotalQFromBCRHS = carrier.TotalQFromBCRHS;
  Eigen::saveMarket(DGTotalQFromBCRHS, nameOfMatrix);
 
  nameOfMatrix = str + "_DGFluxLeftQFromU.mtx";
  DGFluxLeftQFromU = carrier.FluxLeftQFromU;
  Eigen::saveMarket(DGFluxLeftQFromU, nameOfMatrix);
  
  nameOfMatrix = str + "_DGTotalQFromURHS.mtx";
  DGTotalQFromURHS = carrier.TotalQFromURHS;
  Eigen::saveMarket(DGTotalQFromURHS,nameOfMatrix );
  
  nameOfMatrix = str + "_DGTotalUFromQRHS.mtx";
  DGTotalUFromQRHS = carrier.TotalUFromQRHS;
  Eigen::saveMarket(DGTotalUFromQRHS, nameOfMatrix);

return 0;
}








