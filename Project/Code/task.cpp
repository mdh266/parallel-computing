#include<iostream>
#include<omp.h>

using std::cout;
using std::endl;


typedef struct props
{ 
	int * values;
	int len;
} props;

typedef struct poisson
{
	props properties;
	
	void initialize(int N)
	{
		properties.len = N;
		properties.values = new int[N];
	}
	
	void update(int time, int * threads, int numWorkers)
	{
		#pragma omp parallel num_threads(numWorkers)
		{
		
			#pragma omp for
			for(int i = 0; i < properties.len; i++)
				properties.values[i] = time;
		
/*			
			#if _OPENMP
			#pragma omp for
			for(int i = 0; i < numWorkers; i++)
			{
				threads[omp_get_thread_num() % numWorkers] 
								= omp_get_thread_num();
			}
			#endif
*/
		}		
		
	}

} poisson;

typedef struct carrier
{
	props properties;
	int (* fun) (int x);

	void initialize(int  N, int (* f)(int x) )
	{
		properties.len = N;
		properties.values = new int[N];
		fun = f;
	}
	
	void update(poisson const & elecfield , int * carrier_threads, int numWorkers)
	{
		// single thread doing work up that need to be done by one thread here

	
		// spawn new threads to do computationally hard work
		#pragma omp parallel num_threads(numWorkers)
		{
			// parallel work with (numthreads/2) workers!
			#pragma omp for
			for(int i = 0; i < properties.len; i++)
				properties.values[i] =  fun(elecfield.properties.values[i]) ;	
	
/*			// record which threads doing what
			#if _OPENMP
			#pragma omp for
			for(int i = 0; i < numWorkers; i ++)	
			{
				carrier_threads[omp_get_thread_num() % numWorkers]
							 = omp_get_thread_num();
			}
			#endif
*/		
		}
		// single thread goes on.. maybe 	
	} 

}carrier;




void print(int * x, int N);


int f1(int x)
{
	return x;
}

int f2(int x)
{
	return x*x;
}

int main()
{
	int N = 1000000;
	poisson elecfield;
	carrier electrons, holes;

	elecfield.initialize(N);

	electrons.initialize(N, &f1);
	holes.initialize(N, &f2);

	int numThreads;
	int * elecfield_threads;
	int * hole_threads;
	int * electron_threads;
	int numWorkers;

	int time = 0;
	int deltaT  = 1;
	int endTime = 1000;

	// only need one thread to initialize 
	#pragma omp parallel
	{
		#pragma omp single
		{
			#if _OPENMP
			numThreads = omp_get_num_threads();
			numWorkers = numThreads/2 ;
			hole_threads = new int[numWorkers];
			electron_threads = new int[numWorkers];
			elecfield_threads = new int[numThreads-1];
			#endif
		}
	}

	// electric field, holes, and electrons need to be shared!!!
	// they will be shared between worker threads, but not between
	//  master threads.

	#if _OPENMP	
	omp_set_nested(1);
	omp_set_max_active_levels(2);
	#endif

	#pragma omp parallel num_threads(2)
	{ 
		
		// Begin time steppin
		
		while(time < endTime)
		{

			// All threads used to update electric field
			#pragma omp single
			{
				elecfield.update(time, elecfield_threads, numThreads);
			}	
			// implied barrier
	
			// to time update for electrons and holes as sections
			#pragma omp sections
			{
				// update holes
				#pragma omp section
				{
					holes.update(elecfield, hole_threads,numWorkers);
				}

				// update electrons
				#pragma omp section
				{
					electrons.update(elecfield, electron_threads,numWorkers);
				}
			 } 
	
			// implied barrier
	
			// update time
			#pragma omp single
			time += deltaT;
			
			// implied barrier	
		} 

	} // implied barrier

/*
	#if _OPENMP
	cout << "Using numThreads = " << numThreads << endl;

	cout << "Updates for electric field done with threads: ";
	print(elecfield_threads, numThreads-1);

	cout << "Update for electrons done by threads: " << endl;
	print(electron_threads, numThreads/2);

	cout << "Update for holes done by threads: " << endl;
	print(hole_threads,numThreads/2);
	#endif

	cout << "Electric field = " << endl;
	print(elecfield.properties.values, N);
	
	cout << "Holes = " << endl;
	print(holes.properties.values, N);

	cout << "Electrons = " << endl;
	print(electrons.properties.values, N);


*/
	return 0;
}


		
void print(int * x, int N)
{
	for(int i = 0; i < N; i++)
		cout << x[i] << endl;

	
	cout << endl;
}


