#ifndef _POISSON_H_
#define _POISSON_H_

#include "includes.hpp"
#include "main.hpp"
#include "dopingprofile.hpp"
#include "carrier.hpp"
#include "solver.hpp"

extern "C" void MKL_FreeBuffers();


typedef struct ddpPoisson_type
{
 
  //////////////////////////////////////////////////////////////////////////////
  //DATA MEMBERS
  //////////////////////////////////////////////////////////////////////////////
  ddpPoissonProperties_type PoissonProps;
  ddpPoissonState_type PoissonState;

  //MKL Solver 
  ddpSolver_MKL_type solverABig_MKL;


  // Weights and Points Vectors
  ddpSparseVector_type
  weightsSparse,
    PTSSparse;
  
  ddpDenseVector_type
  PTSDense,
    weightsDense;
  
  // Bijections
  ddpBijection_type Bijections;
  
  // VandeMondeMatrices, dont need if memory beocomes issue
  ddpVandeMondeMatrices_type VandeMondeMatrices;
  ddpDGFluxMatrices_type DGFluxMatrices;
  
  //////////////////////////////////////////////////////////////////////////////
  // Data Functions
  //////////////////////////////////////////////////////////////////////////////
  
  // Boundary Condition Pointers
  double (* BCDirLeft) (double const & x);
  double (* BCDirRight) (double const & x);
  
  //////////////////////////////////////////////////////////////////////////////
  // Structure Methods
  //////////////////////////////////////////////////////////////////////////////
  
  int initialize(ddpGrid_type & grid,
		 ddpProblemInfo_type problem,
		 ddpCarrierConstants_type & carrierConstants
		 )
  {
    // set Lambda
    this->PoissonProps.Lambda = (problem.semiCondRelativePerm *
				 problem.vacuumPermittivity /
				 problem.electronCharge);
        
    // make the weights and points
    ddpMakeWeightsAndPoints(grid, 
			    problem, 
			    weightsSparse,
			    PTSSparse,
			    PTSDense,
			    weightsDense);
    
    // Make the bijections
    ddpMakeAllBijections(grid, Bijections);
    
    // Make the global VandeMonde Matrice and Flux Matrices 
    ddpMakeVandeMondeMatrices(grid, 
			      problem, 
			      Bijections,
			      VandeMondeMatrices,
			      DGFluxMatrices);  
    
    // Make all the Poisson Matrices
    ddpMakePoissonProperties(problem,
			     grid,
			     weightsSparse,
			     Bijections,
			     VandeMondeMatrices,
			     DGFluxMatrices,
			     PoissonProps);
    
    // Initialize solver	
    PoissonState.solverABig.compute(PoissonProps.ABig);
    
    // set the couplingStatus
    if(On == problem.ElecFieldCouplingStatus)
      this->PoissonProps.coupledOrNot = 1.0;
    else if(Off == problem.ElecFieldCouplingStatus)
      this->PoissonProps.coupledOrNot = 0.0;
    else 
      {
	cout << "Error: Incorrect Coupling Status." <<endl;
	assert(false);
      }
    
    // TODO: Make MKL VERSION
    // make BCInput a 2x1 array
    PoissonState.BCInput = ddpDenseVector_type(2);
    
    // get MX DOF
    int RowsInTop = PoissonProps.VBig.rows();
    
    // get DG DOF
    int RowsInBottom = PoissonProps.C.rows();
    
    //
    int ColsInBottom = PoissonProps.C.cols();
    
    
    // TODO:  MAKE MKL VERSION
    // make VRHS Matrix that is totalDOF x 2 
    PoissonProps.VRHS = ddpSparseMatrix_type(RowsInTop+RowsInBottom,2);
    
    // TODO: MAKE MKL VERSION
    // make CRHS Matrix that is totalDOF x 2 
    PoissonProps.CRHS = ddpSparseMatrix_type
      (RowsInTop+RowsInBottom,ColsInBottom);
    
    //copy sparse matrix over to larger one:
    for(int k = 0; k < PoissonProps.VBig.outerSize(); ++k)
      {
	for(ddpSparseMatrix_type::InnerIterator it(PoissonProps.VBig,k); 
	    it; ++it)
	  {
	    PoissonProps.VRHS.insert(it.row(), k) = it.value();
	  }
      }
    
    //copy sparse matrix over to larger one:
    for(int k = 0; k < PoissonProps.C.outerSize(); ++k)
      {
	for(ddpSparseMatrix_type::InnerIterator it(PoissonProps.C,k); it; ++it)
	  {
	    PoissonProps.CRHS.insert(RowsInTop + it.row(), k) = it.value();
	  }
      }
    
    // make RHSFromBC vector that will be 
    PoissonState.RHSFromBC = 
      ddpDenseVector_type(RowsInTop + RowsInBottom);
    
    // make RHSFromCandU vector
    PoissonState.RHSFromCandU =
      ddpDenseVector_type(RowsInTop+RowsInBottom);
    
    ///////////////////////////////////////////////////////////////////////
    // MKL version of Matrices and Vectors
    ///////////////////////////////////////////////////////////////////////
    int nnz = 0;
    int m = 0;
    int numRows = 0;
    
    // ABig
    ddpEigenSparseToIntelCsr
      (PoissonProps.ABig,
       PoissonProps.ABig_MKL);
    
    // CRHS
    ddpEigenSparseToIntelCsr
      (PoissonProps.CRHS,
       PoissonProps.CRHS_MKL);
    
    // VRHS
    ddpEigenSparseToIntelCsr
      (PoissonProps.VRHS,
       PoissonProps.VRHS_MKL);
    

    //NOTE: Okay yo use poissonProps.VandeMond... because already copied over
    //to them in ddpMakePoissonProps
    // globalVandeMondMX
    //Assign
//   cout <<  VandeMondeMatrices.globalVandeMondeMX << endl;
   ddpEigenSparseToIntelCsr
	  (PoissonProps.VandeMondeMatrices.globalVandeMondeMX,
	   PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL);
    
    // MKL version of vectors
    PoissonState.elecDof_MKL      = new double[RowsInTop];
    PoissonState.potDof_MKL       = new double[RowsInBottom];
	
    PoissonState.carrierDof_MKL   = new double[RowsInBottom];
    PoissonState.BCInput_MKL      = new double[2];
    PoissonState.RHSFromBC_MKL    = new double[RowsInTop + RowsInBottom];
    PoissonState.RHSFromCandU_MKL = new double[RowsInTop + RowsInBottom];
    PoissonState.RHSTotal_MKL     = new double[RowsInTop + RowsInBottom];
    
    PoissonState.Soln_MKL         = new double[RowsInTop + RowsInBottom];
    PoissonState.Soln_Old_MKL     = new double[RowsInTop + RowsInBottom];
    
    // Set the MKL_INTS
    PoissonProps.numDGDof_MKL = RowsInBottom;
    PoissonProps.numMXDof_MKL = RowsInTop;
    PoissonProps.numTotalDof_MKL = RowsInTop + RowsInBottom;
    PoissonProps.increment_MKL = 1;

    // Initialize the solver
    solverABig_MKL.initialize(PoissonProps.ABig_MKL);
  
    return 0;
  }


  int setDirichletBoundaryConditions( double (* BCLeft) (const double & x), 
		 	    	      double (* BCRight) (const double & x)
			   	    )
  { 
	BCDirLeft = BCLeft;
	BCDirRight = BCRight;
	return 0;
  }	
  

  ///////////////////////////////////////////////////////////////////
  // BIPOLAR MODEL
  ///////////////////////////////////////////////////////////////////


  int
  solveSystem(ddpCarrier_type const & carrier1,
	      ddpCarrier_type const & carrier2,
	      ddpDopingProfile_type const & dopingProfile,
	      const double & time,
	      ddpProblemInfo_type const & problem
	      )
  {  
  
   // This will make it so that the RHS of Poisson's equation is
   // C(x) - (n -p)
   // by using Sign4Poisson to change the constant in front of the carrier's
   // dof.  Note, the order of the carrier in the solveSystem does NOT
   // matter.
  
    PoissonState.carrierDof 
      = 
      carrier1.carrierProps.Sign4Poisson *
      carrier1.carrierState.uDof 
      +
      carrier2.carrierProps.Sign4Poisson *
      carrier2.carrierState.uDof;

    // Set carrierDofMKL = u1Dof_MKL
/*    cblas_dcopy(PoissonProps.numDGDof_MKL,
	       carrier1.carrierState.uDof_MKL,
	       PoissonProps.increment_MKL,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
 */
   //cout << "carrierDOf = " << PoissonState.carrierDof << endl;
   //cout << "carrierDof_MKL = " << endl;
 
   //for(int i = 0; i < PoissonState.carrierDof.size(); i++)
   //	cout << "carrierDof_MKL[ " << i << "] = " << PoissonState.carrierDof_MKL[i] << endl;
 


   // Set carrierDof_MKL = sign4Poisson * u1Dof_MKL
/*
   cblas_dscal(PoissonProps.numDGDof_MKL,
	       carrier1.carrierProps.Sign4Poisson,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);

   // cout << "Sign = " << carrier1.carrierProps.Sign4Poisson << endl;
   //for(int i = 0; i < PoissonState.carrierDof.size(); i++)
   //cout << "carrierDof_MKL[ " << i << "] = " << PoissonState.carrierDof_MKL[i] << endl;
   

   // Set carrierDof_MKL = sign4Poisson * u1Dof_MKL + sign4Poisson * u2Dof_MKL
 

   cblas_daxpy(PoissonProps.numDGDof_MKL,
	       carrier2.carrierProps.Sign4Poisson,
	       carrier2.carrierState.uDof_MKL,
	       PoissonProps.increment_MKL,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
  */ 
   //for(int i = 0; i < PoissonState.carrierDof.size(); i++)
   //cout << "carrierDof_MKL[ " << i << "] = " << PoissonState.carrierDof_MKL[i] << endl;
//   isSame(PoissonState.carrierDof, PoissonState.carrierDof_MKL);
   solveSystem(PoissonState.carrierDof,dopingProfile, time, problem); 
   
   return 0;
  }

  ///////////////////////////////////////////////////////////////////
  // UNIPOLAR MODEL
  ///////////////////////////////////////////////////////////////////
  
  int
  solveSystem(ddpCarrier_type const & carrier1,
	      ddpDopingProfile_type const & dopingProfile,
	      const double & time,
	      ddpProblemInfo_type const & problem
	      )
  { 
 
   // This will make it so that the RHS of Poisson's equation is
   // C(x) - n     OR    C(x) - (-p) = C(x) + p\
   // by using Sign4Poisson to change the constant infront of the carriers
   // dof 
   PoissonState.carrierDof = carrier1.carrierProps.Sign4Poisson*
					carrier1.carrierState.uDof;
/*
 // Set carrierDofMKL = u1Dof_MKL
   cblas_dcopy(PoissonProps.numDGDof_MKL,
	       carrier1.carrierState.uDof_MKL,
	       PoissonProps.increment_MKL,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
   
   // Set carrierDofMKL = sign4Poisson * u1Dof_MKL
   cblas_dscal(PoissonProps.numDGDof_MKL,
	       carrier1.carrierProps.Sign4Poisson,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
   
   
   // solveSystem(PoissonState.carrierDof_MKL,dopingProfile, time, problem); 

*/
   
 //   isSame(PoissonState.carrierDof, PoissonState.carrierDof_MKL);


    solveSystem(PoissonState.carrierDof,dopingProfile, time, problem); 
    return 0;
  }

  ///////////////////////////////////////////////////////////////////
  // BOTH MODELS
  ///////////////////////////////////////////////////////////////////

  int
  solveSystem(ddpDenseVector_type const & carrier1Dof,
	      ddpDopingProfile_type const & dopingProfile,
	      const double & time,
	      ddpProblemInfo_type const & problem
	      )
 { 

  grvy_timer_begin("SolveMXSystem"); 

  int numDofPotential = PoissonProps.C.rows();
  int numDofElec = PoissonProps.ABig.rows() - numDofPotential;

  // Update the BCInput Vector
  PoissonState.BCInput(0) = BCDirLeft(time);
  PoissonState.BCInput(1) = BCDirRight(time);
  
  //Update the RHSFromBC vector
  PoissonState.RHSFromBC =  -PoissonProps.VRHS * PoissonState.BCInput;

/*  // Update the BCInput Vector
  PoissonState.BCInput_MKL[0] = -BCDirLeft(time);
  PoissonState.BCInput_MKL[1] = -BCDirRight(time);
 
  char transa = 'N';
  
  mkl_cspblas_dcsrgemv(
			&transa,
			&PoissonProps.VRHS_MKL.numRows,
			PoissonProps.VRHS_MKL.values,
			PoissonProps.VRHS_MKL.I,
			PoissonProps.VRHS_MKL.J,
			PoissonState.BCInput_MKL,
			PoissonState.RHSFromBC_MKL
		       );

  cout << "RHSFromBC = " << PoissonState.RHSFromBC << endl;

  cout << "RHSFromBC_MKL = " << endl;
 
  for(int i = 0; i < PoissonState.RHSFromBC.size(); i++)
 	cout << "RHSFromBC_MKL[ " << i << "] = " << PoissonState.RHSFromBC_MKL[i] << endl;
*/

 //Update the RHSFRomCandU vector
  PoissonState.RHSFromCandU = PoissonProps.CRHS * 
		(dopingProfile.Dof -PoissonProps.coupledOrNot* carrier1Dof) *
		(1.0/PoissonProps.Lambda);

/* 
 // set RHSFromCandU_MKL = CRHS * dopinprofileDof
  mkl_cspblas_dcsrgemv(
		       &transa,
		       &PoissonProps.CRHS_MKL.numRows,
		       PoissonProps.CRHS_MKL.values,
		       PoissonProps.CRHS_MKL.I,
		       PoissonProps.CRHS_MKL.J,
		       dopingProfile.Dof_MKL,
		       PoissonState.RHSFromCandU_MKL
		       );
 //
 // set RHSFromCandU_MKL = 1/lambda * (CRHS * dopinprofileDof - coupled *uDpof)
  double alpha = -PoissonProps.coupledOrNot/PoissonProps.Lambda;
  double beta =  1.0/PoissonProps.Lambda;
  
  char matsecra[6];
  matsecra[0] = 'G';
  matsecra[1] = 'L';
  matsecra[2] = 'N';
  matsecra[3] = 'C';
  
  mkl_dcsrmv( 
	     &transa,
	     &PoissonProps.CRHS_MKL.numRows,
	     &PoissonProps.CRHS_MKL.numCols,
	     &alpha,
	     matsecra,
	     PoissonProps.CRHS_MKL.values,
	     PoissonProps.CRHS_MKL.J,
	     PoissonProps.CRHS_MKL.I,
	     PoissonProps.CRHS_MKL.I + 1,
	     PoissonState.carrierDof_MKL,
	     &beta,
	     PoissonState.RHSFromCandU_MKL
	    );

	     
  cout << "RHSFromCandU" << PoissonState.RHSFromCandU << endl;
  cout << "RHSFromCandU_MKL =" << endl;

  for(int i = 0; i < PoissonState.RHSFromCandU.size(); i++)
	cout << "RHSFromCandU_MKL[" << i << "] = " << PoissonState.RHSFromCandU[i] << endl;
*/

  
  PoissonState.RHSTotal = PoissonState.RHSFromBC + PoissonState.RHSFromCandU;



// Set PoissonState.RHSTotal_MKL = PoissonState.RHSFromBC_MKL
/*  cblas_dcopy(PoissonProps.numTotalDof_MKL,
	      PoissonState.RHSFromBC_MKL,
	      PoissonProps.increment_MKL,
	      PoissonState.RHSTotal_MKL,
	      PoissonProps.increment_MKL);
  
  
  double a = 1.0;

     
// Set PoissonState.RHSTotal_MKL = PoissonState.RHSFromBC_MKL + PoissonState.RHSFromCandU 
    cblas_daxpy(PoissonProps.numTotalDof_MKL,
		   a,
		   PoissonState.RHSFromCandU_MKL,
		   PoissonProps.increment_MKL,
		   PoissonState.RHSTotal_MKL,
		   PoissonProps.increment_MKL);

	     
  cout << "RHSTotal = " << PoissonState.RHSTotal << endl;
  cout << "RHSTotal_MKL =" << endl;

  for(int i = 0; i < PoissonState.RHSTotal.size(); i++)
	cout << "RHSTotal_MKL[" << i << "] = " << PoissonState.RHSTotal[i] << endl;
*/
  
 //Solve the system  (Both potential and electrif field at same time)
  grvy_timer_begin("MX Solution Line");
  
  
  // This line solvers Abig * PoissonState.Soln = PoissonStateRHSTotal for PoissonState.Soln
  PoissonState.Soln = PoissonState.solverABig.solve(PoissonState.RHSTotal);
  
  if(this->PoissonState.solverABig.info() != Eigen::Success)
    {
      cout << "failed in the solver step" << endl;
      assert(false);
    }
  
  
  
  grvy_timer_end("MX Solution Line");
  
  // MKL Version
  // Solve ABig_MKL * PoissonState.Soln_MKL = PoissonStateRHSTotal_MKL for PoissonState.Soln_MKL
  
  

  
  
  //Extract the parts that make up the solution
 
  // Extract the electric field dof from top of solution vector 
  PoissonState.elecDof = PoissonState.Soln.head(numDofElec);
  // Extract the potential dof from the bottom of the solution vector
  PoissonState.potDof  = PoissonState.Soln.tail(numDofPotential);
/*  

  // TODO: CHANGE THIS WHEN MKL VERSION OF STUFF DONE
  for(int i = 0; i < PoissonState.Soln.size(); i++)
  	PoissonState.Soln_MKL[i] = PoissonState.Soln(i);

   
  // Extract the electric field dof from top of solution vector 
  cblas_dcopy(PoissonProps.numMXDof_MKL,
	      PoissonState.Soln_MKL,
	      PoissonProps.increment_MKL,
	      PoissonState.elecDof_MKL,
	      PoissonProps.increment_MKL);

  // Extract the potential dof from the bottom of the solution vector
  cblas_dcopy(PoissonProps.numDGDof_MKL,
	      &(PoissonState.Soln_MKL[PoissonProps.numMXDof_MKL]),
	      PoissonProps.increment_MKL,
	      PoissonState.potDof_MKL,
	      PoissonProps.increment_MKL);
  

  cout << "elecDof = " << endl;
  cout << PoissonState.elecDof << endl;
  cout << "elecDof_MKL = " << endl;
  for(int i = 0; i < PoissonState.elecDof.size(); i++)
	cout << PoissonState.elecDof_MKL[i] << endl;
  
  cout << "potDof = " << endl;
  cout << PoissonState.potDof << endl;
  cout << "potDof_MKL = " << endl;
  for(int i = 0; i < PoissonState.potDof.size(); i++)
	cout << PoissonState.potDof_MKL[i] << endl;
 

  */
 grvy_timer_end("SolveMXSystem"); 
  
  
  return 0;
  }


///////////////////////////////////////////////////////////////////////////////
  // MKL VERSIONS
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
  // Bipolar Model
///////////////////////////////////////////////////////////////////////////////
  int
  solveSystem_MKL(ddpCarrier_type const & carrier1,
		  ddpCarrier_type const & carrier2,
		  ddpDopingProfile_type const & dopingProfile,
		  const double & time,
		  ddpProblemInfo_type const & problem
		  )
  {

    // This will make it so that the RHS of Poisson's equation is
    // C(x) - (n - p)
    // by using Sign4Poisson to change the constant infront of the carriers
    // dof.  Note, the order of the carrier in the solveSystem does NOT
    // matter.
    
    
    // Set carrierDof_MKL = u1Dof_MKL
    cblas_dcopy(PoissonProps.numDGDof_MKL,
		carrier1.carrierState.uDof_MKL,
		PoissonProps.increment_MKL,
		PoissonState.carrierDof_MKL,
		PoissonProps.increment_MKL);
    
    // Set carrierDof_MKL = sign4Poisson * u1Dof_MKL
    cblas_dscal(PoissonProps.numDGDof_MKL,
		carrier1.carrierProps.Sign4Poisson,
		PoissonState.carrierDof_MKL,
		PoissonProps.increment_MKL);

    // Set carrierDof_MKL = sign4Poisson * u1Dof_MKL + sign4Poisson * u2Dof_MKL
    cblas_daxpy(PoissonProps.numDGDof_MKL,
		carrier2.carrierProps.Sign4Poisson,
		carrier2.carrierState.uDof_MKL,
		PoissonProps.increment_MKL,
		PoissonState.carrierDof_MKL,
		PoissonProps.increment_MKL);
    
    // Now we have reduced the problem into one that the other version can
    // handle
    solveSystem_MKL(PoissonState.carrierDof_MKL,dopingProfile, time, problem); 

    return 0;
  }

  ///////////////////////////////////////////////////////////////////
  // UNIPOLAR MODEL
  ///////////////////////////////////////////////////////////////////
  
  int
  solveSystem_MKL(ddpCarrier_type const & carrier1,
		  ddpDopingProfile_type const & dopingProfile,
		  const double & time,
		  ddpProblemInfo_type const & problem
		  )
  { 
 
   // This will make it so that the RHS of Poisson's equation is
   // C(x) - n     OR    C(x) - (-p) = C(x) + p\
   // by using Sign4Poisson to change the constant infront of the carriers
   // dof 

    // Set carrierDofMKL = u1Dof_MKL

    cblas_dcopy(PoissonProps.numDGDof_MKL,
	       carrier1.carrierState.uDof_MKL,
	       PoissonProps.increment_MKL,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
   

 // Set carrierDofMKL = sign4Poisson * u1Dof_MKL
   cblas_dscal(PoissonProps.numDGDof_MKL,
	       carrier1.carrierProps.Sign4Poisson,
	       PoissonState.carrierDof_MKL,
	       PoissonProps.increment_MKL);
   

   solveSystem_MKL(PoissonState.carrierDof_MKL,dopingProfile, time, problem); 

    return 0;
  }


  ///////////////////////////////////////////////////////////////////
  // BOTH MODELS
  ///////////////////////////////////////////////////////////////////

  int
  solveSystem_MKL(ddpDenseVector_MKL_type const & carrier1Dof,
		  ddpDopingProfile_type const & dopingProfile,
		  const double & time,
		  ddpProblemInfo_type const & problem
		  )
  { 
    

    grvy_timer_begin("SolveMXSystem MKL"); 
    
    
    
    
    // Update the BCInput Vector
    PoissonState.BCInput_MKL[0] = -BCDirLeft(time);
    PoissonState.BCInput_MKL[1] = -BCDirRight(time);
    
    char transa = 'N';
  
    //Update the RHSFromBC vector  = -VRHS * BCInput
    
    mkl_cspblas_dcsrgemv(
			 &transa,
			 &PoissonProps.VRHS_MKL.numRows,
			 PoissonProps.VRHS_MKL.values,
			 PoissonProps.VRHS_MKL.I,
			 PoissonProps.VRHS_MKL.J,
			 PoissonState.BCInput_MKL,
			 PoissonState.RHSFromBC_MKL
			 );

    // set RHSFromCandU_MKL = CRHS * dopinprofileDof
    mkl_cspblas_dcsrgemv(
			 &transa,
			 &PoissonProps.CRHS_MKL.numRows,
			 PoissonProps.CRHS_MKL.values,
			 PoissonProps.CRHS_MKL.I,
			 PoissonProps.CRHS_MKL.J,
			 dopingProfile.Dof_MKL,
			 PoissonState.RHSFromCandU_MKL
			 );
    // set RHSFromCandU_MKL 
    // = 1/lambda * (CRHS * dopinprofileDof - coupled *uDpof)
    double alpha = -PoissonProps.coupledOrNot/PoissonProps.Lambda;
    double beta =  1.0/PoissonProps.Lambda;
    
    char matsecra[6];
    matsecra[0] = 'G';
    matsecra[1] = 'L';
    matsecra[2] = 'N';
    matsecra[3] = 'C';
    
    mkl_dcsrmv( 
	       &transa,
	       &PoissonProps.CRHS_MKL.numRows,
	       &PoissonProps.CRHS_MKL.numCols,
	       &alpha,
	       matsecra,
	       PoissonProps.CRHS_MKL.values,
	       PoissonProps.CRHS_MKL.J,
	       PoissonProps.CRHS_MKL.I,
	       PoissonProps.CRHS_MKL.I + 1,
	       PoissonState.carrierDof_MKL,
	       &beta,
	       PoissonState.RHSFromCandU_MKL
		);
    
    // Set PoissonState.RHSTotal_MKL = PoissonState.RHSFromBC_MKL
    cblas_dcopy(PoissonProps.numTotalDof_MKL,
		PoissonState.RHSFromBC_MKL,
		PoissonProps.increment_MKL,
		PoissonState.RHSTotal_MKL,
		PoissonProps.increment_MKL);
    
 		
    alpha = 1.0;
    
    
    // Set PoissonState.RHSTotal_MKL to
    // PoissonState.RHSFromBC_MKL + PoissonState.RHSFromCandU_MKL 
    cblas_daxpy(PoissonProps.numTotalDof_MKL,
		alpha,
		PoissonState.RHSFromCandU_MKL,
		PoissonProps.increment_MKL,
		PoissonState.RHSTotal_MKL,
		PoissonProps.increment_MKL);
    

  //Solve the system  (Both potential and electrif field at same time)
    grvy_timer_begin("MX Solution Line MKL");
    solverABig_MKL.solve(PoissonProps.ABig_MKL,
			  PoissonState.RHSTotal_MKL,
			  PoissonState.Soln_MKL
			 );
    grvy_timer_end("MX Solution Line MKL");

   


    //Extract the parts that make up the solution
        
    // Extract the electric field dof from top of solution vector 
    cblas_dcopy(PoissonProps.numMXDof_MKL,
		PoissonState.Soln_MKL,
		PoissonProps.increment_MKL,
		PoissonState.elecDof_MKL,
		PoissonProps.increment_MKL);
    
    // Extract the potential dof from the bottom of the solution vector
    cblas_dcopy(PoissonProps.numDGDof_MKL,
		&(PoissonState.Soln_MKL[PoissonProps.numMXDof_MKL]),
		PoissonProps.increment_MKL,
		PoissonState.potDof_MKL,
		PoissonProps.increment_MKL);
    
    // Swap pointers for PoissonState.Soln_Old_MKL and PoissonState.Soln_MKL
    
    
    /*ddpDenseVector_MKL_type tempMKLVector;
    tempMKLVector = PoissonState.Soln_Old_MKL;
    PoissonState.Soln_Old_MKL = PoissonState.Soln_MKL;
    PoissonState.Soln_MKL = tempMKLVector;
    */
    
    
    grvy_timer_end("SolveMXSystem MKL"); 
    
    return 0;
  }
  

  int getElecFieldVals_MKL(ddpDenseVector_MKL_type & EPTS_MKL)
  {
	
    char transa = 'N'; 
    double alpha = 1.0;
    double beta =  0.0;
    
    char matsecra[6];
    matsecra[0] = 'G';
    matsecra[1] = 'L';
    matsecra[2] = 'N';
    matsecra[3] = 'C';
/*    
    mkl_dcsrmv( 
	       &transa,
	       &PoissonProps.CRHS_MKL.numRows,
	       &PoissonProps.CRHS_MKL.numCols,
	       &alpha,
	       matsecra,
	       PoissonProps.CRHS_MKL.values,
	       PoissonProps.CRHS_MKL.J,
	       PoissonProps.CRHS_MKL.I,
	       PoissonProps.CRHS_MKL.I + 1,
	       PoissonState.carrierDof_MKL,
	       &beta,
	       PoissonState.RHSFromCandU_MKL
		);
  */  
    // Set PoissonState.RHSTotal_MKL = PoissonState.RHSFromBC_MKL
  
    mkl_dcsrmv(
		&transa,
		&PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.numRows,
		&PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.numCols,
	        &alpha,
	        matsecra,
		PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.values,
		PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.J,
		PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.I,
		PoissonProps.VandeMondeMatrices.globalVandeMondeMX_MKL.I+1,
		PoissonState.elecDof_MKL,
		&beta,
		EPTS_MKL
		);

	return 0;
  }

  int getElecFieldVals(ddpDenseVector_type & EPTS)
  {
	EPTS = VandeMondeMatrices.globalVandeMondeMX * PoissonState.elecDof;
	return 0;
  }

  
  
  int isSame(ddpDenseVector_type const & eigen, ddpDenseVector_MKL_type const & MKL)
  {
	int tol = 0.1; 
	
	for(int i = 0; i < eigen.size(); i++)
	{
		if( abs((eigen(i) - MKL[i] ) ) > tol )
		{
			cout << "diff = "<< (eigen(i) - MKL[i]) << endl;
			cout << "Eigen[ " << i << "] = " << eigen(i) << endl;
			cout << "MKL[ " << i << "] = " << MKL[i] << endl;
			assert(false);

		}
	}
	
	return 0;
  }


  int free( )
  {
    delete [] PoissonState.elecDof_MKL;
    delete [] PoissonState.potDof_MKL;
	
    delete [] PoissonState.carrierDof_MKL;
    delete [] PoissonState.BCInput_MKL;
    delete [] PoissonState.RHSFromBC_MKL;
    delete [] PoissonState.RHSFromCandU_MKL;
    delete [] PoissonState.RHSTotal_MKL;
    
    delete [] PoissonState.Soln_MKL;
    delete [] PoissonState.Soln_Old_MKL;
    
    return 0; 
  }

}  ddpPoisson_type;
#endif

