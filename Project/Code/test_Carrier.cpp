#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"

int ddpTestingMakeInputFile(void);
int print2File(ddpCarrier_type const & testCarrier);
int print2File(ddpPoisson_type const & testPoisson);
int testInitization(ddpCarrier_type const & testElectrons, ddpCarrier_type const & testHoles);


double ZeroFunction(double const & x)
{
	return 0.0;
}

double Const_BC_Zero(double const & t)
{
	return 0;
}

double Const_BC_Pi(double const & t)
{
	return -M_PI;
}

double SineFunction(double const & x)
{
	return sin(x);
}

double DiffusionSolutionElectronsU(double const & x)
{
	// solution to the diffusion equation after T = 1.0 
	return  exp(-1.0)*sin(x);
}

double DiffusionSolutionElectronsQ(double const & x)
{
	// solution to the diffusion equation after T = 1.0 
	return exp(-1.0)*cos(x);
}

double DriftDiffusionElectrons_IC(double const & x)
{
	// Drift Diffusion electrons initial conditions

	return exp(-0.5*x) * sin(x);
}

double DriftDiffusionHoles_IC(double const & x)
{
	// Drift Diffusion electrons initial conditions

	return exp(0.5*x) * sin(x);
}

double DriftDiffusionElectronsSolutionU(double const & x)
{
	// Drift Diffusion solution for electrons solution at T = 1.0
	return exp(-1.25) * exp(-0.5*x) * sin(x);
}

double DriftDiffusionElectronsSolutionQ(double const & x)
{
	// derivative of solution to Drift Diffusion for 
	// electrons solution at T = 1.0
	return exp(-1.25) * exp(-0.5*x) * ( cos(x) - 0.5*sin(x) );
}

double DriftDiffusionHolesSolutionU(double const & x)
{
	// Drift Diffusion solution for holes solution at T = 1.0
	return exp(-1.25) * exp(0.5*x) * sin(x);
}

double DriftDiffusionHolesSolutionQ(double const & x)
{
	// derivative of solution to Drift Diffusion for 
	// holes solution at T = 1.0
	return exp(-1.25) * exp(0.5*x) * ( cos(x) + 0.5*sin(x) );
}
bool Dofs_Are_Same(ddpCarrier_type const & carrier1, 
					ddpCarrier_type const & carrier2)
{
 	double tol = 1.0e-4;

	if(carrier1.carrierState.uDof.size() != carrier2.carrierState.uDof.size())
	{
		cout << "uDofs size not same" << endl;
		return false;
	}
		
	if(carrier1.carrierState.qDof.size() != carrier2.carrierState.qDof.size())
	{
		cout << "qDofs size not same" << endl;
		return false;
	}

	for(int i = 0; i < carrier1.carrierState.uDof.size(); i++)
	{
		if(abs(carrier1.carrierState.uDof(i)-carrier2.carrierState.uDof(i)) > tol)
		{
			cout << "uDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.uDof(i) << endl;
			cout << "2 = " << carrier2.carrierState.uDof(i) << endl;
			return false;
		}
	
		if(abs(carrier1.carrierState.qDof(i) - carrier2.carrierState.qDof(i)) > tol)
		{
			cout << "qDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.qDof(i) << endl;
			cout << "2 = " << carrier2.carrierState.qDof(i) << endl;
			return false;
		}
		
	}

	return true;
}

bool Dofs_Are_Same_MKL(ddpCarrier_type const & carrier1, 
		       ddpCarrier_type const & carrier2)
{
 	double tol = 1.0e-4;
	
	for(int i = 0; i < carrier1.carrierState.uDof.size(); i++)
	{
		if(abs(carrier1.carrierState.uDof_MKL[i]-carrier2.carrierState.uDof(i)) > tol)
		{
			cout << "uDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.uDof_MKL[i] << endl;
			cout << "2 = " << carrier2.carrierState.uDof(i) << endl;
			return false;
		}
	
		if(abs(carrier1.carrierState.qDof_MKL[i] - carrier2.carrierState.qDof(i)) > tol)
		{
			cout << "qDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.qDof_MKL[i] << endl;
			cout << "2 = " << carrier2.carrierState.qDof(i) << endl;
			return false;
		}
		
	}

	return true;
}


int main()
{
//  grvy_timer_init("GRVY TIMING");
//  grvy_timer_reset();


  // Initial set up stuff
  ddpTestingMakeInputFile();  
  
  ddpDomain_type testdomain;
  ddpGrid_type testgrid;
  ddpProblemInfo_type testproblem;
  ddpCarrierConstants_type testConstants;

  readInput(testdomain, testgrid, testproblem, testConstants, "ddp-test");
  
  //Changing end points so that I can come up with manufactored solutions
  testdomain.LeftEndPoint = 0.0;
  testdomain.RightEndPoint = M_PI;


  ddpMakeUniformGrid(testproblem, testdomain, testgrid); 
  ddpPrintGrid(testgrid);
  
  ddpPoisson_type testPoisson;
  ddpCarrier_type testElectrons;
  ddpCarrier_type testHoles;
  ddpCarrier_type trueSolutionElectrons;
  ddpCarrier_type trueSolutionHoles;

  testElectrons.initialize(testgrid, testproblem, testConstants, "electrons");
  testHoles.initialize(testgrid, testproblem, testConstants, "holes");
  trueSolutionElectrons.initialize(testgrid, testproblem, testConstants, "electrons");
  trueSolutionHoles.initialize(testgrid, testproblem, testConstants, "holes");
  ddpDopingProfile_type dopingProfile;

  

  testPoisson.initialize(testgrid, testproblem, testConstants); 
 
  // set Diffusivity = 1.0 in both carriers
  cout << "\n\n\t\tSetting Diffusivity = 1.0 and remaking carrierProps." << endl;  
  testElectrons.carrierProps.Diffusivity = 1.0;
  testHoles.carrierProps.Diffusivity = 1.0;
  testElectrons.remakeProperties(testgrid,testproblem);
  testHoles.remakeProperties(testgrid,testproblem);
 
  cout << "\n\n1.) Testing Initialization " << endl;
  testInitization(testElectrons, testHoles);
  cout << "\n PASSED!\n\n" << endl;


  // Things to use to project funtion onto a basis
  ddpBijForw_type DGForward = testPoisson.Bijections.DGForward;
  ddpBijForw_type MXForward = testPoisson.Bijections.MXForward;

  ddpSparseMatrix_type globalVandeMondeDG = 
			testPoisson.VandeMondeMatrices.globalVandeMondeDG;
  
  ddpSparseMatrix_type globalVandeMondeMX = 
			testPoisson.VandeMondeMatrices.globalVandeMondeMX;

  ddpBijForw_type PTForward = testPoisson.Bijections.PTForward;

  ddpSparseVector_type sparseWeights = testPoisson.weightsSparse;

  ddpSparseMatrix_type MassU = testElectrons.carrierProps.MassU;
  ddpSparseMatrix_type A00 = testPoisson.PoissonProps.A00;

  ddpBijFlag_type BIJFlagDG = DG; 
  ddpBijFlag_type BIJFlagMX = Mixed; 

 
  // sets lambdda = 1
  testPoisson.PoissonProps.Lambda = 1.0;  
  
  cout << "Testing:  dn/dt - d/dx (En) =  d^2/dx^2 (n) " << 
   	"With  - d^2/dx^2 Phi = C(x),  E = -d/dx Phi."
       << endl << endl;

///////////////////////////////////////////////////////////////////////////////
// DIFFUSION WITH ELECTRONS
///////////////////////////////////////////////////////////////////////////////
	

  cout << "2.) Testing Phi(0) = 0, Phi(pi) = 0, C(x) = 0\t ==>\t E = 0" 
	<< "\n n(0,t) = 0, n(pi) = 0, n(x,0) = sin(x)" << endl << endl; 
         

  // set n(x,t) = sin(x)
  testElectrons.setInitialConditions(testgrid, &SineFunction);
  
  // set n(0) = 0, n(pi) = 0
  testElectrons.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				       &Const_BC_Zero);
  
  // set Phi(0) = 0, Phi(pi) = 0
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_Zero);

  // Now set C(x) = 0
  dopingProfile.setDopingProfile(testgrid, testElectrons, &ZeroFunction);
 
  
  // Solve for Phi, time = 0.0.
  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
  testPoisson.solveSystem_MKL(testElectrons, dopingProfile, 0.0, testproblem);
 
 
  //   Construct TRUE solution
  ddpProjectFunction(&DiffusionSolutionElectronsU,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.uDof);

  ddpProjectFunction(&DiffusionSolutionElectronsQ,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.qDof);


  print2File(testPoisson);
  ddpDenseVector_type EPTS;
  ddpDenseVector_MKL_type EPTS_MKL = new double[testgrid.NumElementsNoGhost+1];
  ddpDenseVector_type ElecFieldDof;
  ddpDenseVector_MKL_type ElecFieldDof_MKL = new double[MXForward.size()]; 


  ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
  testElectrons.makeDot_MKL(ElecFieldDof_MKL, 0.0);

  ElecFieldDof = testPoisson.PoissonState.elecDof;
  testElectrons.makeDot(ElecFieldDof, 0.0);

  print2File(trueSolutionElectrons);
  //print2File(testElectrons);
  

  double Tend = 1.0;
  double tCurrent = 0.0;
  double DeltaT;
  double DeltaT_MKL;
  
  while(tCurrent < Tend)
  {

         testPoisson.getElecFieldVals(EPTS);

 	 ddpComputeDeltaT(testgrid,
		          EPTS,
	        	  testElectrons,
	         	  DeltaT);
   
//         testPoisson.getElecFieldVals_MKL(EPTS_MKL);
/*
 	 ddpComputeDeltaT(testgrid,
		          EPTS_MKL,
	        	  testElectrons,
	         	  DeltaT_MKL);
*/
  	 
	ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
 	testElectrons.makeDot_MKL(ElecFieldDof_MKL, 0.0);
         
	 ElecFieldDof = testPoisson.PoissonState.elecDof;
	 testElectrons.makeDot(ElecFieldDof, tCurrent);


	 testElectrons.forwardEuler(DeltaT);
	 testElectrons.forwardEuler_MKL(DeltaT);	 
   	 tCurrent += DeltaT;
  }
//  print2File(trueSolutionElectrons)
//  print2File(testElectrons);

  if(Dofs_Are_Same(testElectrons, trueSolutionElectrons))
	cout << "Eigen PASSED!" << endl;
  else
	cout << "Eigen FAILED!" << endl;

  if(Dofs_Are_Same_MKL(testElectrons, trueSolutionElectrons))
	cout << "MKL PASSED!" << endl;
  else
	cout << "MKL FAILED!" << endl;

///////////////////////////////////////////////////////////////////////////////
// DRIFT DIFFUSION FOR ELECTRONS
/////////////////////////////////////////////////////////////////////////////// 

 cout << "\n\n3.) Testing Phi(0) = 0, Phi(pi) = -pi, C(x) = 0 \t==> \tE =+1 " 
	<< "\n n(0,t) = 0, n(pi) = 0, n(x,0) = exp(-0.5x)sin(x)" << endl << endl; 

  // Set Phi(0) = 0, Phi(pi) = -pi
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_Pi);
  

  // Now set C(x) = 0.0
  dopingProfile.setDopingProfile(testgrid, testElectrons, &ZeroFunction);
  
 
 
  // Solve for Phi, time = 0.0.
  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
  testPoisson.solveSystem_MKL(testElectrons, dopingProfile, 0.0, testproblem);

  // set n(x,t) = exp(-0.5x) sin(x)
  testElectrons.setInitialConditions(testgrid, &DriftDiffusionElectrons_IC);

  
  ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
  testElectrons.makeDot_MKL(ElecFieldDof_MKL, 0.0);
  
  ElecFieldDof = testPoisson.PoissonState.elecDof;
  testElectrons.makeDot(ElecFieldDof, 0.0);

  print2File(testPoisson);
  print2File(testElectrons);

  //   Construct TRUE solution
  ddpProjectFunction(&DriftDiffusionElectronsSolutionU,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.uDof);

  ddpProjectFunction(&DriftDiffusionElectronsSolutionQ,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.qDof);


  tCurrent = 0.0;

  while(tCurrent < Tend)
  {	 


         testPoisson.getElecFieldVals(EPTS);

 	 ddpComputeDeltaT(testgrid,
		          EPTS,
	        	  testElectrons,
	         	  DeltaT);
  	 
/*         testPoisson.getElecFieldVals_MKL(EPTS_MKL);
 	
	 ddpComputeDeltaT(testgrid,
		          EPTS_MKL,
	        	  testElectrons,
	         	  DeltaT_MKL);

 	  cout << "T = " << DeltaT << endl;
 	  cout << "T_mkl = " << DeltaT_MKL << endl;

	 assert( abs(DeltaT_MKL - DeltaT) > 0.003);
*/
  	 ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
 	 testElectrons.makeDot_MKL(ElecFieldDof_MKL, 0.0);

         ElecFieldDof = testPoisson.PoissonState.elecDof;
	 testElectrons.makeDot(ElecFieldDof, tCurrent);

	 testElectrons.forwardEuler(DeltaT);
	 testElectrons.forwardEuler_MKL(DeltaT);

	 tCurrent += DeltaT;
  }
  
  if(Dofs_Are_Same(testElectrons, trueSolutionElectrons))
	cout << "Eigen PASSED!" << endl;
  else
	cout << "Eigen FAILED!" << endl;

  if(Dofs_Are_Same_MKL(testElectrons, trueSolutionElectrons))
	cout << "MKL PASSED!" << endl;
  else
	cout << "MKL FAILED!" << endl;
///////////////////////////////////////////////////////////////////////////////
// DRIFT DIFFUSION FOR HOLES
///////////////////////////////////////////////////////////////////////////////
 

  cout << "\n\n\nTesting:  dp/dt + d/dx (Ep) =  d^2/dx^2 (p) " << 
   	"With  - d^2/dx^2 Phi = C(x),  E = -d/dx Phi."
       << endl << endl;
	

  cout << "4.) Testing Phi(0) = 0, Phi(pi) = -pi, C(x) = 0\t\t ==>\t E = +1" 
	<< "\n p(0,t) = 0, p(pi) = -pi, p(x,0) = exp(0.5x)sin(x)" << endl << endl; 
      
  // set p(x,t) = exp(0.5x) sin(x)
  testHoles.setInitialConditions(testgrid, &DriftDiffusionHoles_IC);

  // set p(0) = 0, p(pi) = 0
  testHoles.setDirichletBoundaryConditions(&Const_BC_Zero,
	   			           &Const_BC_Zero);

  ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
  testHoles.makeDot_MKL(ElecFieldDof_MKL, 0.0);

  ElecFieldDof = testPoisson.PoissonState.elecDof;
  testHoles.makeDot(ElecFieldDof, 0.0);
  
//  print2File(testHoles);

  //   Construct TRUE solution
  ddpProjectFunction(&DriftDiffusionHolesSolutionU,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionHoles.carrierState.uDof);

  ddpProjectFunction(&DriftDiffusionHolesSolutionQ,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionHoles.carrierState.qDof);

  print2File(trueSolutionHoles);
  tCurrent = 0.0;
  DeltaT;

  while(tCurrent < Tend)
  {
         testPoisson.getElecFieldVals_MKL(EPTS_MKL);
 	 ddpComputeDeltaT(testgrid,
		          EPTS_MKL,
	        	  testHoles,
	         	  DeltaT_MKL);
 /*    
	 testPoisson.getElecFieldVals(EPTS);
 	 ddpComputeDeltaT(testgrid,
		          EPTS,
	        	  testHoles,
	         	  DeltaT);

	 assert( abs(DeltaT_MKL - DeltaT) > 0.003);
*/
  	 ElecFieldDof_MKL = testPoisson.PoissonState.elecDof_MKL;
 	 testHoles.makeDot_MKL(ElecFieldDof_MKL, 0.0);
       
	 ElecFieldDof = testPoisson.PoissonState.elecDof;
	 testHoles.makeDot(ElecFieldDof, tCurrent);
	 

	 testHoles.forwardEuler(DeltaT);
	 testHoles.forwardEuler_MKL(DeltaT);

	 tCurrent += DeltaT;
  }

  if(Dofs_Are_Same(testHoles, trueSolutionHoles))
	cout << "Eigen PASSED!" << endl;
  else
	cout << "Eigen FAILED!" << endl;

  if(Dofs_Are_Same_MKL(testHoles, trueSolutionHoles))
	cout << "MKL PASSED!" << endl;
  else
	cout << "MKL FAILED!" << endl;


  cout << "\n\n\t\t Testing for ddpCarrier_type complete." << endl;

  dopingProfile.free();
  testPoisson.free();
  testElectrons.free();
  testHoles.free();
  trueSolutionElectrons.free();
  trueSolutionHoles.free();

//  grvy_timer_finalize();
//  grvy_timer_summarize();

  return 0;
}

int testInitization(ddpCarrier_type const & testElectrons, 
		    ddpCarrier_type const & testHoles)
{
	assert(testElectrons.carrierProps.Mobility == 1.0);
	assert(testHoles.carrierProps.Mobility == 1.0);
	assert(testElectrons.carrierProps.RecombinationTime == 1.0e-6);
	assert(testHoles.carrierProps.RecombinationTime == 1.0e-6);
	assert(testElectrons.carrierProps.Sign4Force == -1.0);
	assert(testHoles.carrierProps.Sign4Force == 1.0);
 	assert(testElectrons.carrierProps.Diffusivity == 1.0);
        assert(testHoles.carrierProps.Diffusivity == 1.0);

	return 0;
}	

int print2File(ddpCarrier_type const & testCarrier)
{
  ofstream prt;
  prt.open("testCarrier1.dat");
 
  ddpDenseVector_type
    UPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.uDof;
    
  ddpDenseVector_type
    QPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.qDof;

   for(int i = 0; i < UPTS.size(); i++)
   {
	prt << testCarrier.PTSDense(i) << " " 
	    << UPTS(i) << " "
	    << QPTS(i) << endl;
   }
 
 prt.close();
 return 0;
}


int print2File(ddpPoisson_type const & testPoisson)
{
  ofstream prt;
  prt.open("testPoisson.dat");
 
  ddpDenseVector_type
    ElecPTS = testPoisson.VandeMondeMatrices.globalVandeMondeMX 
	 	 * testPoisson.PoissonState.elecDof,
    POTPTS = testPoisson.VandeMondeMatrices.globalVandeMondeDG 
		 * testPoisson.PoissonState.potDof;
  
   for(int i = 0; i < ElecPTS.size(); i++)
   {
	prt << testPoisson.PTSDense(i) << " " 
	    << ElecPTS(i) << " " 
	    << POTPTS(i) << endl;
   }
 
 prt.close();
 return 0;

}


int ddpTestingMakeInputFile(void)
{
  ofstream inputfile;
  inputfile.open("ddp-test");
  inputfile << "[physical] \n \
		xLeftEndPoint = 0.0\n \
		xRightEndPoint = 1.0\n \
		timeInitial = 0.0\n \
	        timeFinal = 1.00\n \
		temperature = 300 \n \
		electronCharge = 1.6e-19\n \
		vacuumPermittivity = 8.85e-14\n \
		semiCondRelativePerm     = 11.9\n \
		BoltzmanConst = 1.3806488e-23\n\
		characteristicLength	= 1.0e-4\n \
		characteristicTime 	= 1.0e-12\n \
		\n \n \
		[electrons] \n \
		Mobility = 1.0 \n \
		ChargeSign = Negative \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[holes] \n \
		Mobility = 1.0 \n \
		ChargeSign = Positive \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[computational]\n \
		maxOrderMX = 1 \n \
		maxOrderDG = 1 \n \
		integrationOrder = 10 \n \
		numElements = 100 \n \
		PointsPerElement= 5 \n \
		numTimeStamps          = 47 \n \
		GaussLobbatoNumPoints  = 15  \n \
		GaussLegendreNumPoints = 5 \n \
		\n \n \
		[couplingStatus]\n \
		couplingToPoisson      = Off \n";
  inputfile.close();
   return 0;
}
		
			
	
