#ifndef _ddpComputeDeltaT_H_
#define _ddpComputeDeltaT_H_

#include"includes.hpp"
#include"main.hpp"
#include"carrier.hpp"

int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_MKL_type const & electricFieldPTS_MKL,
		 ddpCarrier_type const & carrier,
		 double & DeltaT
		 );

int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_MKL_type const & electricFieldPTS_MKL,
		 ddpCarrier_type const & carrier1,
		 ddpCarrier_type const & carrier2,
		 double & DeltaT
		);
int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_type const & electricFieldPTS,
		 ddpCarrier_type const & carrier,
		 double & DeltaT
		 );

int
ddpComputeDeltaT(ddpGrid_type const & grid,
		 ddpDenseVector_type const & electricFieldPTS,
		 ddpCarrier_type const & carrier1,
		 ddpCarrier_type const & carrier2,
		 double & DeltaT
		);
#endif
