// includes.hpp 
// Contains included libraries, using directives, structures, and typedefs
//
///////////////////////////////////////////////////////////////////////////////


#ifndef _INCLUDES_H_
#define _INCLUDES_H_
///////////////////////////////////////////////////////////////////////////////
// INCLUDES
///////////////////////////////////////////////////////////////////////////////

//#define NDEBUG

#include<iostream>
#include<fstream>

// Import functions from the C++ STL.
#include<map>
#include<vector>
#define _USE_MATH_DEFINES
#include<math.h>
#include<limits>
#include<assert.h>
#include<cstdlib>
#include<string>
#include<sstream>
#include<algorithm>

#include<grvy.h>
#include<sys/time.h>
#include<time.h>
//#include<hdf5.h>


// disable annoying warnings from the intel compiler
//#pragma warning disable 869 383 981 2196 279 2536

// Import functions from the Intel MKL.
#include<mkl.h>
#include<mkl_spblas.h>
#include<mkl_rci.h>
#include<mkl_service.h>
#include<mkl_blas.h>
#include<mkl_types.h>
#include<mkl_dss.h>

// Import functions from Gnu scientific library.
#include<gsl/gsl_integration.h>
#include<gsl/gsl_sf_legendre.h>
#include<gsl/gsl_sf_hyperg.h>
#include<gsl/gsl_math.h>

extern "C" {
#include <umfpack.h>
#include <amd.h>
#include <SuiteSparse_config.h>
}


#include "quadrule.hpp"

// Import Sparse Eigen numerical linear algebra library.
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseCore>
#include <Eigen/UmfPackSupport>
#include <unsupported/Eigen/SparseExtra>


#include<omp.h>


///////////////////////////////////////////////////////////////////////////////
// USING FROM THE STL
///////////////////////////////////////////////////////////////////////////////
//use what we need from the standard namespace
using
std::cout;
using
std::cin;
using
std::endl;
using
std::ofstream;
using
std::ifstream;

///////////////////////////////////////////////////////////////////////////////
//  TYPEDEFS
//////////////////////////////////////////////////////////////////////////////


// Matrix and Vector type
typedef Eigen::SparseMatrix<double> ddpSparseMatrix_type;
typedef Eigen::DiagonalMatrix<double,Eigen::Dynamic> ddpDiagonalMatrix_type;
typedef Eigen::VectorXd ddpDenseVector_type;
typedef Eigen::SparseVector<double> ddpSparseVector_type;

typedef struct ddpCoo_MKL_type
{
  MKL_INT numRows;
  MKL_INT numCols;
  double * values;
  MKL_INT * rows;
  MKL_INT * columns;
  MKL_INT nnz;
} ddpCoo_MKL_type;

typedef struct ddpCsr_MKL_type
{
  MKL_INT numRows;
  MKL_INT numCols;
  double * values;
  MKL_INT * J;
  MKL_INT * I;
  MKL_INT nnz;
} ddpCsr_MKL_type;

typedef double * ddpDenseVector_MKL_type;


// Number Of Derivatives
typedef enum{zero,one} ddpNumDeriv_type;

//TODO: Inconsistency between MX and Mixed, fix this
typedef enum{DG, Mixed, Points} ddpBijFlag_type;

// Direction type
typedef enum{Plus, Minus} ddpDirection_type;

// Charge Sign type
typedef enum{Positive, Negative} ddpChargeSign_type;

// Map to allow chargeSign type to be read in and mapped from string to enum
typedef std::map<std::string, ddpChargeSign_type> ddpChargeSignMap_type;

//Linear solver uses UMFPACK
typedef Eigen::UmfPackLU<Eigen::SparseMatrix< double > > ddpSolver_type;

// Bijections for DOF to point values
typedef std::map < std::pair <int, int >, int > ddpBijForw_type;
typedef std::map < int, std::pair <int, int > > ddpBijBack_type;

// Status of whether coupling of drift diffusion to poisson is on or off
typedef enum{On, Off} ddpElecFieldCoupling_type;

// Map to allow couplingToPoisson to be on or off from string to enum
typedef std::map<std::string, ddpElecFieldCoupling_type> ddpElecFieldCouplingMap;


//////////////////////////////////////////////////////////////////////////////
// STRUCTURES
/////////////////////////////////////////////////////////////////////////////



typedef struct ddpElement_type
{
  double Left;
  double Right;
  double Delta;
  int OrderMX;
  int OrderDG;
  int GaussLobattoNumPoints;
  double * GaussLobattoPoints;
  double * GaussLobattoWeights;
  int GaussLegendreNumPoints;
  double * GaussLegendrePoints;
  double * GaussLegendreWeights;
} ddpElement_type;

typedef struct ddpDomain_type
{
  double LeftEndPoint;
  double RightEndPoint;
} ddpDomain_type;

typedef struct ddpGrid_type
{
  double DeltaxMin;
  int OrderDGMax;
  ddpDomain_type Domain;
  int NumElementsNoGhost;
  int NumElementsWithGhost;
  ddpElement_type * ElementList;
  
} ddpGrid_type;

typedef struct ddpProblemInfo_type
{
  int MaxOrderDG;
  int MaxOrderMX;
  double TimeInitial;
  double TimeFinal;
  int GaussLobattoNumPoints;
  int GaussLegendreNumPoints;
  int NumFrames;
  double temperature;
  double electronCharge;
  double vacuumPermittivity;
  double semiCondRelativePerm;
  double BoltzmannConst;
  double thermalVoltage;
  double characteristicLength;
  double characteristicTime;
  double IntrinsicDensity;
  double RadRecomboConst;
  ddpElecFieldCoupling_type ElecFieldCouplingStatus;

} ddpProblemInfo_type;


//Defines the basis functions
typedef struct ddpBasisFunction_type
{
  int element;
  int order;
  enum {psi, upsilon} family;
} ddpBasisFunction_type;


// Flux Matrices
typedef struct ddpDGFluxMatrices_type
{
  // regular versions
  ddpSparseMatrix_type plusplus;
  ddpSparseMatrix_type plusminus;
  ddpSparseMatrix_type minusplus;
  ddpSparseMatrix_type minusminus;
  // transposed versions    
  ddpSparseMatrix_type plusplusTransposed;
  ddpSparseMatrix_type plusminusTransposed;
  ddpSparseMatrix_type minusplusTransposed;
  ddpSparseMatrix_type minusminusTransposed;

  // regular versions MKL
  ddpCsr_MKL_type plusplus_MKL;
  ddpCsr_MKL_type plusminus_MKL;
  ddpCsr_MKL_type minusplus_MKL;
  ddpCsr_MKL_type minusminus_MKL;
} ddpDGFluxMatrices_type;

typedef struct ddpBijection_type
{
  ddpBijForw_type DGForward;
  ddpBijBack_type DGBackwrd;
  ddpBijForw_type MXForward;
  ddpBijBack_type MXBackwrd;
  ddpBijForw_type PTForward;
  ddpBijBack_type PTBackwrd;
} ddpBijection_type;

typedef struct ddpVandeMondeMatrices_type
{
  // regular versions
  ddpSparseMatrix_type globalVandeMondeDG;
  ddpSparseMatrix_type globalVandeMondeDGPrime;
  ddpSparseMatrix_type globalVandeMondeMX;
  ddpSparseMatrix_type globalVandeMondeMXPrime;
  ddpSparseMatrix_type globalVandeMondeFluxMX;

  // transposed Versions
  ddpSparseMatrix_type globalVandeMondeDGTransposed;
  ddpSparseMatrix_type globalVandeMondeDGPrimeTransposed;

  // regular versions MKL
  ddpCsr_MKL_type globalVandeMondeDG_MKL;
  ddpCsr_MKL_type globalVandeMondeDGPrime_MKL;
  ddpCsr_MKL_type globalVandeMondeMX_MKL;
  ddpCsr_MKL_type globalVandeMondeMXPrime_MKL;
  ddpCsr_MKL_type globalVandeMondeFluxMX_MKL;

  


} ddpVandeMondeMatrices_type;

typedef struct ddpCarrierProperties_type
{
  ddpSparseMatrix_type MassU;
  ddpSparseMatrix_type StiffUFromQ;
  ddpSparseMatrix_type FluxRightUFromQ;
  ddpSparseMatrix_type FluxLeftUFromQ;
  ddpSparseMatrix_type TotalUFromQRHS;

  ddpSparseMatrix_type MassQ;
  ddpSparseMatrix_type StiffQFromU;
  ddpSparseMatrix_type FluxRightQFromU;
  ddpSparseMatrix_type FluxLeftQFromU;
  ddpSparseMatrix_type TotalQFromBCRHS;
  ddpSparseMatrix_type TotalQFromURHS;

  ddpVandeMondeMatrices_type VandeMondeMatrices;
  ddpDGFluxMatrices_type DGFluxMatrices;
  
  //MKL Types
  ddpCsr_MKL_type MassU_MKL;
  ddpCsr_MKL_type TotalUFromQRHS_MKL;

  ddpCsr_MKL_type MassQ_MKL;
  ddpCsr_MKL_type TotalQFromBCRHS_MKL;
  ddpCsr_MKL_type TotalQFromURHS_MKL;
  
  ddpDenseVector_MKL_type leftBCVector_MKL;
  ddpDenseVector_MKL_type rightBCVector_MKL;


  // MKL Constants
  MKL_INT numDGDof_MKL;
  MKL_INT numMXDof_MKL;
  MKL_INT numTotalDof_MKL;
  MKL_INT increment_MKL;
  MKL_INT numPoints_MKL;
  MKL_INT numEndPoints_MKL;
  MKL_INT numEndPointsNoEnd_MKL;
  
 
  
  
  double Diffusivity;
  double Mobility;
  ddpChargeSign_type ChargeSign;
  double Sign4Force;
  double Sign4Poisson;
  double RecombinationTime;
  double RadRecomboConst;
  double IntrinsicDensity; 

} ddpCarrierProperties_type;

typedef struct ddpCarrierState_type
{

  // Dofs
  ddpDenseVector_type uDof;
  ddpDenseVector_type qDof;
  ddpDenseVector_type uDotDof;
  
  // Dofs MKL
  ddpDenseVector_MKL_type uDof_MKL;
  ddpDenseVector_MKL_type qDof_MKL;
  ddpDenseVector_MKL_type uDotDof_MKL;

  
  // Work vectors for makeDot
  ddpDenseVector_type BCInput;
  ddpDenseVector_type QRHS;
  ddpDenseVector_type RHSFromQ;
  ddpDenseVector_type RHSFromStiff;
  ddpDenseVector_type RHSFromFluxRightPOS;
  ddpDenseVector_type RHSFromFluxRightNEG;
  ddpDenseVector_type RHSFromFluxLeftPOS;
  ddpDenseVector_type RHSFromFluxLeftNEG;
  ddpDenseVector_type RHSFromBCLeft;
  ddpDenseVector_type RHSFromBCRight;
  ddpDenseVector_type RHSPartial;
  ddpDenseVector_type RHSTotal;
  ddpDenseVector_type RHSFromRecombination;

  // Work vectors for makeDot MKL
  ddpDenseVector_MKL_type BCInput_MKL;
  ddpDenseVector_MKL_type QRHS_MKL;
  ddpDenseVector_MKL_type RHSFromQ_MKL;
  ddpDenseVector_MKL_type RHSFromStiff_MKL;
  ddpDenseVector_MKL_type RHSFromFluxRightPOS_MKL;
  ddpDenseVector_MKL_type RHSFromFluxRightNEG_MKL;
  ddpDenseVector_MKL_type RHSFromFluxLeftPOS_MKL;
  ddpDenseVector_MKL_type RHSFromFluxLeftNEG_MKL;
  ddpDenseVector_MKL_type RHSFromBCLeft_MKL;
  ddpDenseVector_MKL_type RHSFromBCRight_MKL;
  ddpDenseVector_MKL_type RHSPartial_MKL;
  ddpDenseVector_MKL_type RHSTotal_MKL;
  ddpDenseVector_MKL_type RHSFromRecombination_MKL;
 

  ddpDenseVector_type ElecPTS;
  ddpDenseVector_type ElecEndPoints;
  ddpDenseVector_type PosElecEndPoints;
  ddpDenseVector_type NegElecEndPoints;
  ddpDenseVector_type PosElecEndPTSNoEnd;
  ddpDenseVector_type NegElecEndPTSNoEnd;
  ddpDenseVector_type ElecPTSTimesWeights;
  
   
  ddpDenseVector_MKL_type ElecPTS_MKL;
  ddpDenseVector_MKL_type ElecEndPoints_MKL;
  ddpDenseVector_MKL_type PosElecEndPoints_MKL;
  ddpDenseVector_MKL_type NegElecEndPoints_MKL;
  ddpDenseVector_MKL_type PosElecEndPTSNoEnd_MKL;
  ddpDenseVector_MKL_type NegElecEndPTSNoEnd_MKL;
  ddpDenseVector_MKL_type ElecPTSTimesWeights_MKL;
 
  // Work Matrices
  ddpDiagonalMatrix_type
	ElecTimesWeightsMatrix;
  ddpDiagonalMatrix_type
	PosElecEndPTSNoEndMatrix;
  ddpDiagonalMatrix_type
	NegElecEndPTSNoEndMatrix;

  // Work Vectors
  ddpDenseVector_MKL_type workSpaceVector0_MKL;
  ddpDenseVector_MKL_type workSpaceVector1_MKL;



  // solvers
  ddpSolver_type solverU;
  ddpSolver_type solverQ;
  

} ddpCarrierState_type;



typedef struct ddpCarrierConstants_type
{
   // Electron constants
   double electron_Mobility;
   ddpChargeSign_type electron_ChargeSign;
   double electron_RecombinationTime;
   
   // hole constants
   double hole_Mobility;
   ddpChargeSign_type hole_ChargeSign;
   double hole_RecombinationTime;

} ddpCarrierConstants_type;



typedef struct ddpPoissonProperties_type
{

  // Matrices
  ddpSparseMatrix_type ABig;
  ddpSparseMatrix_type A00;
  ddpSparseMatrix_type A01;
  ddpSparseMatrix_type A10;
  ddpSparseMatrix_type C;
  ddpSparseMatrix_type VBig;

  ddpSparseMatrix_type VRHS;
  ddpSparseMatrix_type CRHS;

  ddpVandeMondeMatrices_type VandeMondeMatrices;
  ddpDGFluxMatrices_type DGFluxMatrices;

  // MKL MATRICES
  ddpCsr_MKL_type ABig_MKL;
  ddpCsr_MKL_type VRHS_MKL;
  ddpCsr_MKL_type CRHS_MKL;


  MKL_INT numDGDof_MKL;
  MKL_INT numMXDof_MKL;
  MKL_INT numTotalDof_MKL;
  MKL_INT increment_MKL;

 // Vectors


  // Constants
  double Lambda;
  double coupledOrNot;

} ddpPoissonProp_type;

typedef struct ddpPoissonState_type
{

// Data Members
  
  // Dofs 
  ddpDenseVector_type elecDof;
  ddpDenseVector_type potDof;

  // work vectors
  ddpDenseVector_type carrierDof;
  ddpDenseVector_type BCInput;

  ddpDenseVector_type RHSFromBC;
  ddpDenseVector_type RHSFromCandU;
	
  ddpDenseVector_type RHSTop;
  ddpDenseVector_type RHSBottom;
  ddpDenseVector_type RHSTotal;
  ddpDenseVector_type Soln;


  // MKL DOFS
  ddpDenseVector_MKL_type elecDof_MKL;
  ddpDenseVector_MKL_type potDof_MKL;
  
  // MKL Worker vectors
  ddpDenseVector_MKL_type carrierDof_MKL;
  ddpDenseVector_MKL_type BCInput_MKL;
  
  ddpDenseVector_MKL_type RHSFromBC_MKL;
  ddpDenseVector_MKL_type RHSFromCandU_MKL;
  ddpDenseVector_MKL_type RHSTotal_MKL;
  ddpDenseVector_MKL_type Soln_Old_MKL;
  ddpDenseVector_MKL_type Soln_MKL;



  ddpSolver_type solverABig;
  _MKL_DSS_HANDLE_t solverHandle;

} ddpPoissonState_type;

#endif

