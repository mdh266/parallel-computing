/* Main.cpp is a code for solving the Drift Diffusion Poisson Equations
 *  Copyright 2013 Moderate Perfomance Computing Company
 *  
 *
 * "Computing at the full speed of Freedom!"
 *
 */
#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"


bool Dofs_Are_Same_MKL(ddpCarrier_type const & carrier1);

int 
main()
{
  Eigen::setNbThreads(1);
  grvy_timer_init("GRVY TIMING");
  grvy_timer_reset();
  grvy_timer_begin("Main");

  /////////////////////////////////////////////////////////////////////////////
  // Set Up
  /////////////////////////////////////////////////////////////////////////////

  // Read in inputs about domain, grid and problem.
  ddpDomain_type domain;
  ddpGrid_type grid;
  ddpProblemInfo_type problem;
  ddpCarrierConstants_type carrierConstants;
 
  readInput(domain, grid, problem, carrierConstants, "ddp-input"); 

  // Make the grid
  ddpMakeUniformGrid(problem, domain, grid);

  //Prepare time stamps
  std::vector<double> timeStamps;
  ddpMakeTimeStamps(problem, timeStamps);

  // Make some electrons and holes
  ddpCarrier_type electrons,
		  holes;

  // Initialize all matrices and physical constants
  electrons.initialize(grid, problem, carrierConstants, "electrons");
  holes.initialize(grid, problem, carrierConstants, "holes");
  
  // Set Dirchlet Boundary Conditions
  electrons.setDirichletBoundaryConditions(&ddpElectronDirichletLeft,
				           &ddpElectronDirichletRight);

  holes.setDirichletBoundaryConditions(&ddpHoleDirichletLeft,
				       &ddpHoleDirichletRight);

  // Set the initial conditions of each carrier
  electrons.setInitialConditions(grid, &ddpElectronInitialConditions);
  holes.setInitialConditions(grid, &ddpHoleInitialConditions);


  // Make the electric field and potential
  ddpPoisson_type Poisson;

  // Initialize all matrices and physical constants
  Poisson.initialize(grid, problem, carrierConstants);
  
  // Set the boundary conditions
  Poisson.setDirichletBoundaryConditions(&ddpPotentialDirichletLeft,
				         &ddpPotentialDirichletRight);

  // Make the doping profile
  ddpDopingProfile_type  DopingProfile; 

/*  DopingProfile.setDopingProfile(grid, electrons,
    &NPlus_N_NPlus);
*/

  // Project the Doping profile onto the DG basis functions
  DopingProfile.setDopingProfile(grid, 
				 electrons,
				 holes,
				 &ddpDonorDopingProfile,
				 &ddpAcceptorDopingProfile
				 );
 
  // Make the dofs to be passed back and forth
  ddpDenseVector_type ElecFieldDof = Poisson.PoissonState.elecDof;
  ddpDenseVector_MKL_type ElecFieldDof_MKL = Poisson.PoissonState.elecDof_MKL; 
 

  for(int i = 0; i < Poisson.PoissonState.elecDof.size(); i++)
  {
	ElecFieldDof_MKL[i] = 0.0;
  }

  ddpDenseVector_type EPTS; // used for computing time step


  /////////////////////////////////////////////////////////////////////////////
  // Time Stepping
  /////////////////////////////////////////////////////////////////////////////
     
  grvy_timer_begin("Time Stepping");

  cout << "Entering time stepping." << endl;

     double tCurrent = timeStamps.at(0);
     double tNext;
     double tDeltaProposed;
     double tDelta;
    
     // get initial configuration for potential
     // Poisson.solveSystem(electrons, DoppingDof, tCurrent, problem);
     Poisson.solveSystem_MKL(electrons,
			     holes,
			     DopingProfile,
			     tCurrent,
			     problem);

     Poisson.solveSystem(electrons, 
			 holes, 
			 DopingProfile,
			 tCurrent, 
			 problem);
     

     // get the inital configuration for the electrons
     ElecFieldDof = Poisson.PoissonState.elecDof;
     ElecFieldDof_MKL = Poisson.PoissonState.elecDof_MKL;

/*    cout << "Eigen \n" << Poisson.PoissonState.elecDof << endl;
    cout << "MKL \n" << endl;	
     for(int i = 0;i <  Poisson.PoissonState.elecDof.size(); i++)
     {
	cout << Poisson.PoissonState.elecDof_MKL[i] << endl;
     }
*/
//     electrons.isSame(ElecFieldDof, ElecFieldDof_MKL);
     
//     electrons.updateRecombination(holes);
//     holes.updateRecombination(electrons);

     // MKL VERSION
     electrons.makeDot_MKL(ElecFieldDof_MKL, tCurrent);
     holes.makeDot_MKL(ElecFieldDof_MKL, tCurrent);

//     // EIGEN
      electrons.makeDot(ElecFieldDof, tCurrent);
      holes.makeDot(ElecFieldDof, tCurrent);
	

//            if(!Dofs_Are_Same_MKL(electrons))
//		assert(false);

//            if(!Dofs_Are_Same_MKL(holes))
//		assert(false);

     unsigned int timeStampLabel = 0;
   
     // Print initial State     
     ddpPrintState(electrons,
		   holes,
		   Poisson,
		   timeStampLabel);
     
     // Do large grain time stepping to compute the compute the next "printable"
     // state
     for(timeStampLabel = 1;
	 timeStampLabel < timeStamps.size();
	 ++timeStampLabel)
       {
	 tNext = timeStamps.at(timeStampLabel);

	 // Do fine grain time stepping to reach the next time stamp
	 while(tCurrent < tNext)
	   {
	     
	     Poisson.getElecFieldVals(EPTS);
	     
	     // TODO: This should take into be rewritten so that it uses 
	     // electronCarriers + Potential Carriers and EPTS is made in below
	     // Compute DeltaT Proposed
	     ddpComputeDeltaT(grid,
			      EPTS,
			      electrons,
			      holes,
			      tDeltaProposed
			      );
	     
	     
	     // Adjust deltaT so that its gives a timestep lower than the next
	     // timestamp if necessary
	     // TODO:  We could write this without using an "if" statement if 
	     // we wanted to do so.
	     if(tCurrent + tDeltaProposed > tNext)
     	       {
     		 tDelta = tNext - tCurrent;
     	       }
     	     else
     	       {
     		 tDelta = tDeltaProposed;
     	       }
	     
	     // Get Current state of electrons and solve for Poisson
	     // Poisson.solveSystem(electrons, DoppingDof, tCurrent, problem);
	     Poisson.solveSystem_MKL(electrons,
				     holes,
				     DopingProfile,
				     tCurrent,
				     problem);

	     Poisson.solveSystem(electrons,
				 holes,
				 DopingProfile,
				 tCurrent,
				 problem);
	     

	     // Get current state of Poisson and solve for electrons
	     ElecFieldDof = Poisson.PoissonState.elecDof;
	     ElecFieldDof_MKL = Poisson.PoissonState.elecDof_MKL;

	     
	    // EIGEN
	     electrons.makeDot(ElecFieldDof, tCurrent);
	     holes.makeDot(ElecFieldDof, tCurrent);
	
	    // MKL VERSION
	     electrons.makeDot_MKL(ElecFieldDof_MKL, tCurrent);
	     holes.makeDot_MKL(ElecFieldDof_MKL, tCurrent);

//            if(!Dofs_Are_Same_MKL(electrons))
//		assert(false);

 //           if(!Dofs_Are_Same_MKL(holes))
//		assert(false);

	     //////////////////////////////////////////////////////////////////
	     // Forward Euler
	     //////////////////////////////////////////////////////////////////
	    
	     electrons.forwardEuler(tDelta);
	     holes.forwardEuler(tDelta);

	     electrons.forwardEuler_MKL(tDelta);
	     holes.forwardEuler_MKL(tDelta);

	   
	     tCurrent += tDelta;
	   }
 
	 grvy_timer_begin("Print State");

	 ddpPrintState(electrons,
		       holes,
		       Poisson,
		       timeStampLabel);
	 
	 grvy_timer_end("Print State");
	 
       }
/*      if(Dofs_Are_Same_MKL(electrons))
      {
		cout << "Success on Electrons! " << endl;
      }
      if(Dofs_Are_Same_MKL(holes))
      {
		cout << "Success on Holes! " << endl;
      }
*/
     DopingProfile.free();
     Poisson.free();
     electrons.free();
     holes.free(); 

    
     cout << "\tExiting Time Stepping" << endl;
     grvy_timer_end("Time Stepping");
     grvy_timer_end("Main");
     grvy_timer_finalize();
     grvy_timer_summarize();

  return 0;
}


bool Dofs_Are_Same_MKL(ddpCarrier_type const & carrier1) 
{
 	double tol = 1.0e-4;
	
	for(int i = 0; i < carrier1.carrierState.uDof.size(); i++)
	{
		if( (abs((carrier1.carrierState.uDotDof_MKL[i]-carrier1.carrierState.uDotDof(i))
		  / carrier1.carrierState.uDotDof(i))) > tol)
		{
			cout << "uDotDofs not same at entry " << i << endl;
			cout << "MKL = " << carrier1.carrierState.uDotDof_MKL[i] << endl;
			cout << "Eigen = " << carrier1.carrierState.uDotDof(i) << endl;
			return false;
		}
/*	
		if(abs(carrier1.carrierState.qDof_MKL[i] - carrier1.carrierState.qDof(i)) > tol)
		{
			cout << "qDofs not same at entry " << i << endl;
			cout << "MKL = " << carrier1.carrierState.qDof_MKL[i] << endl;
			cout << "Eigen = " << carrier1.carrierState.qDof(i) << endl;
			return false;
		}
*/		
	}

	return true;
}

