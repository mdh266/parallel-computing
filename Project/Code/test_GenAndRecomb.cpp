#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"

int ddpTestingMakeInputFile(void);
int print2File(ddpCarrier_type const & testCarrier);
int print2File(ddpPoisson_type const & testPoisson);
int testInitization(ddpCarrier_type const & testElectrons, ddpCarrier_type const & testHoles);


double ZeroFunction(double const & x)
{
	return 0.0;
}

double Const_BC_Zero(double const & t)
{
	return 0;
}

double OneFunction(double const & x)
{
	if( x > M_PI/2.0)
		return 1.0;
	else
		return 0.0;
}

double SineFunction(double const & x)
{
	return sin(x);
}

double DiffusionSolutionElectronsU(double const & x)
{
	// solution to the diffusion equation after T = 1.0 
	if( x > M_PI/2.0)
		return  exp(-1.0)*sin(x);
	else
		return  sin(x);
}

double DiffusionSolutionElectronsQ(double const & x)
{
	// solution to the diffusion equation after T = 1.0 
	if( x > M_PI/2.0)
		return  exp(-1.0)*cos(x);
	else
		return  cos(x);
}

bool Dofs_Are_Same(ddpCarrier_type const & carrier1, 
					ddpCarrier_type const & carrier2)
{
 	double tol = 1.0e-4;

	if(carrier1.carrierState.uDof.size() != carrier2.carrierState.uDof.size())
	{
		cout << "uDofs size not same" << endl;
		return false;
	}
		
	if(carrier1.carrierState.qDof.size() != carrier2.carrierState.qDof.size())
	{
		cout << "qDofs size not same" << endl;
		return false;
	}

	for(int i = 0; i < carrier1.carrierState.uDof.size(); i++)
	{
		if(abs(carrier1.carrierState.uDof(i)-carrier2.carrierState.uDof(i)) > tol)
		{
			cout << "uDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.uDof(i) << endl;
			cout << "2 = " << carrier2.carrierState.uDof(i) << endl;
			return false;
		}
	
		if(abs(carrier1.carrierState.qDof(i) - carrier2.carrierState.qDof(i)) > tol)
		{
			cout << "qDofs not same at entry " << i << endl;
			cout << "1 = " << carrier1.carrierState.qDof(i) << endl;
			cout << "2 = " << carrier2.carrierState.qDof(i) << endl;
			return false;
		}
		
	}

	return true;
}


int main()
{
  // Initial set up stuff
  ddpTestingMakeInputFile();  
  
  ddpDomain_type testdomain;
  ddpGrid_type testgrid;
  ddpProblemInfo_type testproblem;
  ddpCarrierConstants_type testConstants;

  readInput(testdomain, testgrid, testproblem, testConstants, "ddp-test");
  
  //Changing end points so that I can come up with manufactored solutions
  testdomain.LeftEndPoint = 0.0;
  testdomain.RightEndPoint = M_PI;


  ddpMakeUniformGrid(testproblem, testdomain, testgrid); 
  ddpPrintGrid(testgrid);
  
  ddpPoisson_type testPoisson;
  ddpCarrier_type testElectrons;
  ddpCarrier_type testHoles;
  ddpCarrier_type trueSolutionElectrons;
  ddpCarrier_type trueSolutionHoles;

  testElectrons.initialize(testgrid, testproblem, testConstants, "electrons");
  testHoles.initialize(testgrid, testproblem, testConstants, "holes");
  trueSolutionElectrons.initialize(testgrid, testproblem, testConstants, "electrons");
  trueSolutionHoles.initialize(testgrid, testproblem, testConstants, "holes");
  ddpDopingProfile_type dopingProfile;

  

  testPoisson.initialize(testgrid, testproblem, testConstants); 
 
  // set Diffusivity = 1.0 in both carriers
  cout << "\n\n\t\tSetting Diffusivity = 0.0 and remaking carrierProps." << endl;  
  testElectrons.carrierProps.Diffusivity = 0.0;
  testHoles.carrierProps.Diffusivity = 0.0;
  testElectrons.remakeProperties(testgrid,testproblem);
  testHoles.remakeProperties(testgrid,testproblem);
 
  cout << "\n\n1.) Testing Initialization " << endl;
  testInitization(testElectrons, testHoles);
  cout << "\n PASSED!\n\n" << endl;


  // Things to use to project funtion onto a basis
  ddpBijForw_type DGForward = testPoisson.Bijections.DGForward;
  ddpBijForw_type MXForward = testPoisson.Bijections.MXForward;

  ddpSparseMatrix_type globalVandeMondeDG = 
			testPoisson.VandeMondeMatrices.globalVandeMondeDG;
  
  ddpSparseMatrix_type globalVandeMondeMX = 
			testPoisson.VandeMondeMatrices.globalVandeMondeMX;

  ddpBijForw_type PTForward = testPoisson.Bijections.PTForward;

  ddpSparseVector_type sparseWeights = testPoisson.weightsSparse;

  ddpSparseMatrix_type MassU = testElectrons.carrierProps.MassU;
  ddpSparseMatrix_type A00 = testPoisson.PoissonProps.A00;

  ddpBijFlag_type BIJFlagDG = DG; 
  ddpBijFlag_type BIJFlagMX = Mixed; 


 
  // sets lambdda = 1
  testPoisson.PoissonProps.Lambda = 1.0;  
  
  cout << "Testing:  dn/dt = -pn" << 
   	"\t With n(0,t) = 0, n(pi) = 0,\t n(x,0) = sin(x),  p(x,t) = H(x-1/2)"
       << endl << endl;

///////////////////////////////////////////////////////////////////////////////
// RECOMBINATION WITH ELECTRONS
///////////////////////////////////////////////////////////////////////////////
	
 
   
  // set n(x,t) = sin(x)
  testElectrons.setInitialConditions(testgrid, &SineFunction);
 

  // set p(x,t) = 1
  testHoles.setInitialConditions(testgrid,&OneFunction);
   
  // set n(0) = 0, n(pi) = 0
  testElectrons.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				       &Const_BC_Zero);
  // set p(0) = 1, p(pi) = 1
  testHoles.setDirichletBoundaryConditions(&OneFunction,
	  				   &OneFunction);
  
  // set Phi(0) = 0, Phi(pi) = 0
  testPoisson.setDirichletBoundaryConditions(&Const_BC_Zero,
	  				     &Const_BC_Zero);

  // Now set C(x) = 0
  dopingProfile.setDopingProfile(testgrid, testElectrons, &ZeroFunction);
 
  
  // Solve for Phi, time = 0.0.
  testPoisson.solveSystem(testElectrons, dopingProfile, 0.0, testproblem);
 
 
  //   Construct TRUE solution
  ddpProjectFunction(&DiffusionSolutionElectronsU,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.uDof);

  ddpProjectFunction(&DiffusionSolutionElectronsQ,
		    testgrid, 
		    DGForward,
		    globalVandeMondeDG,
		    PTForward,
		    BIJFlagDG,
		    sparseWeights,
		    MassU,
		    trueSolutionElectrons.carrierState.qDof);

  print2File(testPoisson);
  ddpDenseVector_type EPTS;
  ddpDenseVector_type ElecFieldDof;
  ElecFieldDof = testPoisson.PoissonState.elecDof;


  testElectrons.updateRecombination(testHoles);
  testElectrons.makeDot(ElecFieldDof, 0.0);
  testHoles.makeDot(ElecFieldDof, 0.0);

  
  print2File(trueSolutionElectrons);
  print2File(testElectrons);
  //print2File(testHoles); 
  testPoisson.getElecFieldVals(EPTS);

  double Tend = 1.0;
  double tCurrent = 0.0;
  double DeltaT = 0.01;
  int timeStamp = 0;

  while(tCurrent < Tend)
  {
         testPoisson.getElecFieldVals(EPTS);
         ElecFieldDof = testPoisson.PoissonState.elecDof;
	
	 testElectrons.updateRecombination(testHoles);
	 testElectrons.makeDot(ElecFieldDof, tCurrent);

	 testElectrons.forwardEuler(DeltaT);
	 ddpPrintState(testElectrons,
		       testHoles, 
		       testPoisson,
		       timeStamp);

	 tCurrent += DeltaT;
	 timeStamp++;
  }

  print2File(trueSolutionElectrons);
  print2File(testElectrons);

  if(Dofs_Are_Same(testElectrons, trueSolutionElectrons))
	cout << " PASSED!" << endl;
  else
	cout << " FAILED!" << endl;
  
  return 0;
}

int testInitization(ddpCarrier_type const & testElectrons, 
		    ddpCarrier_type const & testHoles)
{
	assert(testElectrons.carrierProps.Mobility == 1.0);
	assert(testHoles.carrierProps.Mobility == 1.0);
	assert(testElectrons.carrierProps.RecombinationTime == 1.0e-6);
	assert(testHoles.carrierProps.RecombinationTime == 1.0e-6);
	assert(testElectrons.carrierProps.Sign4Force == -1.0);
	assert(testHoles.carrierProps.Sign4Force == 1.0);
 	assert(testElectrons.carrierProps.Diffusivity == 0.0);
        assert(testHoles.carrierProps.Diffusivity == 0.0);
        assert(testElectrons.carrierProps.IntrinsicDensity == 0.0);
        assert(testHoles.carrierProps.IntrinsicDensity == 0.0);
        assert(testElectrons.carrierProps.RadRecomboConst == 1.0);
        assert(testHoles.carrierProps.RadRecomboConst == 1.0);
	
	return 0;
}	

int print2File(ddpCarrier_type const & testCarrier)
{
  ofstream prt;
  prt.open("testCarrier1.dat");
 
  ddpDenseVector_type
    UPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.uDof;
    
  ddpDenseVector_type
    QPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.qDof;
    
  ddpDenseVector_type
    RECOMBOPTS = testCarrier.VandeMondeMatrices.globalVandeMondeDG 
		* testCarrier.carrierState.RHSFromRecombination;
		
   for(int i = 0; i < UPTS.size(); i++)
   {
	prt << testCarrier.PTSDense(i) << " " 
	    << UPTS(i) << " "
	    << QPTS(i) << " " 
	    << RECOMBOPTS(i) << endl;
   }
 
 prt.close();
 return 0;
}


int print2File(ddpPoisson_type const & testPoisson)
{
  ofstream prt;
  prt.open("testPoisson.dat");
 
  ddpDenseVector_type
    ElecPTS = testPoisson.VandeMondeMatrices.globalVandeMondeMX 
	 	 * testPoisson.PoissonState.elecDof,
    POTPTS = testPoisson.VandeMondeMatrices.globalVandeMondeDG 
		 * testPoisson.PoissonState.potDof;
  
   for(int i = 0; i < ElecPTS.size(); i++)
   {
	prt << testPoisson.PTSDense(i) << " " 
	    << ElecPTS(i) << " " 
	    << POTPTS(i) << endl;
   }
 
 prt.close();
 return 0;

}


int ddpTestingMakeInputFile(void)
{
  ofstream inputfile;
  inputfile.open("ddp-test");
  inputfile << "[physical] \n \
		xLeftEndPoint = 0.0\n \
		xRightEndPoint = 1.0\n \
		timeInitial = 0.0\n \
	        timeFinal = 1.00\n \
		temperature = 300 \n \
		electronCharge = 1.6e-19\n \
		vacuumPermittivity = 8.85e-14\n \
		semiCondRelativePerm     = 11.9\n \
		BoltzmanConst = 1.3806488e-23\n\
		characteristicLength	= 1.0e-4\n \
		characteristicTime 	= 1.0e-12\n \
		IntrinsicDensity	= 0 \n \
		RadRecomboConst		= 1 \n \
		\n \n \
		[electrons] \n \
		Mobility = 1.0 \n \
		ChargeSign = Negative \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[holes] \n \
		Mobility = 1.0 \n \
		ChargeSign = Positive \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[computational]\n \
		maxOrderMX = 1 \n \
		maxOrderDG = 1 \n \
		integrationOrder = 10 \n \
		numElements = 100 \n \
		PointsPerElement= 5 \n \
		numTimeStamps          = 100 \n \
		GaussLobbatoNumPoints  = 15  \n \
		GaussLegendreNumPoints = 5 \n \
		\n \n \
		[couplingStatus]\n \
		couplingToPoisson      = Off \n";
  inputfile.close();
   return 0;
}
		
			
	
