#ifndef _DDPPRINTSTATE_H_
#define _DDPPRINTSTATE_H_ 

#include "includes.hpp"
#include "carrier.hpp"
#include "poisson.hpp"



int
ddpPrintState(ddpCarrier_type const & carrier1,
	      ddpPoisson_type const & poisson,
	      int const & timeStamp);
int
ddpPrintState(ddpCarrier_type  & carrier1,
	      ddpCarrier_type  & carrier2,
	      ddpPoisson_type const  & poisson,
	      int const & timeStamp);


#endif
