#include "includes.hpp"
#include "main.hpp"
#include "carrier.hpp"
#include "poisson.hpp"
#include "dopingprofile.hpp"
#include "ddpPrintState.hpp"
#include "ddpComputeDeltaT.hpp"

int ddpTestingMakeInputFile(void);
int print2File(ddpDopingProfile_type const & testDoping,
	       ddpSparseMatrix_type const & VandeMonde,
	       ddpDenseVector_type const & PTS);

int print2File(ddpDenseVector_type const & testDoping,
	       ddpSparseMatrix_type const & VandeMonde,
	       ddpDenseVector_type const & PTS);


double Doping_N_A(double const & x)
{
	if(x <= 0.5e-4)
		return 1.0;
	else
		return 0.0;
}

double True_Doping_N_A(double const & x)
{
	if(x <= 0.5e-4)
		return -1.0;
	else
		return 0.0;
}

double Doping_N_D(double const & x)
{
	if(x > 0.5e-4)
		return 1.0;
	else
		return 0.0;
}

double Doping_PN(double const & x)
{
	if(x <= 0.5e-4)
		return -1.0;
	else
		return 1.0;
}



bool Dofs_Are_Same(ddpDenseVector_type const & DP1, ddpDenseVector_type const & DP2)
{
 	double tol = 1.0e-8;
	
	if(DP2.size() != DP1.size())
	{
		cout << "Dofs size not same" << endl;
		return false;
	}

	for(int i = 0; i < DP1.size(); i++)
	{
		if( abs(DP1(i)-DP2(i)) > tol )
		{
			cout << "Dofs not same at entry " << i << endl;
			return false;
		}
	}

	return true;
}


int main()
{
  // Initial set up stuff
  ddpTestingMakeInputFile();  
  
  ddpDomain_type testdomain;
  ddpGrid_type testgrid;
  ddpProblemInfo_type testproblem;
  ddpCarrierConstants_type testConstants;

  readInput(testdomain, testgrid, testproblem, testConstants, "ddp-test");
  ddpMakeUniformGrid(testproblem, testdomain, testgrid); 
  
  ddpCarrier_type testElectrons, testHoles;
  testElectrons.initialize(testgrid, testproblem, testConstants,"electrons");
  testHoles.initialize(testgrid, testproblem, testConstants,"holes");
  
  ddpDopingProfile_type DopingProfile;
  

  // Things to use to project funtin onto a basis
  ddpBijForw_type DGForward = testElectrons.Bijections.DGForward;

  ddpSparseMatrix_type globalVandeMondeDG = 
			testElectrons.VandeMondeMatrices.globalVandeMondeDG;
  

  ddpBijForw_type PTForward = testElectrons.Bijections.PTForward;

  ddpSparseVector_type sparseWeights = testElectrons.weightsSparse;

  ddpSparseMatrix_type MassU = testElectrons.carrierProps.MassU;

  ddpBijFlag_type BIJFlagDG = DG; 
  
  ddpDenseVector_type PTS = testElectrons.PTSDense;
  
  ddpDenseVector_type testDopingDof;
  ddpDenseVector_type trueDopingDof;


  cout << "\n \n\t\tTesting ddpDopingProfile_type " << endl;

  cout << "\n \n 1.) Testing: C(x) = N_D(x)" << endl << endl;

  DopingProfile.setDopingProfile(testgrid,testElectrons,&Doping_N_D);

  // get testing C(x) = N_D(x) dof
  testDopingDof = DopingProfile.Dof;
  
  // Get dof for N_D(x)
  ddpProjectFunction(&Doping_N_D,
		     testgrid, 
		     DGForward,
		     globalVandeMondeDG,
		     PTForward,
		     BIJFlagDG,
		     sparseWeights,
		     MassU,
		     trueDopingDof);


  // Test if Dofs are same
  if(Dofs_Are_Same(trueDopingDof, testDopingDof))
	cout << "PASSED!" << endl << endl;
  else
 	cout << "FAILED!" << endl << endl;

//  print2File(DopingProfile, globalVandeMondeDG, PTS);
  
  cout << "\n \n 2.) Testing: C(x) = -N_A(x)" << endl << endl;

  DopingProfile.setDopingProfile(testgrid,testHoles,&Doping_N_A);

  // get testing C(x) = N_A(x) dof
  testDopingDof = DopingProfile.Dof;
  
  // Get dof for N_A(x)
  ddpProjectFunction(&True_Doping_N_A,
		     testgrid, 
		     DGForward,
		     globalVandeMondeDG,
		     PTForward,
		     BIJFlagDG,
		     sparseWeights,
		     MassU,
		     trueDopingDof);

  // Test if Dofs are same
  if(Dofs_Are_Same(trueDopingDof, testDopingDof))
	cout << "PASSED!" << endl << endl;
  else
 	cout << "FAILED!" << endl << endl;

//  print2File(DopingProfile, globalVandeMondeDG, PTS);

 cout << "\n \n 3.) Testing: C(x) = N_D(x)-N_A(x)" << endl << endl;

  DopingProfile.setDopingProfile(testgrid,testElectrons, testHoles,
				 &Doping_N_D, &Doping_N_A);

  // get testing C(X) = N_D(x)-N(A) dof
  testDopingDof = DopingProfile.Dof;
  
  // Get dof for PN(X) = N_D - N_A
  ddpProjectFunction(&Doping_PN,
		     testgrid, 
		     DGForward,
		     globalVandeMondeDG,
		     PTForward,
		     BIJFlagDG,
		     sparseWeights,
		     MassU,
		     trueDopingDof);

  // Test if Dofs are same
  if(Dofs_Are_Same(trueDopingDof, testDopingDof))
	cout << "PASSED!" << endl << endl;
  else
 	cout << "FAILED!" << endl << endl;

  print2File(DopingProfile, globalVandeMondeDG, PTS);

  cout << "\n\n \t \tTesting for test_Doping.cpp complete" << endl;

  DopingProfile.free();
  testElectrons.free();
  testHoles.free();


  return 0;
}

int print2File(ddpDenseVector_type const & testDoping,
	       ddpSparseMatrix_type const & VandeMonde,
	       ddpDenseVector_type const & PTS)
{
  ofstream prt;
  prt.open("testDoping.dat");
 
 
  ddpDenseVector_type DopingPTS = VandeMonde*testDoping;
  if(DopingPTS.size() != PTS.size())
  {
	cout << "x values size != Doping values size" << endl;
	assert(false);
  } 
  for(int i = 0; i < PTS.size(); i++)
  {
	prt << PTS(i) << " " 
	    << DopingPTS(i) << endl;
   }
 
 prt.close();
 return 0;
}

int print2File(ddpDopingProfile_type const & testDoping,
	       ddpSparseMatrix_type const & VandeMonde,
	       ddpDenseVector_type const & PTS)
{
  ofstream prt;
  prt.open("testDoping.dat");
 
 
  ddpDenseVector_type DopingPTS = VandeMonde*testDoping.Dof;
  if(DopingPTS.size() != PTS.size())
  {
	cout << "x values size != Doping values size" << endl;
	assert(false);
  } 
  for(int i = 0; i < PTS.size(); i++)
  {
	prt << PTS(i) << " " 
	    << DopingPTS(i) << endl;
   }
 
 prt.close();
 return 0;

}




int ddpTestingMakeInputFile(void)
{
  ofstream inputfile;
  inputfile.open("ddp-test");
  inputfile << "[physical] \n \
		xLeftEndPoint = 0.0\n \
		xRightEndPoint = 1.0\n \
		timeInitial = 0.0\n \
	        timeFinal = 1.00\n \
		temperature = 300 \n \
		electronCharge = 1.6e-19\n \
		vacuumPermittivity = 8.85e-14\n \
		semiCondRelativePerm     = 11.9\n \
		BoltzmanConst = 1.3806488e-23\n\
		characteristicLength	= 1.0e-4\n \
		characteristicTime 	= 1.0e-12\n \
		\n \n \
		[electrons] \n \
		Mobility = 1500 \n \
		ChargeSign = Negative \n \
		recobinationTime = 1.0e-6 \n \
		\n \n \
		[computational]\n \
		maxOrderMX = 1 \n \
		maxOrderDG = 1 \n \
		integrationOrder = 10 \n \
		numElements = 50 \n \
		PointsPerElement= 5 \n \
		numTimeStamps          = 47 \n \
		GaussLobbatoNumPoints  = 15  \n \
		GaussLegendreNumPoints = 5 \n \
		\n \n \
		[couplingStatus]\n \
		couplingToPoisson      = On \n";
  inputfile.close();
   return 0;
}
		
			
	
