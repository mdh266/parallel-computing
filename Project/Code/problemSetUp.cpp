#include "includes.hpp"
#include "main.hpp"


//TODO: NOTICE COMMENTS DONT MATCH FOR MAKING GLOBALVANDEMONDE


double
ddpPsi( ddpGrid_type const & grid, 
	int const & elem, 
	int const & order,
	ddpNumDeriv_type const & numDerivs,
	double const & x
	)
{
  double xLeft = (grid.ElementList[elem]).Left;
  double xRight = (grid.ElementList[elem]).Right;
  double Delta = (grid.ElementList[elem]).Delta;
  double xScaled = 2.0 * (x - xLeft) / Delta - 1;
  double temp1 = 0;
  //  This is the jacobian to account for the change of variables from the 
  //  reference element to the local element.
  double jacobian;
  jacobian = 2.0 / Delta;;
  //std::cout << "xScaled = " << xScaled << std::endl;
  if(  (x > xRight) || (x < xLeft) )
    {
      assert (fabs(xScaled) >= 1.0);      
      return 0.0;
    }
  else
    {
      // we need to cast the order into a double for the gsl to use it.
      double dorder = (double) order;
      
      switch (numDerivs)
	{	
	case zero:
	  temp1 = gsl_sf_legendre_Pl(order, xScaled);
	  break;
	case one:
	  // this expression gives the derivative of the legendre polynomials
	  // Kent can explain.	 
	  // See the wolfram functions site for the identity that we use here.
	  temp1 = 0.5* dorder * (dorder + 1)
	    * gsl_sf_hyperg_2F1 ( 1.0 - dorder, 2.0 + dorder, 2.0, 
				  0.5 * (1.0 - xScaled) );

	  temp1 *= jacobian;
	  break;
	default:
	  cout << "Can't happen on line number "  << __LINE__ << endl;
	  break;
	  return 1;
	}
      return (temp1);
    }
}

double
ddpLimPsi(ddpGrid_type const & grid,
	  int const & elem, 
	  int const & order,
	  ddpDirection_type const & Flag,
	  int const & endPointNumber
	  )
{
  // Note:  These fluxes are for the legendre polynomials.  
  // If a different choice of basis function is chosen, this has to be modified
  // appropriately.

  int i = elem;
  int istar = endPointNumber;
  double temp;
  int numElementsNoGhost = grid.NumElementsNoGhost;
  if(Flag == Plus)
    {
      if( istar == i) // Then we are at the left endpoint of the i'th elemement
	{
	  temp = pow( -1.0, (double) order);
	  return temp;
	}
      else
	return 0.0;
    }
  else if(Flag == Minus)
    {
      if(i + 1 == istar )  // Then we are the right endpoint of the i'th element
	{
	  return 1.0;
	}
      else
	return 0.0;
    }
  else 
    {
      cout << "can't happen on line " << __LINE__ << endl;
      return NAN;
      assert(false);
      // TODO: some sort of break statement more appropriate?
    }
}

double
ddpUpsilon( ddpGrid_type const & grid,
	    int const & elem,
	    int const & order,
	    ddpNumDeriv_type const & numDerivs,
	    double const & x
	    )
{
  int NumElementsNoGhost = grid.NumElementsNoGhost;
  int J = (order+1) % 2;
  

  ddpNumDeriv_type nD = numDerivs;
  //This assignment is only here to make the following code shorter 
  //  and fit on single lines
  
  

  
  if( ( 0 == order ) && (0 == elem) ) 
    {
      return (0.5) *
	(ddpPsi(grid,0,0,nD,x) - ddpPsi(grid,0,1,nD,x) );
    }

  else if(  (0 == order) && (elem == (NumElementsNoGhost)))
    {

      return (0.5) * 
	(ddpPsi(grid,NumElementsNoGhost - 1, 0,nD,x)
	 + ddpPsi(grid,NumElementsNoGhost - 1, 1,nD,x) );
      //The minus two here is something we don't really like, but it is
      // on purpose.
    }
  else if (0 == order)
    {
      return (0.5) * ( ddpPsi(grid, elem - 1,0,nD,x) 
		       + ddpPsi(grid, elem - 1,1,nD,x) 
		       + ddpPsi(grid,elem, 0, nD, x) 
		       -ddpPsi(grid,elem,1,nD,x) );
    }
  else  
    {
      return ddpPsi(grid, elem, order+1, nD, x) - ddpPsi(grid,elem, J,nD, x);
    }
}


////////////////////////////////////////////////////////////////////////////////



int
ddpMakeUniformGrid(ddpProblemInfo_type const & problem, 
		   ddpDomain_type const & domain, 
		   ddpGrid_type & grid)
{

  // Make uniform grid. Although we make a uniform grid, what follows does 
  // not require the grid to be uniform

  // TODO:  The integration points are attached here in the make uniform grid,
  //        This will cause problems if we ever want to use non-uniform grids.
  //        The proper fix is to do something else to attach the grid points

	  int numElementsNoGhost = grid.NumElementsNoGhost;
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  
	  
	  double xLeftEndPoint = domain.LeftEndPoint;
	  double xRightEndPoint = domain.RightEndPoint;
	  
	  
	  ddpElement_type * tempElementList = new ddpElement_type[numElementsWithGhost];

	 
	  ddpElement_type tempElement;
	  double Length = xRightEndPoint - xLeftEndPoint;
	  
	  double deltaX = Length / (numElementsNoGhost);
	    
	  int maxOrderDG = problem.MaxOrderDG;
	  int maxOrderMX = problem.MaxOrderMX;
	  int gaussLobattoNumPoints = problem.GaussLobattoNumPoints;
	  int gaussLegendreNumPoints = problem.GaussLegendreNumPoints;
	  
	  grid.DeltaxMin = deltaX;
	  grid.OrderDGMax = maxOrderDG;

	  //Todo: Setting the points shouldn't really happen here 
	  // (in order to accomodate non-uniform meshed );
	  
	  // TODO: These should be freed
	  double *
	     gaussLobattoPoints = new double[gaussLobattoNumPoints];
	  double *
	    gaussLobattoWeights = new double[gaussLobattoNumPoints];
	  double * 
	    gaussLegendrePoints = new double[gaussLegendreNumPoints];
	  double * 
	    gaussLegendreWeights = new double [gaussLegendreNumPoints];
	  
	  // contained in quadrule.cpp
	  lobatto_set (gaussLobattoNumPoints, gaussLobattoPoints, gaussLobattoWeights);
	  legendre_set (gaussLegendreNumPoints, 
			gaussLegendrePoints, 
			gaussLegendreWeights);

	    for(int i = 0; i < numElementsNoGhost; ++i)
	    {
	      tempElement.Left = xLeftEndPoint + (i ) * deltaX;
	      tempElement.Right = xLeftEndPoint + (i+1) * deltaX;
	      tempElement.Delta = tempElement.Right - tempElement.Left;
	      tempElement.OrderMX = maxOrderMX; 
	      tempElement.OrderDG = maxOrderDG; 
	      tempElement.GaussLegendreNumPoints = gaussLegendreNumPoints;
	      tempElement.GaussLobattoNumPoints  = gaussLobattoNumPoints;
	      
	      // Todo: Question for Mike: Do we need to explicitly call delete or free 
	      // on these?
	      tempElement.GaussLobattoPoints = new double[gaussLobattoNumPoints];
	      tempElement.GaussLobattoWeights = new double[gaussLobattoNumPoints];
	      for(int j = 0; j < gaussLobattoNumPoints; ++j)
		{	  
		  (tempElement.GaussLobattoPoints)[j] 
		    =  tempElement.Left + deltaX * 0.5 * (gaussLobattoPoints[j] + 1);
		  (tempElement.GaussLobattoWeights)[j] 
		    = 0.5 * (tempElement.Right - tempElement.Left) * 
		    gaussLobattoWeights[j];
		}
		
	      tempElement.GaussLegendrePoints = new double[gaussLegendreNumPoints];
	      tempElement.GaussLegendreWeights = new double[gaussLegendreNumPoints];
	      
	      for(int j = 0; j < gaussLegendreNumPoints; ++j)
		{
		  (tempElement.GaussLegendrePoints)[j]
		    = tempElement.Left + deltaX / 2 * (gaussLegendrePoints[j] + 1);
		   (tempElement.GaussLegendreWeights)[j] 
		    = 0.5 * (tempElement.Right - tempElement.Left) * 
		     gaussLegendreWeights[j];
		}
	      tempElement.Delta = tempElement.Right - tempElement.Left;

	      tempElementList[i] = tempElement;
	      
	    }

	  // We still need to add the upsilon element at the extreme right hand side
	  // We do that here.
	  
	  // Add the extreme right hand side element
	  // tempElement.Delta = NAN;
	  // This is an ugly hack, need to fix this later.
	  tempElement.Left =  domain.RightEndPoint;
	  tempElement.Right = domain.RightEndPoint;
	  tempElement.OrderMX = 0;
	  tempElement.OrderDG = -1;
	  tempElement.GaussLobattoNumPoints = -1;
	  tempElement.GaussLobattoPoints = NULL;
	  tempElement.GaussLobattoWeights = NULL;
	  tempElement.GaussLegendreNumPoints = -1;
	  tempElement.GaussLegendrePoints = NULL;
	  tempElement.GaussLegendreWeights = NULL;
	  
	  
	  // Set the last element to the temp element
	  tempElementList[numElementsWithGhost - 1] = tempElement;
	  grid.ElementList = tempElementList;
	  
	  return 0;
	}



int
ddpMakeTimeStamps(ddpProblemInfo_type const & problem, 
		  std::vector< double > & timeStamps)
	{
	  double tFinal = problem.TimeFinal;
	  double tInitial = problem.TimeInitial;
	  int numFrames = problem.NumFrames;
	  
	  cout << "numFrames = " << numFrames << endl;
	  timeStamps.reserve(numFrames);

	  double FrameDeltaT = (tFinal - tInitial) / (numFrames - 1);
	  // TODO:  think about the right way to handle the case where we only want
	  //        one frame, the initial condition.
	  //        The "minus one" is for the fact that we will output the 
	  //        initial condition.
	  //  Solution:  Don't think in terms of frames, think in terms of time steps
	  //             (fencepost problem)

	  //double * timeStampsTemp = new double[numFrames];  
	  for(int j = 0; j < numFrames; ++j)
	    {
	      timeStamps.push_back( tInitial + FrameDeltaT * j);
	    }
	  return 0;  
	}


int
ddpMakeBijs(ddpGrid_type const & grid,
	    ddpBijFlag_type const & flag,
	    ddpBijForw_type& Forward,
	    ddpBijBack_type& Backward)
	{
	  int numElements = grid.NumElementsWithGhost;
	  ddpElement_type currentElement;
	  int counter = 0;
	  int LocalMax = 0;

	  std::pair < int, int > tempPair;
	  std::pair < std::pair < int, int >, int > tempDataForward;
	  std::pair < int, std::pair <int, int > > tempDataBackward;
	  
	  // Here, we loop over every element in the grid, 
	  // and depending on how the flag is set, we perform an inner loop.
	  
	  // Note that we loop over all the elements, including the ghost elements.
	  // This doens't cause a problem because of the way the element data is set
	  // for the ghost element.
	  for(int i = 0; i < numElements; ++i)
	    {
	      currentElement = grid.ElementList[i];    
	      switch(flag)
		{
		case DG:
		  LocalMax = currentElement.OrderDG+1;
		  // plus one is on purpose
		  break;
		case Mixed:
		  LocalMax = currentElement.OrderMX+1;  // plus one is on purpose
		  break;
		case Points:
		  LocalMax = (currentElement.GaussLegendreNumPoints);
		  // lack of plus one is on purpose
		  break;
		default:
		  cout << "Can't happen on line number "  << __LINE__ << endl;
		}
	      
	      for(int j = 0; j < LocalMax; ++j)
		{
		  tempPair.first  = i;
		  tempPair.second = j;
		  
		  tempDataForward.first   = tempPair;
		  tempDataForward.second  = counter;	  
		  
		  tempDataBackward.first  = counter;
		  tempDataBackward.second = tempPair;
		  
		  Forward.insert (tempDataForward );
		  Backward.insert (tempDataBackward);

		  ++counter;
		}
	    }
	  return 0;  
	}


			    




int
ddpMakeGlobalVandeMonde(ddpGrid_type const & grid, 
			ddpProblemInfo_type const & problem,
			ddpBijFlag_type const & flag,	
			ddpBijForw_type const & FuncForward,
			ddpBijForw_type const & PTForward,
			ddpNumDeriv_type const & numDeriv,
			ddpSparseMatrix_type & vandeMonde)
{
	  // THIS WILL MAKE A VANDEMONDE MATRIX SUCH THAT 
	  //  (NumPoints x numDofs) and the rows corresond
	  //  to the element and order the rows columns are the
	  //  basis functions evaluated at the points
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  int numElementsNoGhost = grid.NumElementsNoGhost;

	    
	  int localOrderMax;
	  int localNumPoints;
	  long int numDofs = FuncForward.size();
	  long int numPtsPerDof = problem.GaussLegendreNumPoints;
	  long int numPts = PTForward.size(); 
	  


	  std::pair<int, int> elemAndOrderJ;
	  std::pair<int, int> elemAndPointI;  

	  int GlobalI;  // global matrix row
	  int GlobalJ; // global matrix column
	  double ValueIJ; // value at matrix(GlobalI,GlobalJ)
	  double pt;
	  
	  // This is just used to build the sparse matrix
	  typedef
	    Eigen::Triplet < double > T;
	  
	  std::vector<T> triplets;
	  ddpElement_type localElement;

	  long long int estimateofsize;
	  estimateofsize = numDofs * numPtsPerDof;
	  
	  
	  triplets.reserve(estimateofsize);
	  
	  Eigen::SparseMatrix<double> vandeMondeAux(numPts,numDofs);
	  //vandeMondeAux.reserve(estimateofsize);
	  
	  // Loop over just the nearest neighbors
	  for(int elem1 = 0; elem1 < numElementsWithGhost; ++elem1)
	    { 
	      for( int elem2 = std::max(0,elem1 - 1); 
		   elem2 < std::min(numElementsNoGhost, elem1 + 1);
		   ++elem2)
		{
		  // Figure out if your doing mixed or DG 
		  switch (flag) 
		    {
		    case DG:
		      localOrderMax = grid.ElementList[elem2].OrderDG;
		      break;
		    case Mixed:
		      localOrderMax = grid.ElementList[elem2].OrderMX;
		      break;
		    default:
		      cout << "this shouldn't happen on line" << __LINE__ << endl;
		      return 1;
		    }
	  
		  localElement = grid.ElementList[elem2];
		  for(int order = 0; order <= localOrderMax; ++order)
		    {	  
		      localNumPoints = localElement.GaussLegendreNumPoints;
		      for(int pointNumber = 0; 
			  pointNumber < localNumPoints;
			  ++pointNumber)
			{
			  
			  elemAndOrderJ.first = elem1;
			  elemAndOrderJ.second = order;
			  
			  elemAndPointI.first = elem2;
			  elemAndPointI.second = pointNumber;
			  
			  pt = grid.ElementList[elem2].GaussLegendrePoints[pointNumber];
			  
			  GlobalJ = FuncForward.find(elemAndOrderJ)->second;
			  GlobalI = PTForward.find(elemAndPointI)->second;
			  
			  switch(flag)
			    {
			    case DG:
			      ValueIJ = ddpPsi(grid, elem1, order, numDeriv, pt);
			      break;
			    case Mixed:
			      ValueIJ = ddpUpsilon(grid, elem1, order, numDeriv,pt);
			      break;
			    default:
			      cout << "this shouldn't happen on line"  << __LINE__ 
				   << endl;
			      return 1;
			      break;
			    }
			
			
			  double absValueIJ = fabs(ValueIJ);
			  double Threeeps = 12.0*std::numeric_limits<double>::epsilon();

			  if( absValueIJ > Threeeps )
			    {
			      triplets.push_back( T(GlobalI,GlobalJ,ValueIJ) );
			    }
			}
		    }
		}
	    }
	  vandeMondeAux.setFromTriplets(triplets.begin(), triplets.end() );
	  vandeMondeAux.makeCompressed(); 
	  vandeMonde = vandeMondeAux;
	  return 0;
	}

int
ddpMakeGlobalVandeMondeFluxDG(ddpGrid_type const & grid, 
			      ddpProblemInfo_type const & problem,
			      ddpBijForw_type const & FuncForward,
			      ddpDirection_type const & Flag1,
			      ddpDirection_type const & Flag2,		      
			      ddpSparseMatrix_type & vandeMonde)
{
	  
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  int numElementsNoGhost = grid.NumElementsNoGhost;

	  int numFunctions = FuncForward.size();
	  int numEndpoints = numElementsNoGhost + 1;
	  
	  ddpSparseMatrix_type vandeMondeAux(numEndpoints - 1,numFunctions);


	  std::pair<int, int> elemAndOrderI;
	  std::pair<int, int> elemAndOrderJ;
	  
	  int GlobalI;
	  int GlobalJ;
	  double ValueIJ;
	   
	  typedef
	    Eigen::Triplet < double > T;
	  
	  std::vector<T> triplets;
	  
	  long long int estimateofsize;
	  estimateofsize = numEndpoints;
	  
	  // A ROW IS THE DIFFERENT BASIS FUNCTION
	  // A COLUMN IS THE CHOSEN END POINT OF THE ELEMENT
	  // OVER WHICH THE BASIS HAS SUPPORT

	  triplets.reserve(3 * estimateofsize);
	  int endpointNumber;
	  for(int elem1 = 0; elem1 < numElementsNoGhost; ++elem1)
	    {
	      int MaxDGOrder = grid.ElementList[elem1].OrderDG;
	      
	      for(int elem2 = std::max(0, elem1 - 1 ); 
		  elem2 <= std::min(numElementsNoGhost - 1, elem1 + 1);
		  ++elem2)
		{
		  for(int order = 0; order <= MaxDGOrder; ++order)
		    {
		      int offset;
		      
		      if(Plus == Flag1)
			{
			  offset = 1;
			  
			}
		      else if(Minus == Flag1)
			{
			  offset = 0;
			}	 
		      
		      else
			{
			  cout << "this can't happen on line "  << __LINE__ << endl;
			  assert(false);
			}
		      
		      elemAndOrderJ.first = elem1;
		      elemAndOrderJ.second = order;
		      GlobalJ = FuncForward.find(elemAndOrderJ)->second;
		      
			   
		      GlobalI = elem2;
		      endpointNumber = elem2 + offset;
		      

		      ValueIJ = ddpLimPsi(grid, elem1, order, Flag2, endpointNumber);
		    
		      
		      double absValueIJ = fabs(ValueIJ);
		      double Threeeps = 12.0*std::numeric_limits<double>::epsilon();
		      
		      if( absValueIJ > Threeeps )
			{
			  triplets.push_back( T(GlobalI,GlobalJ,ValueIJ) );
			}
		      
		    }
		}
	    }
	  vandeMondeAux.setFromTriplets(triplets.begin(), triplets.end() );
	  vandeMondeAux.makeCompressed(); 
	  vandeMonde = vandeMondeAux;
	 
	  
	  return 0;
	}

	//TODO:  These functions before and after this TODO 
	// are in a different order in main.  Fix this.

	   
int
ddpMakeGlobalVandeMondeFluxMX(ddpGrid_type const & grid, 
			      ddpProblemInfo_type const & problem,
			      ddpBijForw_type const & FuncForward,
			      ddpSparseMatrix_type & vandeMonde)
{
	  
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  int numElementsNoGhost = grid.NumElementsNoGhost;
	  
	  int numFunctions = FuncForward.size();
	  int numEndpoints = numElementsNoGhost + 1;  
	  
	  ddpSparseMatrix_type vandeMondeAux(numEndpoints, numFunctions);


	  typedef
	    Eigen::Triplet < double > T;
	  
	  std::vector<T> triplets;
	  
	  long long int estimateofsize;
	  estimateofsize = numEndpoints;
	  
	  
	  triplets.reserve(estimateofsize);
	  
	  for(int elem = 0; elem < numElementsWithGhost - 1; ++elem)
	    // minus one on NumElements is on purpose to deal with the last
	    // element specifically.
	    {
	      int MaxMXOrder = grid.ElementList[elem].OrderMX;
	      
	      // TODO: Make this better.

	      for(int order = 0; order <= MaxMXOrder; ++order)
		{
		  std::pair<int, int> elemAndOrderJ;
		  
		  int GlobalI;
		  int GlobalJ;
		  double ValueIJ;
		  elemAndOrderJ.first = elem;
		  elemAndOrderJ.second = order;
		  
		  
		  double pt = grid.ElementList[elem].Left;
		  // loop over all "possible" points
		  // "possible" in here means possible for upsilon to be
		  // nonzero.
		  
		  GlobalJ = FuncForward.find(elemAndOrderJ)->second;
		  GlobalI = elem;
		  ddpNumDeriv_type nD = zero;
		  ValueIJ = ddpUpsilon(grid, elem, order, nD, pt);

		  //This is an ugly hack to account for the overcounting 
		  // of the interior
		  // element endpoints.  A less hacky way to do this 
		  // would be to 
		  // implement some sort of "limit" functional like 
		  // in Mathematica.
		  // For now, this works.
		  if(0 != elem)
		    {
		      ValueIJ *= 0.5;
		    }
		  double absValueIJ = fabs(ValueIJ);
		  double Threeeps = 12.0*std::numeric_limits<double>::epsilon();
		  
		  if( absValueIJ > Threeeps )
		    {
		      triplets.push_back( T(GlobalI,GlobalJ,ValueIJ) );
		    }
		}
	    }
	  
	  //Need to add the last function, the last "half tent" function;  

	  
	  int elem = numElementsWithGhost - 1; //The last element
	  int order = 0;

	  int GlobalI;
	  int GlobalJ;
	  double ValueIJ;
	  std::pair<int, int> elemAndOrderJ;
	  double pt = (grid.ElementList[numElementsNoGhost - 1 ]).Right;

	  elemAndOrderJ.first = elem;
	  elemAndOrderJ.second = order;
	  


	  // The upsilon we will consider here is the last function,
	  // half of its domain is on the last element and
	  // the other half of its domain is the "ghost element".
	  GlobalI = numElementsNoGhost;
	  GlobalJ = FuncForward.find(elemAndOrderJ)->second;
	  ddpNumDeriv_type nD = zero;
	  ValueIJ = ddpUpsilon(grid, elem, order, nD, pt);
	  
	  double absValueIJ = fabs(ValueIJ);
	  double Threeeps = 12.0*std::numeric_limits<double>::epsilon();
	  
	  if( absValueIJ > Threeeps )
	    {
	      triplets.push_back( T(GlobalI,GlobalJ,ValueIJ) );
	    }
	  
	  
	  vandeMondeAux.setFromTriplets(triplets.begin(), triplets.end() );
	  vandeMondeAux.makeCompressed(); 
	  vandeMonde = vandeMondeAux;
	  
	  return 0;
	}


int
ddpMakeWeightVectorDense(ddpGrid_type const & grid,
		    ddpDenseVector_type &weights)
{

	  ddpDenseVector_type temp1;
	  Eigen::VectorXd temp;
	  
	  //TODO:  NumElements Issue
	  int numElementsNoGhost = grid.NumElementsNoGhost;
	  std::vector<double> weightsVector;
	  int pointCount = 0;
	  
	  //TODO:  NumElements Issue
	  for(int i = 0; i < numElementsNoGhost; ++i)
	    {      
	      int numLocalPoints = (grid.ElementList[i]).GaussLegendreNumPoints;
	      for(int j = 0; j < numLocalPoints; ++j)
		{     	  
		  double localWeight = (grid.ElementList[i]).GaussLegendreWeights[j];
		  weights(pointCount) = localWeight;
		  pointCount++;
		}
	    }  
	  return 0;
	}

int
ddpMakeWeightVectorSparse(ddpGrid_type const & grid,
   			  ddpSparseVector_type & weights)
{
	  //TODO:  NumElements Issue
	  int numElements = grid.NumElementsNoGhost;
	  
	  int pointCount = 0;
	 
	  for(int i = 0; i < numElements; ++i)
	    {      
	      int numLocalPoints = (grid.ElementList[i]).GaussLegendreNumPoints;
	      for(int j = 0; j < numLocalPoints; ++j)
		{     	  
		  double localWeight = (grid.ElementList[i]).GaussLegendreWeights[j];
		  weights.insert(pointCount)  = localWeight;
		  pointCount++;	  
		}
	    }  
	  
	  return 0;
	}

	int
	ddpMakePTVectorSparse(ddpGrid_type const & grid,
			      ddpSparseVector_type & points)
	{

	  
	  
	  int numElements = grid.NumElementsNoGhost;
	  
	  int pointCount = 0;
	 
	  
	  for(int i = 0; i < numElements; ++i)
	    {      
	      int numLocalPoints = (grid.ElementList[i]).GaussLegendreNumPoints;
	      for(int j = 0; j < numLocalPoints; ++j)
		{     	  
		  double localPT = (grid.ElementList[i]).GaussLegendrePoints[j];
		  points.insert(pointCount)  = localPT;
		  pointCount++;	  
		}
	    }  
	  return 0;
	}


int
ddpMakePTVectorDense(ddpGrid_type const & grid,
		     ddpDenseVector_type & points)
{
	  
	 
	  //TODO:  NumElements Issue
	  int numElements = grid.NumElementsNoGhost;
	  
	  int pointCount = 0;
	 
	  for(int i = 0; i < numElements; ++i)
	    {      
	      int numLocalPoints = (grid.ElementList[i]).GaussLegendreNumPoints;
	      for(int j = 0; j < numLocalPoints; ++j)
		{     	  
		  double localPT = (grid.ElementList[i]).GaussLegendrePoints[j];
		  points(pointCount)  = localPT;
		  pointCount++;
		}
	    }  
	  return 0;
	}





int
ddpMakeGeneralMatrix(ddpProblemInfo_type const & problem,
		     ddpGrid_type const & grid,
		     ddpSparseVector_type const & sparseWeights,
		     ddpBijFlag_type const & flagI,
		     ddpBijForw_type const & ForwardI,
		     ddpSparseMatrix_type const & globalVandeMondeI,
		     ddpBijFlag_type const & flagJ,
		     ddpBijForw_type const & ForwardJ,
		     ddpSparseMatrix_type const & globalVandeMondeJ,
		     ddpSparseMatrix_type & OutputMatrix
		     )
{
	  ddpSparseVector_type localWeights = sparseWeights;
	  
	  
	  int numElements = grid.NumElementsWithGhost;
	  int numTestFunctions = ForwardI.size();  //Test Functions
	  int numDof = ForwardJ.size();            //Trial Functions
	  
	 
	  ddpSparseMatrix_type MatrixTemp( numTestFunctions, numDof);
	  /*
	    cout << "will attempt to make MatrixTemp to be of size (" 
	    << numTestFunctions << "x" <<  numDof << ")"  << endl;/**/

	  typedef
	    Eigen::Triplet < double > T;
	  
	  std::vector<T> triplets;
	  
	  // We need to estimate the number of nonzero entries in the matrix.
	  // Without thinking too hard, we decide to just use the maximum of 
	  // DGandMXOrder
	 
	  // Todo: Think of a better way to do this.  (if memory becomes an issue).
	  int maxOfDGandMXOrder = std::max(problem.MaxOrderDG, problem.MaxOrderMX);
	  
	  int estimateOfSize = numTestFunctions * maxOfDGandMXOrder * maxOfDGandMXOrder;
	  
	  triplets.reserve(estimateOfSize);
	  
	  //We look over all the elements and local orders.
	  //Note that we are looping over the ghost elements even for the DG matrices.
	  // This is ok since since the MaxOrderDG for the ghost element is set 
	  // to something like -1.
	  for(int elem1 = 0; elem1 < numElements; ++ elem1)
	    {
	      //cout << "elem1 = " << elem1 << endl;
	      int localOrderMaxJ;
	      switch (flagJ) 
		{
		case DG:
		  localOrderMaxJ = grid.ElementList[elem1].OrderDG;
		  break;
		case Mixed:
		  localOrderMaxJ = grid.ElementList[elem1].OrderMX;
		  break;
		default:
		  cout << "this shouldn't happen on line " << __LINE__ << endl;
		  return 1;
		} 
	      
	      //Now loop over the neighbors of elem1
	      //TODO: Look in the nodal discontinous galerkin book to see how to do
	      //      this.
	      

	      // The plus 2 is required here.
	      for(  int elem2 = std::max(0,elem1-1); 
		    elem2 < std::min(numElements, elem1+2);
		    ++elem2)
		{
		  //cout << "   elem2 = " << elem2 << endl;
		  int localOrderMaxI;
		  switch (flagI) 
		    {
		    case DG:
		      localOrderMaxI = grid.ElementList[elem2].OrderDG;
		      break;
		    case Mixed:
		      localOrderMaxI = grid.ElementList[elem2].OrderMX;
		      break;
		    default:
		      cout << "this shouldn't happen on line " << __LINE__ << endl;
		      return 1;
		    }
		  
		  
		  for( int order1 = 0; order1 <= localOrderMaxJ; ++order1)
		    {
		    
		      
		      
		      for( int order2 = 0; order2 <= localOrderMaxI; ++order2)
			{
		
			  std::pair<int, int> elemAndOrderI;
			  std::pair<int, int> elemAndOrderJ;
			  elemAndOrderI.first = elem2;
			  elemAndOrderI.second = order2;
			  elemAndOrderJ.first = elem1;
			  elemAndOrderJ.second = order1;
			  
			  int globalI = ForwardI.find(elemAndOrderI)->second;
			  int globalJ = ForwardJ.find(elemAndOrderJ)->second;
			  
		
			  // So now we know the global where to find the 
			  // values for the functions
			  ddpSparseVector_type temp1 = globalVandeMondeI.col(globalI);
			  ddpSparseVector_type temp2 = globalVandeMondeJ.col(globalJ);
			  
					  
			  //We loop over the nonzero elements of temp1
			  double partial_sum = 0.0;
			  for(ddpSparseVector_type::InnerIterator it(temp1); it; ++it)
			    {
			      //TODO:  Change coeffRef to coeff
			      int ind = it.index();
			      double t1val = temp1.coeffRef(ind);
			      double t2val = temp2.coeffRef(ind);
			      double weightval = localWeights.coeffRef(ind);
			      
			      partial_sum += t1val * t2val * weightval;
			    }
			  double valueIJ = partial_sum;
			  
			  
					  
			  double absValueIJ = fabs(valueIJ);
			  //TODO:  Need a rational basis for the constant
			  double Threeeps 
			    = 12.0 * std::numeric_limits<double>::epsilon();
			  
			  if( absValueIJ > Threeeps )
			    {
			      // cout << "matrix(" << globalI << "," << globalJ 
			      //<< ")= " << valueIJ << endl;
			      
			      triplets.push_back( T(globalI, globalJ, valueIJ) );
			    }
			}
		    }
		}
	    }

	  MatrixTemp.setFromTriplets(triplets.begin(), triplets.end() );
	  MatrixTemp.makeCompressed();
	  OutputMatrix = MatrixTemp;
	  
	  return 0;
}


int ddpMakeGeneralFluxVector(ddpGrid_type const & grid,
			     ddpDenseVector_type const & VPosOrNeg,
			     int const & ElementNumber,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type  const & DGFluxMatrices,
			     ddpDirection_type const & Flag1, 
			     ddpSparseMatrix_type & output)
{
	  int numElementsNoGhost = grid.NumElementsNoGhost;
	  
	  const ddpSparseMatrix_type * Mneg;
	  const ddpSparseMatrix_type * Mpos;
	  const ddpSparseMatrix_type * Mtest;

	  int offset;
	  
	  long long int NumTestFunctions = FuncForward.size();
	  
	  int one = 1;

	  ddpSparseMatrix_type VectorTemp( NumTestFunctions, one);

	  typedef
	    Eigen::Triplet < double > T;
	  
	  std::vector<T> triplets;  
	  //TODO:  Pick a better choice for this estimate
	  long long int estimateofSize = 20;
	  triplets.reserve(estimateofSize);


	   // Right Side of Element	  
	  if(Plus == Flag1)
	    {
	      Mneg = &(DGFluxMatrices.plusminus);
	      Mpos = &(DGFluxMatrices.plusplus);
	      Mtest = &(DGFluxMatrices.plusminus);
	      offset = 1;
	    } // Left Side of Element
	  else if( Minus == Flag1 )
	    {
	      Mneg = &(DGFluxMatrices.minusminus);
	      Mpos = &(DGFluxMatrices.minusplus);
	      Mtest = &(DGFluxMatrices.minusplus);
	      offset = 0;
	    }
	  else
	    {
	      cout << "can't happen on line " << __LINE__ << endl;
	      assert(false);
	    }

	  for(int elem1 = ElementNumber; elem1 < ElementNumber+1; ++elem1)
	    {
	      int maxLocalOrderDG1 = (grid.ElementList[elem1]).OrderDG;
	      for(int order1 = 0; order1 <= maxLocalOrderDG1; ++ order1)
		{
		  //cout << "  order1 = " << order1 << endl;
		  //Compute GlobalJ
		  std::pair<int, int> elemAndOrderI;
		  
		  elemAndOrderI.first = elem1;
		  elemAndOrderI.second = order1;
		  
		  int GlobalI = FuncForward.find(elemAndOrderI)->second;
		  
		  double ValueI;
		  // cout << "  GlobalI = " << GlobalI << endl;
		  double 
		    termPosOrNeg;

		  double termTest;
		  
		  int elem1PlusOffset = elem1 + offset;
		  
		  termPosOrNeg = VPosOrNeg.coeff(elem1PlusOffset);
		  termTest = Mtest->coeff(elem1, GlobalI);
			  
		  ValueI = termPosOrNeg * termTest;

		  double absValueI = fabs(ValueI);
		  
		  double Threeeps 
		    = 12.0 * std::numeric_limits<double>::epsilon();

		  int GlobalJ = 0;
		  if( absValueI > Threeeps )
		    {
		      triplets.push_back( T(GlobalI,GlobalJ, ValueI) );
		    }
		}
	    }

	  VectorTemp.setFromTriplets(triplets.begin(), triplets.end() );
	  VectorTemp.makeCompressed();
	  output = VectorTemp;
	  
	  return 0;
}

int ddpMakeGeneralFluxMatrix(ddpGrid_type const & grid,
			     ddpDenseVector_type const & Vpos,
			     ddpDenseVector_type const & Vneg,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type  const & DGFluxMatrices,
			     ddpDirection_type const & Flag1, 
			     ddpSparseMatrix_type & output)
{
	  int numPoints = grid.NumElementsWithGhost;
	  int numElementsNoGhost = grid.NumElementsNoGhost;
	  
	  ddpDenseVector_type VposShort, VnegShort;
	  
	  
	  //TODO:  There doesn't seem to be much of a point in doing things this way.
	  //  Keep for now;
	  const ddpSparseMatrix_type * Mneg;
	  const ddpSparseMatrix_type * Mpos;
	  const ddpSparseMatrix_type * Mtest;

	  int offset;
	  
	  long long int NumTestFunctions = FuncForward.size();
	     
	  /*
	    cout << "will attempt to make MatrixTemp to be of size (" 
	    << numTestFunctions << "x" <<  numDof << ")"  << endl;/**/
	    
	  if(Plus == Flag1)
	    {
	      Mneg = &(DGFluxMatrices.plusminus);
	      Mpos = &(DGFluxMatrices.plusplus);
	      Mtest = &(DGFluxMatrices.plusminus);
	      offset = 1;
	    }
	  else if( Minus == Flag1 )
	    {
	      Mneg = &(DGFluxMatrices.minusminus);
	      Mpos = &(DGFluxMatrices.minusplus);
	      Mtest = &(DGFluxMatrices.minusplus);
	      offset = 0;
	    }
	  else
	    {
	      cout << "can't happen on line " << __LINE__ << endl;
	    }

	  VposShort = Vpos.segment(offset, numPoints - 1);
	  VnegShort = Vneg.segment(offset, numPoints - 1);
	  /*cout << "VposShort = \n" << VposShort << endl;
	  cout << "VnegShort = \n" << VnegShort << endl;
	  cout << "Mneg->rows() = " << Mneg->rows() << endl;
	  cout << "Mneg->cols() = " << Mneg->cols() << endl;

	  cout << "Mpos->rows() = " << Mpos->rows() << endl;
	  cout << "Mpos->cols() = " << Mpos->cols() << endl;
	  
	  cout << "Mtest->rows() = " << Mtest->rows() << endl;
	  cout << "Mtest->cols() = " << Mtest->cols() << endl;
	  */

	  ddpDiagonalMatrix_type VposShortDiagMatrix(VposShort.size() );
	  ddpDiagonalMatrix_type VnegShortDiagMatrix(VnegShort.size() );
	  ddpSparseMatrix_type TEMP1, TEMP2;
	  
	  VposShortDiagMatrix = VposShort.asDiagonal();
	  VnegShortDiagMatrix = VnegShort.asDiagonal();
	  
	      
	  //TODO:  Understand why Mneg is paired with Vpos and why
	  //                      Mpos is paired with Vneg.
	  //Answer: This is correct.  
	  ddpSparseMatrix_type
	    temp1,
	    temp2;


	  
	  temp1 = Mtest->transpose() * VposShortDiagMatrix * (*Mneg);
	  temp2 = Mtest->transpose() * VnegShortDiagMatrix * (*Mpos);
	  
	  output = (temp1 + temp2);
	  
	 
	  //((*Mneg) * VposShortDiagMatrix + (*Mpos) * VnegShortDiagMatrix) * Mtest->transpose();
	  //  cout << "TEMP11 = \n"  << TEMP11 << endl;  
	  //output = TEMP1 * (Mtest->transpose());
	  //cout << "TEMP2 = " << TEMP22 << endl;
	  
	  return 0;

	}

int ddpMakeSpecialFluxMatrix(ddpGrid_type const & grid,
			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type const & DGFluxMatrices,
			     ddpDirection_type const & Flag1,
			     std::vector<ddpDirection_type> const& Flag2Vector,
			     ddpSparseMatrix_type & output
			     )
{
	  
	   
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  
	  ddpDenseVector_type dummyVPos(numElementsWithGhost);
	  ddpDenseVector_type dummyVNeg(numElementsWithGhost);
	  
	  //TODO:  Maybe a switch statement?
	    for(int i = 0; i < numElementsWithGhost; ++i)
	      {
		if(Plus == Flag2Vector[i])
		  {
		    dummyVPos(i) = 0.0;
		    dummyVNeg(i) = 1.0;
		  }
		else if(Minus == Flag2Vector[i])
		  {
		  dummyVPos(i) = 1.0;
		  dummyVNeg(i) = 0.0;
		  }
		else
		  {
		    cout << "error on line " << __LINE__ << endl;
		    assert(false);
		  }
	      }
	    
	    
	  
	    
	  ddpMakeGeneralFluxMatrix( grid, 
				    dummyVPos, 
				    dummyVNeg, 
				    FuncForward,
				    DGFluxMatrices,
				    Flag1,
				    output);
	  return 0;
	}

int ddpMakeSpecialFluxVector(ddpGrid_type const & grid,
			     int const & ElementNumber,
 			     ddpBijForw_type const & FuncForward,
			     ddpDGFluxMatrices_type const & DGFluxMatrices,
			     ddpDirection_type const & Flag1,
			     ddpSparseMatrix_type & output)
{
	  int numElementsWithGhost = grid.NumElementsWithGhost;
	  
	  ddpDenseVector_type 
	    dummyV(numElementsWithGhost);
	  
	  for(int i = 0; i < numElementsWithGhost; ++i)
	    {
	      dummyV(i) = 1.0;
	    }
	    
	  ddpMakeGeneralFluxVector(grid,
				   dummyV,
				   ElementNumber,
				   FuncForward,
				   DGFluxMatrices,
				   Flag1, 
				   output);
	    
	  
  return 0;
}


int	
ddpMakeWeightsAndPoints(ddpGrid_type const & grid, 
			ddpProblemInfo_type const & problem,
			ddpSparseVector_type & weightsSparse,
			ddpSparseVector_type & PTSSparse, 
			ddpDenseVector_type & PTSDense, 
			ddpDenseVector_type & weightsDense)
{

	 // Makes weights and points vectors for quadrature

	  int weightVectorLength = (grid.NumElementsNoGhost)  
	    * problem.GaussLegendreNumPoints;
	  
	  cout << "Entering MakeWeightVectorSparse" << endl;
	  ddpSparseVector_type 
	    temp_weightsSparse(weightVectorLength),
	    temp_PTSSparse(weightVectorLength);
	  
	  ddpDenseVector_type
	    temp_PTSDense(weightVectorLength),
	    temp_weightsDense(weightVectorLength);
	  
	  ddpMakeWeightVectorSparse(grid, temp_weightsSparse);

	    temp_weightsDense = temp_weightsSparse;
	    //cout << "weightsSparse = \n" << weightsSparse << endl;  
	    
	    cout << "\tExiting MakeWeightVectorSparse" << endl;

	  cout << "Entering MakePTVectorSparse"  << endl;
	  ddpMakePTVectorSparse(grid, temp_PTSSparse);
	  //cout << "PTSSparse = \n" << PTSSparse << endl;
	  
	  cout << "\tExiting MakePTVectorSparse" << endl;

	  cout << "Entering MakePTVectorDense" << endl;
	  
	  ddpMakePTVectorDense(grid, temp_PTSDense);
	  //cout << "PTSDense = " << PTSDense << endl;
	  
	  cout << "\tExiting MakePTVectorSparse" << endl;

	  weightsSparse = temp_weightsSparse;
	  PTSSparse = temp_PTSSparse;
	  PTSDense = temp_PTSDense;
	  weightsDense = temp_weightsDense;
	  
  return 0;
}

int 
ddpMakeAllBijections(ddpGrid_type const & grid,
		     ddpBijection_type & Bijections)
{
	  
	  ddpBijFlag_type flag;

	  cout << "Entering MakeBijs" << endl;
	  flag = DG;
	  ddpMakeBijs(grid, flag, Bijections.DGForward, Bijections.DGBackwrd);
	 
	  flag = Mixed;
	  ddpMakeBijs(grid, flag, Bijections.MXForward, Bijections.MXBackwrd);
	  
	  flag = Points;
	  ddpMakeBijs(grid, flag, Bijections.PTForward, Bijections.PTBackwrd);
	  cout << "\tExiting MakeBijs" << endl;

  return 0;
}


int 
ddpMakeVandeMondeMatrices(ddpGrid_type const & grid, 
			  ddpProblemInfo_type const & problem, 
			  ddpBijection_type const & Bijections,
			  ddpVandeMondeMatrices_type & VandeMondeMatrices,
			  ddpDGFluxMatrices_type & DGFluxMatrices)
{

	  // Create and Global VandeMonde Matrices for mixed and dg spaces
	  ddpSparseMatrix_type 
	   temp_globalVandeMondeDG,
	   temp_globalVandeMondeDGPrime,
	   temp_globalVandeMondeMX,
	   temp_globalVandeMondeMXPrime,
	   temp_globalVandeMondeFluxMX,
	    workingMatrix;
	    
	  ddpBijFlag_type flag;
	  ddpNumDeriv_type numDeriv;
	  
	  cout << "Entering MakeGlobalVandeMonde" << endl;
	  
	  flag = DG;
	  numDeriv = zero;
	  ddpMakeGlobalVandeMonde(grid, problem, flag,
				  Bijections.DGForward, Bijections.PTForward,numDeriv,
				  temp_globalVandeMondeDG);
	  
	  //cout << "globalVandeMondeDG = \n" << globalVandeMondeDG  << endl;


	  flag = DG;  
	  numDeriv = one;
	  ddpMakeGlobalVandeMonde(grid, problem, flag, 
				 Bijections.DGForward, Bijections.PTForward, numDeriv,
				 temp_globalVandeMondeDGPrime);


	  flag = Mixed;
	  numDeriv = zero;
	  ddpMakeGlobalVandeMonde(grid, problem, flag,
				  Bijections.MXForward, Bijections.PTForward, numDeriv,
				  temp_globalVandeMondeMX);

	  flag = Mixed;
	  numDeriv = one;
	  ddpMakeGlobalVandeMonde(grid, problem, flag, 
				  Bijections.MXForward, Bijections.PTForward, numDeriv,
				  temp_globalVandeMondeMXPrime);
	  
	  cout << "\tExiting MakeGlobalVandeMonde" << endl;

	  // Create Global VandeMonde Flux Matrices
	  
	  cout << "Entering MakeGlobalVandeMondeFluxMX" << endl;
	  
	 ddpMakeGlobalVandeMondeFluxMX(grid, 
					problem,
					Bijections.MXForward,
					temp_globalVandeMondeFluxMX);
	 
	 // cout << "globalVandeMondeFluxMX = \n" << globalVandeMondeFluxMX << endl;
	  

	 cout << "\tExiting MakeGlobalVandeMondeFluxMX" << endl;
	 


	  cout << "Entering MakeGlobalVandeMondeFluxDG" << endl;
	  
	  ddpDGFluxMatrices_type temp_DGFluxMatrices;

	 // lim_{x-> x_{i+1/2} && x > x_{i+1/2}}
	  ddpDirection_type Flag1 = Plus;
	  ddpDirection_type Flag2 = Plus;
 
	  ddpMakeGlobalVandeMondeFluxDG(grid, 
					problem,
					Bijections.DGForward,
					Flag1,
					Flag2,		      
					workingMatrix); 
	  
	  temp_DGFluxMatrices.plusplus = workingMatrix;
	  temp_DGFluxMatrices.plusplusTransposed = workingMatrix.transpose();


	 // lim_{x-> x_{i+1/2} && x < x_{i+1/2}}
	  Flag1 = Plus;
	  Flag2 = Minus;
	  
	  ddpMakeGlobalVandeMondeFluxDG(grid, 
					problem,
					Bijections.DGForward,
					Flag1,
					Flag2,		      
					workingMatrix);
	  
	  temp_DGFluxMatrices.plusminus = workingMatrix;
	  temp_DGFluxMatrices.plusminusTransposed = workingMatrix.transpose();

	 
	 // lim_{x-> x_{i-1/2} && x > x_{i-1/2}}
	  Flag1 = Minus;
	  Flag2 = Plus;
	  
	  ddpMakeGlobalVandeMondeFluxDG(grid, 
					problem,
					Bijections.DGForward,
					Flag1,
					Flag2,		      
					workingMatrix);
	  
	  temp_DGFluxMatrices.minusplus = workingMatrix;
	  temp_DGFluxMatrices.minusplusTransposed = workingMatrix.transpose();

	 
	 // lim_{x-> x_{i-1/2} && x < x_{i-1/2}}
	  Flag1 = Minus;
	  Flag2 = Minus;
	  
	  ddpMakeGlobalVandeMondeFluxDG(grid, 
					problem,
					Bijections.DGForward,
					Flag1,
					Flag2,		      
					workingMatrix);
	  
	  
	  temp_DGFluxMatrices.minusminus = workingMatrix;
	  temp_DGFluxMatrices.minusminusTransposed = workingMatrix.transpose();
	   
	  cout << "\tExiting MakeGlobalVandeMondeFluxDG" << endl; 

	  // regular version
	  VandeMondeMatrices.globalVandeMondeDG = temp_globalVandeMondeDG;
	  VandeMondeMatrices.globalVandeMondeDGPrime = temp_globalVandeMondeDGPrime;
	  VandeMondeMatrices.globalVandeMondeMX = temp_globalVandeMondeMX;
	  VandeMondeMatrices.globalVandeMondeMXPrime = temp_globalVandeMondeMXPrime;
	  VandeMondeMatrices.globalVandeMondeFluxMX =  temp_globalVandeMondeFluxMX;

	  // transposed versions
	  VandeMondeMatrices.globalVandeMondeDGTransposed =
					 temp_globalVandeMondeDG.transpose();
	  VandeMondeMatrices.globalVandeMondeDGPrimeTransposed =
					 temp_globalVandeMondeDGPrime.transpose();
	  DGFluxMatrices = temp_DGFluxMatrices; 
return 0;

}

int 
ddpMakeCarrierProperies
(
	 ddpProblemInfo_type const & problem,
	 ddpGrid_type const & grid,
	 ddpSparseVector_type const & weightsSparse,
	 ddpBijection_type const & Bijections,
	 ddpVandeMondeMatrices_type const & VandeMondeMatrices,
	 ddpDGFluxMatrices_type const & DGFluxMatrices,
	 ddpCarrierProperties_type & CarrierProps)
{
	  CarrierProps.VandeMondeMatrices = VandeMondeMatrices;
	  CarrierProps.DGFluxMatrices = DGFluxMatrices;

	  // The "I" variables denote the test functions
	  // While the "J" variable are the trial function.
	  ddpBijFlag_type flagI;
	  ddpBijFlag_type flagJ;
	  ddpSparseMatrix_type vandI;
	  ddpSparseMatrix_type vandJ;
	  ddpBijForw_type BijI;
	  ddpBijForw_type BijJ;

	  // DGMassU
	  flagI = DG;
	  vandI = VandeMondeMatrices.globalVandeMondeDG;
	  BijI = (Bijections.DGForward);
	  
	  flagJ = DG;
	  vandJ = VandeMondeMatrices.globalVandeMondeDG;
	  BijJ = (Bijections.DGForward);
	  
	  ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			       flagI, BijI, vandI, 
			       flagJ, BijJ, vandJ,
			       CarrierProps.MassU);
	  

	  // DGMassQ
	  flagI = DG;
	  vandI = VandeMondeMatrices.globalVandeMondeDG;
	  BijI = (Bijections.DGForward);
	  
	  flagJ = DG;
	  vandJ = VandeMondeMatrices.globalVandeMondeDG;
	  BijJ = (Bijections.DGForward);
	  
	  ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			       flagI, BijI, vandI, 
			       flagJ, BijJ, vandJ,
			       CarrierProps.MassQ);
	  
	  // StiffUFromQ
	  flagI = DG;
	  vandI = VandeMondeMatrices.globalVandeMondeDGPrime;
	  BijI = (Bijections.DGForward);
	  
	  flagJ = DG;
	  vandJ = VandeMondeMatrices.globalVandeMondeDG;
	  BijJ  = (Bijections.DGForward);
	  
	  ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			       flagI, BijI, vandI, 
			       flagJ, BijJ, vandJ,
			       CarrierProps.StiffUFromQ);

	  // DGStiffQFromU  
	  flagI = DG;
	  vandI = VandeMondeMatrices.globalVandeMondeDGPrime;
	  BijI  = Bijections.DGForward;

	  
	  flagJ = DG;
	  vandJ = VandeMondeMatrices.globalVandeMondeDG;
	  BijJ  = Bijections.DGForward;

	  ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			       flagI, BijI, vandI, 
			       flagJ, BijJ, vandJ,
			       CarrierProps.StiffQFromU);
	  
	  //////////////////////////////////////////////////////////////////////////////
	  // ALTERNATING FLUXES
	  //////////////////////////////////////////////////////////////////////////////
	  // We implement the alternating fluxes by setting:
	  // flag2 to "Plus" for
	  // DGFluxRightUFromQ and DGFluxLeftUFromQ
	  // and
	  // flag2 to "Minus" for DGFluxRightQfromU and DGFluxLeftQFromU
	  // or vice versa.
	  //////////////////////////////////////////////////////////////////////////////
	  
	  ddpDirection_type Flag1;
	  
	  // DGFluxRightUFromQ
	  Flag1 = Plus;
	  std::vector<ddpDirection_type> Flag2Vector(grid.NumElementsNoGhost + 1);
	  for(int i = 0; i< grid.NumElementsNoGhost + 1; ++i)
	    {
	      Flag2Vector[i] = Plus;
	    }
	  
	    Flag2Vector[grid.NumElementsNoGhost] = Minus;

	   
	  
	   
	    ddpMakeSpecialFluxMatrix(grid,
				     Bijections.DGForward,
				     DGFluxMatrices,
				     Flag1,
				     Flag2Vector,
				     CarrierProps.FluxRightUFromQ);
	    
	  // DGFluxLeftUfromQ
	  Flag1 = Minus;
	  for(int i = 0; i < grid.NumElementsNoGhost + 1; ++i)
	    {
	      Flag2Vector[i] = Plus;
	    }
	  
	  ddpMakeSpecialFluxMatrix(grid,
				   Bijections.DGForward,
				   DGFluxMatrices,
				   Flag1,
				   Flag2Vector,
				   CarrierProps.FluxLeftUFromQ);
	  
	   // DGFluxRightQFromU
	  Flag1 = Plus;
	  for(int i = 0; i< grid.NumElementsNoGhost + 1; ++i)
	    {
	      Flag2Vector[i] = Minus;
	    }
	  Flag2Vector[ grid.NumElementsNoGhost ] = Plus;
	  
	  ddpMakeSpecialFluxMatrix(grid,
				   Bijections.DGForward,
				   DGFluxMatrices,
				   Flag1,
				   Flag2Vector,
				   CarrierProps.FluxRightQFromU); 
	  
	  // DGFluxLeftQFromU
	  Flag1 = Minus;
	  for(int i = 0; i < grid.NumElementsNoGhost + 1; ++i)
	    {
	      Flag2Vector[i] = Minus;
	    }
	  
	  ddpMakeSpecialFluxMatrix(grid,
				   Bijections.DGForward,
				   DGFluxMatrices,
				   Flag1,
				   Flag2Vector,
				   CarrierProps.FluxLeftQFromU);

	  //Eigen::saveMarket(DGFluxLeftQFromU, "DGFluxLeftQFromU.mtx");
	  //cout << "DGFluxLeftQFromU = \n" << DGFluxLeftQFromU << endl;
	  
	  //
	  // TotalQFromBCRHS
	  // This consists of FluxRightEndPointQFromBC and FluxLeftEndPointQFromBC.
	  // We build them here.

	  ddpSparseMatrix_type
	    FluxRightEndPointQFromBC,
	    FluxLeftEndPointQFromBC;
	  
	  // DGFluxRightFromBC 
	  Flag1 = Plus;  //right side of element
	  int elementNumber = grid.NumElementsNoGhost - 1;
	  
	  ddpMakeSpecialFluxVector(grid,
				   elementNumber,
				   Bijections.DGForward,
				   DGFluxMatrices,
				   Flag1,
				   FluxRightEndPointQFromBC);
	 
	 
	  //cout << "DGFluxRightEndPointQFromBC = \n" << FluxRightEndPointQFromBC 
	 //   << endl;
	 // Eigen::saveMarket(DGFluxRightQFromU, "DGFluxRightEndPointQFromBC.mtx");
	 


	 // DGFluxLeftFromBC
	 Flag1 = Minus;  // left side of element
	 elementNumber = 0;
	 ddpMakeSpecialFluxVector(grid,
				  elementNumber,
				  Bijections.DGForward,
				  DGFluxMatrices,
				  Flag1,
				  FluxLeftEndPointQFromBC);
	 
	 
	//cout << "FluxLeftEndPointQFromBC = \n" << FluxLeftEndPointQFromBC; 
	 //   << endl;

	 ddpSparseMatrix_type TotalQFromBCRHS_temp;
	 // The minus sign on the Left endpoint matrix is due moving the flux vector 
	 // (which originally had plus sign) to the RHS of the equation.  
	 // The plus sign is if from 
	 // moving the flux vector from the
	 // LHS to the RHS as well.
	 
	 ddpConcatenateMatrices(-FluxLeftEndPointQFromBC, 
				/* +1 */  FluxRightEndPointQFromBC, 
				TotalQFromBCRHS_temp);
	
	   
	  CarrierProps.TotalQFromBCRHS = CarrierProps.Diffusivity*TotalQFromBCRHS_temp;
	 //  cout << "DGTotalQFromBCRHS = \n" << CarrierProps.TotalQFromBCRHS << endl;
	  //  Eigen::saveMarket(DGTotalQFromBCRHS, "DGTotalQFromBCRHS.mtx");

	  // TotalQFromURHS
	  CarrierProps.TotalQFromURHS = 
	    CarrierProps.Diffusivity * (- CarrierProps.StiffQFromU 
					+ CarrierProps.FluxRightQFromU
					- CarrierProps.FluxLeftQFromU);
	  
	   // TotalUFromQRHS
	  CarrierProps.TotalUFromQRHS = 
	     -CarrierProps.StiffUFromQ 
	    + CarrierProps.FluxRightUFromQ 
	    - CarrierProps.FluxLeftUFromQ;

	//cout << "CarrierProps " <<  CarrierProps.TotalUFromQRHS << endl; 
return 0;
}



int 
ddpMakePoissonProperties
	(
	 ddpProblemInfo_type const & problem,
	 ddpGrid_type const & grid,
	 ddpSparseVector_type const & weightsSparse,
	 ddpBijection_type const & Bijections,
	 ddpVandeMondeMatrices_type const & VandeMondeMatrices,
	 ddpDGFluxMatrices_type const & DGFluxMatrices,
	 ddpPoissonProperties_type & PoissonProperties)
{
	  
	  ddpBijFlag_type flagI;
	  ddpBijFlag_type flagJ;
	  ddpSparseMatrix_type  vandI;
	  ddpSparseMatrix_type  vandJ;
	  ddpBijForw_type  BijI;
	  ddpBijForw_type  BijJ;
	  
	  //ddpSparseMatrix_type MXA00Matrix;
	  ddpSparseMatrix_type MXA01Matrix;
	  ddpSparseMatrix_type MXA10Matrix;
	  ddpSparseMatrix_type MXCMatrix;
	  
	  // MX A00 Matrix
	  flagI = Mixed;
	  vandI = VandeMondeMatrices.globalVandeMondeMX;
	  BijI  = Bijections.MXForward;
	  
	  flagJ = Mixed;
	  vandJ = VandeMondeMatrices.globalVandeMondeMX;
	  BijJ  = Bijections.MXForward;
	  ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			       flagI, BijI,vandI, 
			       flagJ, BijJ,vandJ,
			       PoissonProperties.A00);

	  //Eigen::saveMarket(MXA00Matrix, "MXA00Matrix.mtx");

	 // MX A01
	 flagI = Mixed;
	 vandI = VandeMondeMatrices.globalVandeMondeMXPrime;
	 BijI  = Bijections.MXForward;
	 
	 flagJ = DG;
	 vandJ = VandeMondeMatrices.globalVandeMondeDG;
	 BijJ  = Bijections.DGForward;
	 
	 ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			     flagI, BijI,vandI, 
			     flagJ, BijJ,vandJ,
			     PoissonProperties.A01);
	 
	//  Eigen::saveMarket(MXA01Matrix, "MXA01Matrix.mtx");
	 
	 // MX A10
	 flagI = DG;
	 vandI = VandeMondeMatrices.globalVandeMondeDG;
	 BijI  = (Bijections.DGForward);

	 flagJ = Mixed;
	 vandJ = VandeMondeMatrices.globalVandeMondeMXPrime;
	 BijJ  = Bijections.MXForward;
	 
	 ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			     flagI, BijI,vandI, 
			     flagJ, BijJ,vandJ,
			     PoissonProperties.A10);
	 
	  // MXVLeft
	 ddpSparseMatrix_type MXVLeftFromBC;
	 
	 // This depends on a specific choice of the upsilon 
	 // basis functions.  This part needs to be changed if a different choice
	 // of basis functions is made.
	 
	 ddpSparseMatrix_type 
	   globalVandeMondeFluxMXTranspose 
	   = VandeMondeMatrices.globalVandeMondeFluxMX.transpose();
	 
	 MXVLeftFromBC =  globalVandeMondeFluxMXTranspose.col(0);
	 // cout << "MXVLeftFromBC = \n" << MXVLeftFromBC << endl;
	 
	 // MXVRight
	 ddpSparseMatrix_type MXVRightFromBC;
	 
	 MXVRightFromBC 
	   = globalVandeMondeFluxMXTranspose.col( grid.NumElementsWithGhost - 1 );

	 // cout << "MXVRightFromBC = \n" << MXVRightFromBC  << endl;
	 
	 // MXVBig 
	 ddpSparseMatrix_type MXVBig_temp;
	 
	 // Concatenate these matrices.
	 
	 cout << "Entering Concatenate BC" << endl;
	 ddpConcatenateMatrices(-MXVLeftFromBC, MXVRightFromBC, MXVBig_temp);
	 cout << "\tExiting Concatenate BC" << endl;
	 PoissonProperties.VBig = MXVBig_temp; 
	 
	 // MXC
	  flagI = DG;
	 vandI = VandeMondeMatrices.globalVandeMondeDG;
	 BijI  = (Bijections.DGForward);

	 flagJ = DG;
	 vandJ = VandeMondeMatrices.globalVandeMondeDG;
	 BijJ  = (Bijections.DGForward);
	 
	 ddpMakeGeneralMatrix(problem, grid, weightsSparse, 
			      flagI, BijI,vandI, 
			      flagJ, BijJ,vandJ,
			      PoissonProperties.C);
	 // Eigen::saveMarket(MXCMatrix, "MXCMatrix.mtx");

	 //MXABig
	 ddpSparseMatrix_type MXABig;
	 // We make this a block so the extra matrices go out of scope 
	 // and the memory is freed.
	 {
	   ddpSparseMatrix_type
	     ATop,
	     ATopTranspose,
	     ABottom,
	     ABottomTranspose,
	     MXABigTranspose;
	   

	   ddpSparseMatrix_type zeros(PoissonProperties.A10.rows(),
					PoissonProperties.A01.cols());
				      

	   ddpConcatenateMatrices(PoissonProperties.A00,
				  -PoissonProperties.A01,ATop);


	   ddpConcatenateMatrices(PoissonProperties.A10, zeros, ABottom);

	   ATopTranspose = ATop.transpose();
	   ABottomTranspose = ABottom.transpose();

	   ddpConcatenateMatrices(ATopTranspose, ABottomTranspose, MXABigTranspose);

	      
	   PoissonProperties.ABig = MXABigTranspose.transpose();
	  }
	  
  return 0;
}



