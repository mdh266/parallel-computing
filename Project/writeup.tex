\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fullpage}

\begin{document}

\title{A Parallel Version Of Gummels Iteration Using OpenMP}
\author{Michael Harmon}


\maketitle 

\section{Abstract}

\noindent
The world is currently consuming 15 approximately terawatts (TW) of power and the International Energy Agency (IEA) has estimated that by 2050 world power consumption could be up to 50 terawatts (TW).  This unprecidented need for energy cannot be met with traditional methods of production from hyrdocarbons alone.  In order to avoid a massive energy crisis in the near future serious invests into clean and renewable energy must be made. Of all the possible sources, no source has has much potential as solar energy (approximately 35,0000 TW of solar power are available).  However, the cost of solar energy and photovoltaic devices are still too expensive for large scale deployment.  The next generation of photovoltaic devices must be more efficent and cost effective if they are to play a larger role in the world energy market.

\vspace{3mm}


\noindent
The recent explosion of cheap computational power has greatly changed the field of research and development of photovoltaic devices and semiconductor devices in general.  Research in this area of engineering has increasing relied on computational modeling and simulation to drive innovation, optimize design, and increase production.  Simulations of photovoltaic devices (and semiconductor devices in general) are accomplished by numerical solutions of the drift diffusion-Poisson equations.  Therefore a numerical method to solve these equations that is extremely fast and highly accurate would be of great interest in designing the next generation of photovoltaic devices and and is the purpose of this project.


\section{Mathematical Model}
The transport of electrons ($n$) and holes $(p)$ in semiconductor devices is governed by the drift 
diffusion-Poisson equations:

\begin{equation}
 \frac{\partial n}{\partial t} -\nabla \cdot \left( \mu_{n} n E(x,t) + D_{n} \nabla_{x} n  \right)
= R_{n}(n,p) + G_{th}(n,p)
\end{equation}

\begin{equation}
\frac{\partial p}{\partial t} - \nabla \cdot \left(  \mu_{p} p E(x,t) -D_{p} \nabla_{x} p  \right)
 = R_{p}(n,p) + G_{th}(n,p)
\end{equation}


\begin{equation}
 -\nabla \cdot \left( \epsilon_{r} \nabla \Phi(x,t) \right) = \frac{q}{\epsilon_{0}} (C(x)- (n - p)) 
\end{equation}


\noindent
Where $G_{th}, R$ is the thermal generation and radiative recombination rates of the carriers respectively.
$C(x)$ is the doping profile, and $E$ and $\Phi$ are the electric field potential of the system. ($E = -\nabla \Phi$)

\vspace{3mm}


\noindent
We use a local discontinuous Galerkin method to solve for (1) and (2) and a mixed finite element method for (3). (These methods have high accuracy and the ability to explore more complicated designs/geometries.)  

\vspace{4mm}


\noindent 
These numerical methods lead to  the three linear systems show below:

\newpage

\begin{equation}
M \dot{n} = S_{1} n - R + G
\end{equation}

\begin{equation}
M \dot{p} = S_{2} p - R + G
\end{equation}

\begin{equation}
\Lambda \Phi = v
\end{equation}


\noindent
The solution of these linear systems is described in the next section.


\section{Serial Gummel Iteration}

\noindent
The drift diffusion-Poisson equations represent a non-lienar system of partial differential equations and only have analytic solutions under certain circumstances.  In order to solve these equations self consistently using a numerical algorithm one must use the Gummel iteration. 

\vspace{2mm}


\noindent
The Gummel iteration is an iterative procedure where by we solve for the electric field in Poisson's equation and use this solution to solve the drift diffusion equations for electrons and holes.  We repeat this procedure until a steady state solution is obtained.  

\vspace{2mm}

\noindent
Numerically, an algorithm to preform this procedure must do the following at every time iteration:

\begin{enumerate}
\item Construct $v$ from values of $n$ and $p$ and use a direct linear solver on (6)
\item Use solutions from (6) assemble Matrices $S_{1}$ and preform matrix vector multiplication and add vectors $R$ and $G$ to the right hand side.
\item Use a linear solver on (4) and preform forward Euler.
\item Use solutions from (6) assemble Matrices $S_{2}$ and preform matrix vector multiplication and add vectors $R$ and $G$ to the right hand side.
\item Use a linear solver on (5) and preform forward Euler.
\end{enumerate}


\noindent
This algorithm is called the Gummel iteration and one time step is depicted below:

\begin{center}
 \includegraphics[scale=0.55]{./serialgummel.png}
\end{center}

\vspace{3mm}



\section{Parallel Gummel Iteration}


\noindent
The idea to speed up this procedure is to use the the MKL (OpenMp) on the parts of the code that require the most time which are the direct linear solver in (6) and the matrix vector multiplications involved in the construction of $S_{1}$ and $S_{2}$ in (4) and (5).  Since these equations are in a linear system form, it is only necessary to port them to a library which has these numerical linear algebra routines in parallel using OpenMP. 


\vspace{2mm}


\noindent
In addition, we will also explore the idea of using task parallelism to solve (4) and (5).  This idea arises naturally as the calculations of electrons and holes can be done independently of one another.  The algorithm is summarized below:


\begin{itemize}
\item Use all available threads for direct solver in (6).
\item Use half the threads to each do matrix-vector multiplication and linear solving in (4) and (5) at the same time.
\end{itemize}


\noindent
A depiction of this algorithm on one of Stampedes nodes is shown below:


\begin{center}
 \includegraphics[scale=0.55]{./parallelgummel1.png}
\end{center}


\noindent
This turned out to not be the best way to write a parallel implementation of Gummel's iteration for two charge carriers. The reasons for this will be explained in the next section and another implementation will be discussed in future work.


\section{Challenges and Solutions}

\subsection{The three challenges}

\noindent
The main challenges in speeding up this code were:

\begin{itemize}
\item Building the matrices faster (and in parallel)
\item Solving the linear systems using parallel libraries
\item Do this for each carrier concurrently using task parallelism
\end{itemize}

\noindent
The first two of these turned out to be the largest bottle necks of the run time performance and the effort (as well as the feasibility) of the last outweighed its potential gains.  Each is explained below. 


\subsection{Solution 1. Building matrices in parallel}
\noindent
At first constructing the matrices $S_{1}$ and $S_{2}$ were the biggest consumer of time.  These matrices were actually the linear combination of other matrices which were assembled.  The most time consuming matrix to assemble was called StiffFromU.  This matrix corresponds to: (the $\pm$ depend on the whether its and electron or hole)


\begin{equation}
\begin{tabular}{cl}
   StiffFromU(J,I) &= $\pm \int_{I_{J}} \, E(x,t) 
		     \psi_{I}(x) \, 
			\frac{\partial}{\partial x} \psi_{J}(x) \, dx $ \\ \\
%	    &= $\pm \left( \text{M}_{DGPrime}^{T} \; \cdot \; \left(
%		\text{E}_{w} \; \cdot \; \left(
%		\text{M}_{DG} \; \cdot \; u_{DOF} \right) \right) \right)$
  \end{tabular}
\end{equation}


\vspace{2mm}


\noindent
At every time step this matrix was constructed (where the integral was performed using Gauss quadrature) and then multiplied the vector $u_{DOF}$ to give another vector RHSFromStiff.  
Although all the matrices in our routines were sparse, constructing StiffFromU was time as we needed to do quadrature at every entry.


\vspace{3mm}


\noindent
Instead of constructing StiffFromU at every time step the following was done instead,\footnote{This technique originally done serially by another student in my group, before I started to work on developing a parallel version of the code.  I introduce here to explain what I converted over to the MKL} 
 

\vspace{2mm}

\begin{equation}
\text{RHSFromStiff}	    = \pm \left( \text{M}_{DGPrime}^{T} \; \cdot \; \left(
		\text{E}_{w} \; \cdot \; \left(
		\text{M}_{DG} \; \cdot \; u_{DOF} \right) \right) \right)
\end{equation}


\vspace{2mm}

\noindent
The matrices $M_{DG}(i,j) = \psi_{i}(x_{j})$ and $M_{DGPrime}(i,j) = \frac{d\psi_{i}(x_{j})}{dx}$ are called the Vandermonde matrices and do not change in time.  The entries $\psi_{i}(x_{j})$ represents the i-th basis function evaluated at the j-th qudrature point. These are computed once initially and stored to be used later.  The last  matrix, $E_{w}$, is still constructed at every time step, but since its a diagonal matrix its only order $\mathcal{O}(N)$


$$ \text{E}_{w} = \left(
			     \begin{array}{ccccc} 
			     \pm E(x_{0}) w_{0} & 0 & 0  & 0 & \ldots \\
			     0 & \pm E(x_{1}) w_{1} & 0 & 0 & \ldots \\
			     0 & 0 & \pm E(x_{2}) w_{2} & 0 & \ldots \\
			    \vdots & & & & \\
                            \end{array} \right)$$

\noindent
Where $E(x_{i})$ is the electric field evaluated at the i-th quadrature point and $w_{i}$ is the i-th quadrature weight.
Implementing this over explicitly constructing StiffFromU resulted in very big time reduction.  


\vspace{3mm}


\noindent
These matrix vector multiplications were being computed using a C++ based, vectorized, linear algebra library called Eigen (Eigen is actually faster than the MKL for serially calculations).  In order to increase performance it was desired to convert all the data structures and routines to the MKL so that the matrix and vector operations involved in constructing $S_{2}$, $S_{1}$ would be done in parallel.   A MKL version of the method in (8) that was implemented is  described below:


\begin{enumerate}
\item First preform $\text{M}_{DG} \; \cdot \; u_{DOF} $ and store it  in a temporary vector $V_{0}$ using the MKL call:   mkl\_cspblas\_dcsrgemv

\item Perform component wise multiplication of the electric field point values ($E(x_{i})$) and the the Gaussian quadrature weights ($w_{i}$) and store this in a temporary vector $V_1$ using the MKL call: vdMul

\item Perform another component wise multiplication of $V_{1}$ and $V_{0}$ and store this in a temporary vector $V_2$ using the MKL call: vdMul

\item Preform the matrix-vector multiplication RHSFromSTiff$=M_{DGPrime}^{T} \cdot V_{2}$ again using the MKL call mkl\_cspblas\_dcsrgemv
\end{enumerate}


\noindent
Similar routines were performed in the construction of the and matrix-vector multiplications in the right hand side of (4) and (5).  The run time performances of these MKL calls will be shown in the results section.


\subsection{Solution 2. Linear solvers}
After creating RHSFromStiff using the technique in (8), the largest consumer of time was the inversion of $\Lambda$. The matrices $M$ turned out to be diagonal because of choice of basis functions, so their inversions were not as tedious. (For another choice of basis functions they might be).  Originally we chose to use a direct solver for inverting $\Lambda$ since the iterative solvers in Eigen would not converge for the diagonal matrices $M$.  Therefore in order to be consistent a direct solver was chosen for the MKL as well using Intel's Direct Sparse Solver Interface Routines.


\vspace{2mm}


\noindent
The linear solver routine that was used was dss\_solve\_real from the MKL's Direct Sparse Solver Interface Routines.  However, in order to make simpler to use and more generally applicable, a struct/class was written to act as wrapper for the MKL solver routine.  This allowed for instantiation of solver objects, created to solve a specific linear system corresponding to sparse matrices in Compressed Row Storage (CRS) format.  This class is called ``ddpSolver\_type" and is in the file solver.hpp.  In order use it one simply applies the commands:

\vspace{3mm}


\begin{tabular}{ll}
ddpSolver\_type SolverObject;  & // creates solver object \\
SolverObject.initialize(A);  & // initialize the MKL solver routine by giving it a matrix in CSR form \\
SolverObject.solve(A,b,x); &// solves $Ax=b$ using the MKL direct solver: dss\_solve\_real \\
& // where b and x are points to double arrays
\end{tabular}

\vspace{3mm}


\noindent
The routine performances of these will be shown in the results section.


\subsection{Solution 3.  Task parallelism}

In order to allow for simpler use, generality, and to allow for task parallelism while still being readable a struct/class was created for the carriers so that electrons and holes would be instantiations of the class.  This class was called ``ddpCarrier\_type" and stored is stored in the file carrier.hpp.  Another struct/class ``ddpPoisson\_type" (contained in the file poisson.hpp) was also written to to encapsulate construction and solution of (6). This was done for ease of use, readability, and for future work.


\vspace{2mm}


\noindent
Applying the ideas of task parallelism to this problem turned out to be much harder than originally anticipated and the pay off for using half the threads to solve for the (4) and (5) concurrently using the MKL instead of using all the threads to solve the (4) and then (5) using the MKL would be minimal.



\vspace{2mm}


\noindent
Achieving task parallelism using simpilar classes, data structures, and mathematical operations (not using the MKL or Eigen) was completed in the file task.cpp.


\section{Model}

\noindent
For simplicity we assume $R = G = 0$ and that we have Silicon $p-n$ junction of length $1 \times 10^{-4}$ cm.  Initially, we have $n(x,0) =  p(x,0) = C(x)$.  That is initially, the only charge in device is due the doping profile.  This is depicted below:


\begin{center}
\includegraphics[scale=0.35]{initial.png} 
\end{center}


\noindent
Forward biasing this device (setting $(\Phi(0,t) = 0, \; \Phi(L,t)=0.5$ [Volts]) and running our simulation for a time $t = 1\times 10^{-12}$ seconds we arrive at the following using Eigen:


\begin{center}
 \includegraphics[scale=0.35]{Final_Eigen.png}
\end{center}

\noindent
And the following using the MKL:


\begin{center}
 \includegraphics[scale=0.35]{final.png}
\end{center}

\noindent
This shows that the Eigen and MKL results are in good agreement.\footnote{The error at the right boundary condition in the MKL version is most likely due to the way the boundary conditions were enforced in the local discontinuous Galerkin method. This shows up in other cases when using Eigen as well, but I'm not sure why it doesn't show up here.  The MKL version did pass all the unit tests where it was compared with analytical solutions, so I'm confident the MKL version is working properly.}


\section{Results}
Getting the MKL to out perform Eigen's routines requires that the spawning of OpenMP threads by the MKL take less time than it takes these threads to do their work. This necessitates that the problem involve very large linear system. The timings for these runs were done on matrices of size 40000 $\times$ 40000 and to keep the run times down only 100 time steps were taken.  The timings were done using timers from the grvy library\footnote{https://red.ices.utexas.edu/
projects/software/wiki/GRVY} and were started and stopped in the serial sections of the code, i.e. before and after the Eigen and MKL calls.
  

\vspace{2mm}


\noindent
The timings below are of the took two biggest bottlenecks are in the computation: computing RHSFromStiff and solving (6) (solving for the electric field).  The times below  represent the total time it took to complete all 100 of computations RHSStiff for both Eigen and the MKL.

\begin{center}
\includegraphics[scale=0.5]{RHS.png} 
\end{center}


\noindent
The next timing graph represents the total time it took to complete all 100 of computations (6) for both Eigen and the MKL.


\begin{center}
\includegraphics[scale=0.5]{Elec.png} 
\end{center}


\noindent
The total run times of the simulations are plotted against the number of threads below:\footnote{Solutions using Eigen and the MKL were computed in the same time step.}

\begin{center}
\includegraphics[scale=0.5]{Total.png} 
\end{center}

\noindent
One reason for the relatively poor reduction in time past 4 threads could be due to the fact that time for the threads to complete the work was so short (ex: with 4 threads it took on average 8.40937$\times 10^{-3}$ seconds to solve (6) ) that any time reduction by increasing the number of processors is canceled out by the time it takes to maintain cache coherence across cores as well as across sockets.  A more scalable algorithm will be developed in the next section.


\section{Future Work}

\noindent
In order to achieve better performance and scaling for solving the drift diffusion-Poisson equations it will be attempted to change the way (4) and (5) are constructed and solved.  Instead of constructing and solving (4) and then doing the same (5) one may reformulate the problem and solve both at the same time.  We can rewrite the system of (4) and (5) as one larger system


\begin{equation}
\left( \begin{matrix}
M & 0 \\
0 & M \\
\end{matrix} \right)
\left[ \begin{array}{c}
n \\
p \\
\end{array} \right] = 
\left( \begin{matrix}
S_{1} & 0 \\
0 & S_{2} \\
\end{matrix} \right)
\left[ \begin{array}{c}
n \\
p \\
\end{array} \right]
- \left[ \begin{array}{c}
R\\
R \\
\end{array} \right]
+\left[ \begin{array}{c}
G\\
G \\
\end{array} \right]
\end{equation}

\noindent
This maybe be attempted using PetSc or Trilinos to take advantage of the built in parallelism as well as distributed memory.
\end{document}