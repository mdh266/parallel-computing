#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc,char **argv)
{
  MPI_Comm comm;
  MPI_Win win;  // Each process needs a window
  int ntids, mytid;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  { 
    int dest, source;
    int my_number = mytid, other_number = -1.;

    if (mytid == 0)
	source = MPI_PROC_NULL;
    else
	source = mytid-1;
 
    if (mytid == ntids -1)
	dest = MPI_PROC_NULL;
    else
	dest = mytid+1;


    // set the window on the target
    MPI_Win_create(&my_number,sizeof(int),1,MPI_INFO_NULL,comm,&win);

    // synchronize the window
    MPI_Win_fence(0,win);

    // get the information from source and assign it to the origins
    // other_number
    MPI_Get(&other_number, 1, MPI_INT, source, 0, 1, MPI_INT, win);

    // synchronize the window
    MPI_Win_fence(0,win);

    MPI_Win_free(&win);

    /* Correctness check */
    int *gather_buffer=NULL;
    if (mytid==0) 
    {
      gather_buffer = (int*) malloc(ntids*sizeof(int));
      if (!gather_buffer) MPI_Abort(comm,1);
    }

    MPI_Gather(&other_number,1,MPI_INT,
               gather_buffer,1,MPI_INT, 0,comm);
  
    if (mytid==0)
    {
      int i,error=0;
      for (i=0; i<ntids; i++) 
        if (gather_buffer[i]!=i-1)
	{
          printf("Processor %d was incorrect: %d should be %d\n",
                 i,gather_buffer[i],i-1);
          error =1;
        }
      if (!error) printf("Success!\n");
      free(gather_buffer);
    }
  } // end MPI BLOCK

  MPI_Finalize();
  return 0;
}
